#version 100
uniform mat4 model;
//uniform mat3 normalMatrix;
uniform mat4 view;
uniform mat4 projection;

attribute vec3 aPos;
//attribute vec3 aNormal;
//attribute vec2 aTexCoords;

//varying vec2 TexCoords;
//varying vec3 FragPos;
//varying vec3 Normal;
//varying vec3 Center;


void main()
{
    //TexCoords = aTexCoords;    
    //Normal = aNormal;
    vec3 FragPos = vec3(model * vec4(aPos, 1.0));
    //Center = vec3(model * vec4(0.0,0.0,0.0,1.0));
    gl_Position = projection * view * vec4(FragPos, 1.0);
}
