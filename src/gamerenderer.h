#ifndef GAMERENDERER_H
#define GAMERENDERER_H

#include "renderer.h"

#include <memory>

class GameRenderer : public Renderer {
    unique_ptr<Renderer> defaultRenderer;
    const tid_t bulletType = 2;
    bool haveBullets;
  public:
    GameRenderer();
    void addGeometry(tid_t tid, const vector<glm::dvec2> &coordinates) override;
    void addGeometry(tid_t tid, const vector<glm::vec2> &coordinates) override;
    void removeGeometry(tid_t tid) override;
    void reset(unsigned int) override;
    void submit() override;
    void draw(tid_t tid, const vec3 &transform, uint8_t layer, uint8_t color) override;
};

#endif // GAMERENDERER_H
