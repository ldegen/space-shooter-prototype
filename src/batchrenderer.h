#ifndef BATCHRENDERER_H
#define BATCHRENDERER_H

#include "renderer.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>
#include <glad/glad.h>
#include <unordered_map>
#include <vector>
#include <list>


// sry about the name, will find a better one.
class BatchRenderer : public Renderer {


    using index_t = unsigned short;
    using vertex_count_t = index_t;
    using index_count_t = index_t;
    using vertex_offset_t = vertex_count_t;
    using index_offset_t = index_count_t;
    using object_name_t = unsigned int;

    // total number of vertices fitting in the buffer
    vertex_count_t maxVertices;
    vertex_count_t slotSize;
    index_count_t maxIndices;

    object_name_t VBO_vertices=0;
    object_name_t VBO_transforms=0;
    object_name_t EBO=0;

    vector<index_t> indices;
    vector<vec3> transforms;
    vector<vec2> positions;

    const bool fill = false;


    // The memory pool is devided in equal-sized slots.
    // While all the slots have the same, fixed size, the
    // actual geometry data stored in them may not.

    struct Slot {
      vertex_offset_t start;
      vertex_count_t size;
    };

    // all the slots that are currently in use
    // are kept in a table, keyed by their "user" uid
    unordered_map<tid_t, Slot> usedSlots;

    // when a slot is not used any more, its offset is appended to a free-list
    list<vertex_offset_t> freeList;


    //--------- drawing state

    pair<index_t, index_t> _bounds {std::numeric_limits<index_t>::max(),0};
    index_offset_t _pos = 0;

    Slot * allocateSlot(tid_t tid);
    void setupBufferObjects();
    index_offset_t draw(const tid_t tid, const vec3 & transform, index_offset_t pos, pair<index_t,index_t> & bounds);
  public:
    BatchRenderer(vertex_count_t slotSize, vertex_count_t max_vertices = 2048, bool fill=true);

    virtual void addGeometry(tid_t tid, const vector<glm::vec2> & coordinates) override;
    virtual void removeGeometry(tid_t tid) override;
    virtual void reset() override;
    virtual void submit() override;
    virtual void draw(tid_t tid, const vec3 & transform) override;
    ~BatchRenderer() override;
};
#endif // BATCHRENDERER_H
