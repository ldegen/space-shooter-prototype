#ifndef CAMERAMAN_H
#define CAMERAMAN_H
#include "bbox.hpp"
#include "protocol.h"
#include "plotter.h"
#include <glm/glm.hpp>
using glm::dvec2;
using namespace std;
using Box = Bbox<double>;
using Pair = pair<dvec2,dvec2>;
class CameraMan
{
  // the world boundaries
  Box worldBox;

  Plotter plotter;
  Box currentBox;
  Box velocityBox;

  double duration = 2.5;

  double margin = 100.0;
  double timestep = 1.0/60.0;

  dvec2 A {0,0};
  dvec2 B {0,0};
  dvec2 dA {0,0};
  dvec2 dB {0,0};
  bool initializedA {false};
  bool initializedB {false};


  bool hflipped = false, vflipped=false;
  public:
    CameraMan(const Box &bbox);
    cmd_look_at track(double t, const Blip * A, const Blip * B);
    cmd_look_at track(double t, const Blip * A);
    cmd_look_at total(double t);
    void reset(double t);
    void onExit();


  private:
    void updatePositions(const Blip * blipA, const Blip * blipB);
    void updatePosition(const Blip * blip);
    void trackTwo(const dvec2 &A, const dvec2 & B, const dvec2 &dA, const dvec2 &dB);
    void normalize();
    pair<dvec2*,dvec2*> sortY(dvec2 * A, dvec2 * B);
    pair<dvec2*,dvec2*> sortX(dvec2 * A, dvec2 * B);
    void hflip(dvec2 * A, dvec2 * B);
    void vflip(dvec2 * A, dvec2 * B);
    Box tween(const Box &current, const Box &target, double f);
    double tween(double current, double target, double f);
    Box mkbox(const dvec2 & A, const dvec2 &B);
    Box mkbox(const dvec2 & A, double r);
    dvec2 position(const Blip * b);
    dvec2 velocity(const Blip * b);
    double width(const dvec2 & A, const dvec2 &B);
    double height(const dvec2 & A, const dvec2 &B);
    dvec2 mid(const dvec2 & A, const dvec2 &B);
    cmd_look_at lookat(const Box & extent);
};

#endif // CAMERAMAN_H
