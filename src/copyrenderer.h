#ifndef COPYRENDERER_H
#define COPYRENDERER_H

#include "renderer.h"

#include <unordered_map>
#include <memory>

using namespace std;

class CopyRenderer : public Renderer
{
    unique_ptr<Renderer> delegatee;
    unsigned int numcopies;

    unordered_map<tid_t, tid_t> ranks;

    //------------- draw state
    unordered_map<tid_t, tid_t> _counters;

  public:
    CopyRenderer(unique_ptr<Renderer> delegatee, unsigned int numCopies);

    virtual void addGeometry(tid_t tid, const vector<glm::vec2> &coordinates) override;
    virtual void removeGeometry(tid_t tid) override;
    virtual void reset(unsigned int) override;
    virtual void submit() override;
    virtual void draw(tid_t tid, const vec3& transform) override;
};

#endif // COPYRENDERER_H
