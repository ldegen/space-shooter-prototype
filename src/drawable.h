#ifndef DRAWABLE_H
#define DRAWABLE_H
class Shader;
class Drawable {
  public:
    virtual void Draw(const Shader & shader, unsigned int hints=0) const = 0;
    virtual ~Drawable(){}
};
#endif
