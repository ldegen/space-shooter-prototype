#include "application.h"
#include "glfw-shell.h"
#include "protocol.h"
#include "bbox.hpp"
#include "render-context.h"
#include "mesh.h"
#include "batchrenderer.h"
#include "multipassrenderer.h"
#include "copyrenderer.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>
#include <glad/glad.h>


class App : public Application{
    Bbox<double> extent {-1.0, -1.0, 1.0, 1.0};
    Bbox<double> motiv {-1.0, -1.0, 1.0, 1.0};
    Bbox<double> fit(const Bbox<double> & motiv, double viewportWidth, double viewportHeight){
      double zoomFactor = std::min(viewportWidth/motiv.width() , viewportHeight/motiv.height());
      double w = viewportWidth / zoomFactor;
      double h = viewportHeight / zoomFactor;
      double x = - 0.5 * w;
      double y = - 0.5 * h;
      return {x,y,x+w,y+h};
    }
    glm::mat4 ortho(){
      double extentWidth = extent.width();
      double extentHeight = extent.height();
      float left = static_cast<float>(-extentWidth/2.0);
      float right = static_cast<float>(extentWidth/2.0);
      float bottom = static_cast<float>(-extentHeight/2.0);
      float top = static_cast<float>(extentHeight/2.0);
      float near = 0.0f;
      float far = 1000.0f;
      return glm::ortho(left, right, bottom, top, near, far);
    }

    int randomInt(int min, int max){
      return min + rand() % (1 + max - min);
    }
    unsigned int randomUInt( unsigned int min, unsigned int max){
      return min + static_cast<unsigned int>(rand()) % (1 + max - min);
    }
    const float minAsteroidRadius =0.14f;
    const float maxAsteroidRadius = 1.0f;
    const float radiusDerivation = 0.4f;
    const float angleDerivation = 0.3f;
    const float meanRadius = glm::linearRand(minAsteroidRadius, maxAsteroidRadius);
    int columns = 8;
    int rows = 8;
    int activeRows = 0;
    int activeColumns = 0;
    vector<glm::vec2> astroidVertices(unsigned int seed){
      srand(seed);
      unsigned int N = randomUInt(8,15);
      std::vector<glm::vec2> vertices;
      vertices.reserve(N);
      for (unsigned int i = 0; i < N; i++){
        const float mtheta = 2.0f * glm::pi<float>() * static_cast<float>(i) / static_cast<float>(N);
        const float theta = glm::gaussRand<float>(mtheta, angleDerivation);
        const float r = glm::gaussRand<float>(meanRadius, radiusDerivation);
        const float x = r * glm::cos(theta);
        const float y = r * glm::sin(theta);

        vertices.push_back(glm::dvec2 {x,y});
      }
      return vertices;
    }

    Mesh astroidMesh(unsigned int seed){
      return Mesh::polygon2D(astroidVertices(seed), HINT_FILL);
    }

    float position(int i0, int n0){
      float i = static_cast<float>(i0);
      float n = static_cast<float>(n0);
      return 2.0f*i - n + 1.0f;
    }
  public:
    virtual ~App(){}

    int run(Shell & shell) override{

      unique_ptr<Renderer> renderer {
        make_unique<CopyRenderer>(
          make_unique<MultiPassRenderer>(make_unique<BatchRenderer>(16,16384)),
          32
        )
      };

      glEnable(GL_DEPTH_TEST);
      Shader shader{"batch.gles2.vert", "blip.gles2.frag"};
      unsigned int numberOfTypes = 4;
      double width = columns + 2;
      double height = rows + 2;
      motiv.left = -width ;
      motiv.right = width;
      motiv.bottom = -height;
      motiv.top = height;
      //std::vector<Mesh> meshes;
      //meshes.reserve(N);
      for(unsigned int i = 0; i < numberOfTypes; i++){
        renderer->addGeometry(i,astroidVertices(i));
        //meshes.push_back(astroidMesh(i));
      }
      while(!shell.windowShouldClose()){

        shell.pollEvents();
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
        float t = static_cast<float>(shell.time());
        mat4 viewTf(1.0f);
        shader.use();
        shader.setMat4("view", viewTf);
        shader.setMat4("projection", ortho());
        shader.setVec3("objectColor", vec3(1.0f));
        renderer->reset();
        for(int row = 0; row < rows; row++){
          for(int column = 0; column < columns; column++){
            float tx = position(column,columns);
            float ty = position(row,rows);
            float theta = 2*t / (1 + row + column);
            uid_t i = static_cast<uid_t>(row * columns + column);
            uid_t uid= i % 4;
            //uid_t uid = 0;
            renderer->draw(uid,{tx,ty,theta});
          }
        }
        renderer->submit();
        shell.swapBuffers();
      }
      return 0;
    }
    void buttonStateChanged(Shell &shell, unsigned int state) override{
      if(state & BTN_QUIT){
        shell.requestWindowClose();
      }
      if(state & BTN_TURN_RIGHT_A){
        activeColumns = min(columns, activeColumns + 1);
      }
      if(state & BTN_TURN_LEFT_A){
        activeColumns = max(0, activeColumns - 1);
      }
      if(state & BTN_FORWARD_THRUST_A){
        activeRows = min(rows, activeRows + 1);
      }
      if(state & BTN_REVERSE_THRUST_A){
        activeRows = max(0, activeRows - 1);
      }
    }
    void windowResized(Shell &shell, int width, int height) override{
      glViewport(0,0,width, height);
      extent = fit(motiv, static_cast<double>(width), static_cast<double>(height));
    }

};

int main(int argc, char *argv[]){
  Config config {Config::parseCommandLine(argc, argv)};
  App app;
  return GlfwShell::run(config, app);
}
