#include "misc-utils.h"
#include "cameraman.h"

#include <tuple>

CameraMan::CameraMan(const Bbox<double> & bbox)
  :worldBox(bbox)
  ,plotter("data")
  ,currentBox(bbox)
{

}



void CameraMan::trackTwo(const dvec2 &A, const dvec2 & B, const dvec2 &dA, const dvec2 &dB)
{
  auto Anext = A + dA * duration;
  auto Bnext = B + dB * duration;
  auto predictedBox = mkbox(Anext,Bnext);

  // to make the move extra soft and fluffy, tween the velocities, not
  // the positions.
  // But we cannot use the velocities of A and B directly -- this
  // would make no sense, since we used them to obtain our prediction
  // in the first place. The whole prediction is based on the
  // assumption that the velocities *do not change*.
  // What we need to ease instead is the movement of the things that
  // are derived from the center position.
  // For this, we create a (component-wise) time derivate of the current box

  auto bottomLeft = (predictedBox.bottomLeft() - currentBox.bottomLeft()) * (1.0 / duration);
  auto topRight = (predictedBox.topRight() - currentBox.topRight()) * (1.0 / duration);

  // this is not a real box, but the rate at which the box has to change to eventually match the prediction.
  Box desiredVelocity(bottomLeft, topRight);

  // next, we tween not the currentBox, but the velocityBox instead!
  velocityBox = tween(velocityBox, desiredVelocity, 0.1);

  // now that we know the rate of change for the box, we can modify the actual box

  bottomLeft = currentBox.bottomLeft() + timestep * velocityBox.bottomLeft();
  topRight = currentBox.topRight() + timestep * velocityBox.topRight();
  currentBox = Box(bottomLeft, topRight);

  // finally, all absolute positions need to be normalized

  normalize();
}

cmd_look_at CameraMan::track(double t, const Blip *blipA, const Blip *blipB)
{
  updatePositions(blipA,blipB);
  trackTwo(A,B,dA,dB);

/*
  plotter.at(t)
      .line("A",A.x)
      .line("B",B.x, -1, 2)
      .interval("currentBox",currentBox.left,currentBox.right)
      .flush();
*/

  return lookat(currentBox);
}


cmd_look_at CameraMan::track(double t, const Blip *bA)
{
  updatePosition(bA);
  if(bA->hints & HINT_PLAYER_B){
    trackTwo(B,B,dB,dB);
  }
  else {
    trackTwo(A,A,dA,dA);
  }
  return lookat(currentBox);
}

cmd_look_at CameraMan::total(double t)
{
  auto predictedBox = worldBox;
  currentBox =tween(currentBox,predictedBox,timestep/duration);
  return lookat(currentBox);
}

void CameraMan::reset(double t)
{
  A = {0,0};
  B = {0,0};
  dA = {0,0};
  dB = {0,0};
  initializedA=false;
  initializedB=false;
}

void CameraMan::onExit()
{
  plotter.close();
}


void CameraMan::updatePositions(const Blip *blipA, const Blip *blipB)
{

  updatePosition(blipA);
  updatePosition(blipB);

  // flip a coin
  bool coin = std::rand() > RAND_MAX/2;
  // switch perspective if we enter our set margin
  if(worldBox.width() - width(A,B) < margin) {
    if(!hflipped){
      if(coin){
        hflip(&A,&B);
      }
      else {
        hflip(&B,&A);
      }
      hflipped = true;
    }
  } else {
    hflipped = false;
  }
  if(worldBox.height()-height(A,B) < margin) {
    if(!vflipped){
      if(coin){
        vflip(&A,&B);
      }
      else {
        vflip(&B,&A);
      }
      vflipped=true;
    }
  }
  else {
    vflipped = false;
  }
}

void CameraMan::updatePosition(const Blip *blip)
{
  dvec2 *pos, *vel;
  bool *initialized;

  if(blip->hints & HINT_PLAYER_B){
    pos = &B;
    vel = &dB;
    initialized = &initializedB;
  }
  else {
    pos = &A;
    vel = &dA;
    initialized = &initializedA;
  }

  *vel = velocity(blip);
  if(*initialized){
    *pos += timestep * *vel;
  }
  else {
    *pos = position(blip);
    *initialized=true;
  }
}

void CameraMan::normalize()
{
   auto C = mid(A,B);
   auto diff = C % worldBox - C;
   A += diff;
   B += diff;
   currentBox = currentBox.translate(diff);
}

void CameraMan::hflip(glm::dvec2 *A, glm::dvec2 *B)
{
  if(A->x < B->x){
    B->x -= worldBox.width();
  }
  else {
    B->x += worldBox.width();
  }
}

void CameraMan::vflip(glm::dvec2 *A, glm::dvec2 *B)
{
  if(A->y < B->y){
    B->y -= worldBox.height();
  }
  else {
    B->y += worldBox.height();
  }
}

Box CameraMan::tween(const Box &start, const Box &target, double t){
  //cout<<"t: "<<t<<endl;
  return Box(tween(start.left, target.left, t),
             tween(start.bottom, target.bottom, t),
             tween(start.right, target.right, t),
             tween(start.top, target.top, t));
}

double CameraMan::tween(double start, double target, double t)
{
  //double ease = t*t / (t*t +(1.0-t)*(1.0-t));
  return start + (target - start) * t;
}

pair<dvec2*,dvec2*> CameraMan::sortX(dvec2 * A, dvec2 * B){
  if(A->x < B->x){
    return {A,B};
  }
  else {
    return {B,A};
  }
}
pair<dvec2*,dvec2*> CameraMan::sortY(dvec2 * A, dvec2 * B){
  if(A->y < B->y){
    return {A,B};
  }
  else {
    return {B,A};
  }
}


Box CameraMan::mkbox(const glm::dvec2 &A, const glm::dvec2 &B)
{
  Box b;
  b.include(A);
  b.include(B);
  return b;
}

Box CameraMan::mkbox(const glm::dvec2 &A, double r)
{
  Box b(A);
  b.grow(r);
  return b;
}

glm::dvec2 CameraMan::position(const Blip *b)
{
  return {b->state.x, b->state.y};
}

glm::dvec2 CameraMan::velocity(const Blip *b)
{
  return {b->state.dx, b->state.dy};
}

double CameraMan::width(const glm::dvec2 &A, const glm::dvec2 &B)
{
  return abs(A.x-B.x);
}
double CameraMan::height(const glm::dvec2 &A, const glm::dvec2 &B)
{
  return abs(A.y-B.y);
}

glm::dvec2 CameraMan::mid(const glm::dvec2 &A, const glm::dvec2 &B)
{
  return 0.5*(A+B);
}

cmd_look_at CameraMan::lookat(const Box &extent)
{
  auto w =min(worldBox.width(), extent.width()+margin);
  auto h =min(worldBox.height(), extent.height()+margin);
  auto center = extent.center();
  cmd_look_at c;
  c.top=center.y + 0.5*h;
  c.left=center.x - 0.5*w;
  c.right=center.x + 0.5*w;
  c.bottom=center.y - 0.5*h;
  return c;
}

