#include "shatter.h"
#include "bbox.hpp"
#include "point-on-bbox.hpp"

#include <boost/polygon/voronoi.hpp>
#include <clipper.hpp>
#include <glm/gtc/random.hpp>

using boost::polygon::voronoi_builder;
using boost::polygon::voronoi_diagram;
using boost::polygon::x;
using boost::polygon::y;
using boost::polygon::low;
using boost::polygon::high;
using ClipperLib::cInt;
using ClipperLib::Clipper;
using ClipperLib::ptClip;
using ClipperLib::ptSubject;
using ClipperLib::ctIntersection;
using ClipperLib::pftEvenOdd;
using std::vector;

//#include "voronoi_visual_utils.hpp"
#include <iostream>

struct Segment {
  Point p0;
  Point p1;
  Segment(int x1, int y1, int x2, int y2) : p0(x1, y1), p1(x2, y2) {}
};

namespace boost {
  namespace polygon {

    template <>
      struct geometry_concept<Point> {
        typedef point_concept type;
      };

    template <>
      struct point_traits<Point> {
        typedef int coordinate_type;

        static inline coordinate_type get(
            const Point& point, orientation_2d orient) {
          return (orient == HORIZONTAL) ? point.X : point.Y;
        }
      };

    template <>
      struct geometry_concept<Segment> {
        typedef segment_concept type;
      };

    template <>
      struct segment_traits<Segment> {
        typedef int coordinate_type;
        typedef Point point_type;

        static inline point_type get(const Segment& segment, direction_1d dir) {
          return dir.to_int() ? segment.p1 : segment.p0;
        }
      };
  }  // polygon
}  // boost

typedef voronoi_diagram<double>::vertex_type vertex_type;
typedef voronoi_diagram<double>::edge_type edge_type;
typedef voronoi_diagram<double>::cell_type cell_type;
typedef voronoi_diagram<double>::const_cell_iterator cell_iterator;

template <typename T>
struct Ray {
  glm::tvec2<T> c;
  glm::tvec2<T> v;
};

/* returns a point and a direction, i.e. a ray.
 *
 * The point will be the midpoint between the point features associated with the two adjacent cells.
 * The direction will be a vector of unspecified length but colinear to the edge.
 *
 * Note that this way we can define the ray without refering to the vertices of the edge. So this
 * should work even for edges that are unbound on both sides.
 */
template <typename T>
Ray<T> rayFromEdge( const edge_type * edge, const vector<Point> & sites){
  //winding order is ccw, so the site associated with my own cell is to the left
  //Note the implicit typecast!!
  const Point leftPoint = sites[edge->cell()->source_index()];
  const Point rightPoint = sites[edge->twin()->cell()->source_index()];
  const glm::tvec2<T> leftSite {leftPoint.X, leftPoint.Y};
  const glm::tvec2<T> rightSite {rightPoint.X, rightPoint.Y};
  const glm::tvec2<T> c = (rightSite + leftSite) * 0.5;
  const glm::tvec2<T> D = rightSite - leftSite;
  const glm::tvec2<T> v {-D.y, D.x};
  return Ray<T>{c,v};
}

void appendToLoop(vector<Point> & loopVertices,  const edge_type * edge, const vector<Point> & sites, const Bbox<double> & bbox){
  // we are only interested in the second vertex of the edge, i.e.
  // the one it is "pointing to". 
  const vertex_type * v = edge->vertex1();

  // if it exists, life is easy. Just add it to the loop.
  if(v) {
    loopVertices.push_back({(int)v->x(), (int)v->y()}); 
  }
  //else {
    //loopVertices.clear(); 
  //}
  else{
    //std::cout<<std::endl<<"infinit edge"<<std::endl;
    // If v1 is null it means that the edge is infinite.
    // The good news is: 
    //   - we know that the next edge will be also infinite -- if this one is "going to" infinity,
    //     the next one is "coming from" infinity. 
    // We intersect both rays with the bounding box. Then we add the intersections, filling in 
    // parts of the bounding box as necessary.
    //
    //
    // // First, we need to determine the directions of both rays, so we can determine the
    // intersection points.
    Ray<double> goingOut = rayFromEdge<double>(edge, sites);
    Ray<double> comingIn = rayFromEdge<double>(edge->next(), sites);

    PointOnBbox first = PointOnBbox::project<double>(bbox, goingOut.c, goingOut.v);
    PointOnBbox last = PointOnBbox::project<double>(bbox, comingIn.c, -comingIn.v);

    // add the first intersection point plus intermediate bounding box vertices 
    // if necessary
    for( PointOnBbox p = first; p != last; p = p.next(last) ) {
      glm::dvec2 x { p.position<double>(bbox)};
      loopVertices.push_back({(cInt) x.x, (cInt) x.y});
    }

    // add the last intersection point

    glm::dvec2 x = last.position<double>(bbox);
    loopVertices.push_back({(cInt) x.x, (cInt) x.y});
  }
}

Shape createRegionShape(const Shape & orig, vector<Point> intVertices, double pixelsPerUnit){
  vector<dvec2> vertices;
  for(auto p: intVertices){
    vertices.push_back({(double)p.X / pixelsPerUnit, (double)p.Y / pixelsPerUnit}); 
  }
  return Shape(vertices, 
      orig.density(), 
      orig.friction(), 
      orig.elasticity(), 
      orig.hints());
}

vector<Shape> Shatter::shatter(const Shape & s, int N, const glm::dvec2 & mean, const double deviation){
  vector<glm::dvec2> points;
  points.reserve(N);


  for(int i=0;i<N; i++){
    double x = glm::gaussRand(mean.x , deviation );
    double y = glm::gaussRand(mean.y , deviation );
    points.push_back({x, y});
  }
  auto regions = shatter(s, points);
  return regions;
}
vector<Shape> Shatter::shatter(const Shape & s, const vector<Point> & points){

  Bbox<double> bbox(points[0].X, points[0].Y);

  vector<Point> contour;

  for(auto p : points){
    bbox.include(p.X, p.Y); 
  }

  for(auto v : s.vertices()) {
    Point p {(int) (v.x * pixelsPerUnit), (int) (v.y * pixelsPerUnit)} ;
    bbox.include(p.X, p.Y); 
    contour.push_back(std::move(p));
  }

  vector<vector<Point>> regionLoops {shatter(contour, points, bbox)};
  vector<Shape> regionShapes;
  regionShapes.reserve(regionLoops.size());

  for(auto loopVertices : regionLoops){
    regionShapes.push_back(createRegionShape(s, loopVertices, pixelsPerUnit));
  }
  return regionShapes;

}

std::vector<Shape> Shatter::shatter(const Shape &s, const std::vector<glm::dvec2> &input)
{
  vector<Point> points;
  points.reserve(input.size());

  for(auto v : input){
    Point p {static_cast<int>(v.x * pixelsPerUnit), static_cast<int>(v.y * pixelsPerUnit)};
    points.push_back(p);
  }
  return shatter(s, points);
}

template<>
dvec2 asVector(const Point & p){
  return {p.X,p.Y};
}
vector<vector<Point>> Shatter::shatter(const std::vector<Point> & contour, const std::vector<Point> & points, const Bbox<double> & bbox0){ 

#ifdef ENABLE_OBJECT_BUILDER
  objBuilder.reset();
  objBuilder.group("orig").loop(contour);
  objBuilder.group("features").vertices(points);
#endif

  // this is where we collect the resulting fragment shapes in
  vector<vector<Point>> loops;

  // begin by creating a voronoi diagram from the given feature points
  voronoi_diagram<double> vd;
  construct_voronoi(points.begin(), points.end(), &vd);

  // bbox0 includes all the feature points, but we need one that also includes
  // all voronoi vertices
  Bbox bbox {bbox0};
  for (auto it = vd.vertices().begin(); it != vd.vertices().end(); ++it) {
    bbox.include(it->x(), it->y());
  }

  Clipper clpr;
#ifdef ENABLE_OBJECT_BUILDER
  auto & unclippedNode = objBuilder.group("unclipped");
  auto & clippedNode = objBuilder.group("clipped");
#endif
  //For each of the voronoi cells
  for (cell_iterator it = vd.cells().begin(); it != vd.cells().end(); ++it) {
    const cell_type & cell = *it;
    // ignore devenerate cells. (not sure if there can be any in our case)
    if(cell.is_degenerate()){
      continue; 
    }
    // pick any edge from the cell boundary. Which one is not important.
    const edge_type * edge = cell.incident_edge();

    // starting from this edge, loop around the cell and collect
    // all vertices.
    vector<Point> loopVertices; 
    do {
      // since we only have point features, there should only be primary edges
      if (edge->is_primary()){
        // append vertex this edge is "going to". If it is going to infinity, use the
        // subsequent edge (which is guaranteed to "come back from infinity" and fill in
        // intersections with the bbox as necessary
        appendToLoop(loopVertices, edge, points, bbox);

        if(loopVertices.empty()){
          // i think this cannot happen, but if it does, break from the loop
          break; 
        }
      } else {
        // this would be very odd.
        throw "w00t?!" ;
      }
      edge = edge->next();

      // continue until we arrived at the original edge
    } while (edge != cell.incident_edge());

    // the loop could be empty for degenerate cells. I don't think
    // we have those, though.
    if(!loopVertices.empty()){
      // use the clipper library to clip away any part of the cell that
      // lies outside the contour of the original shape.
      // Though highly unlikely, clipping could in theory result in more
      // than one loop, since the "asteroid" shapes we create do not need to be
      // convex.
      clpr.Clear();
      clpr.AddPath(loopVertices, ptSubject, true);
      clpr.AddPath(contour, ptClip, true);
#ifdef ENABLE_OBJECT_BUILDER
      unclippedNode.loop(loopVertices);
#endif
      vector<vector<Point>> clippedLoops;

      clpr.Execute(ctIntersection, clippedLoops, pftEvenOdd, pftEvenOdd);
#ifdef ENABLE_OBJECT_BUILDER
      for(auto & clippedLoop : clippedLoops){
        clippedNode.loop(clippedLoop);
      }
#endif

      // finally, add the clipped loops to our list.
      loops.insert(loops.end(), clippedLoops.begin(), clippedLoops.end());
    }
  } // END For each voronoi cell

  return loops;
}

void Shatter::__debug__flush(ostream &out)
{
#ifdef ENABLE_OBJECT_BUILDER
  objBuilder.write(out);
#endif
}
