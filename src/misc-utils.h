#ifndef MISCUTILS_H
#define MISCUTILS_H
#include "flags.h"

#include <iostream>

template<typename ... Args>
void debug(const std::string&  fmt, Args ... args ){
  fprintf(stderr, (fmt+"\n").c_str(), args...);
}

template<typename ... Args>
void info(const std::string & fmt, Args ... args ){
  fprintf(stderr, (fmt+"\n").c_str(), args...);
}

template<typename ... Args>
void warn(const std::string & fmt, Args ... args ){
  fprintf(stderr, (fmt+"\n").c_str(), args...);
}

template<typename ... Args>
void error(const std::string & fmt, Args ... args ){
  fprintf(stderr, (fmt+"\n").c_str(), args...);
}

template<typename ... Args>
void output(const std::string & fmt, Args ... args ){
  fprintf(stdout, (fmt+"\n").c_str(), args...);
}

#ifdef GL_CHECK_ERROR
#include <glad/glad.h>
static GLenum glCheckError_(const char *file, int line)
{
    GLenum errorCode;
    while ((errorCode = glGetError()) != GL_NO_ERROR)
    {
        std::string errorStr;
        switch (errorCode)
        {
            case GL_INVALID_ENUM:                  errorStr = "INVALID_ENUM"; break;
            case GL_INVALID_VALUE:                 errorStr = "INVALID_VALUE"; break;
            case GL_INVALID_OPERATION:             errorStr = "INVALID_OPERATION"; break;
            case GL_STACK_OVERFLOW:                errorStr = "STACK_OVERFLOW"; break;
            case GL_STACK_UNDERFLOW:               errorStr = "STACK_UNDERFLOW"; break;
            case GL_OUT_OF_MEMORY:                 errorStr = "OUT_OF_MEMORY"; break;
            case GL_INVALID_FRAMEBUFFER_OPERATION: errorStr = "INVALID_FRAMEBUFFER_OPERATION"; break;
        }

        error("%s | %s (%d)",errorStr.c_str(), file, line);
    }
    return errorCode;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__)
#else
//#define glCheckError()
#endif


#endif // MISCUTILS_H
