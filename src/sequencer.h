#ifndef SEQUENCER_H
#define SEQUENCER_H

#include <map>
#include <functional>


class Sequencer
{
    std::multimap<double, std::function<void()>> queue;
  public:
    Sequencer();
    void schedule(double t, std::function<void()> f);
    void executePending(double t);
};

#endif // SEQUENCER_H
