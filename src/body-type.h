#ifndef BODY_TYPE_H
#define BODY_TYPE_H
#include "shape.h"
#include <vector>

class BodyType {
    mutable double M = 0.0;
    mutable double J = 0.0;
    mutable double A = 0.0;
    mutable glm::dvec2 C {0,0};

    std::vector<Shape> _shapes;
    uint32_t _tid;
    double _r;
  public:
    BodyType();
    BodyType(uint32_t tid, const std::vector<Shape> & shapes);
    double r() const;
    const std::vector<Shape> & shapes() const;
    uint32_t tid() const;
    double mass() const;
    double area() const;
    double momentOfInertia() const;
    glm::dvec2 centerOfGravity() const;

  private:

    void calc() const;
};
#endif
