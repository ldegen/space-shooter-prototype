#ifndef BBOX_H
#define BBOX_H

#include <glm/glm.hpp>
#include <vector>


#define BBOX_BOTTOM     1
#define BBOX_RIGHT      2
#define BBOX_TOP        3
#define BBOX_LEFT       4

template <typename T>
class Bbox {
  private:
  bool undefined=true;
  public:

  bool isDefined() const {
    return !undefined; 
  }
  T left;
  T bottom;
  T right;
  T top;
 

  Bbox ()
  {}
  Bbox (const glm::tvec2<T> & v, const glm::tvec2<T> & w)
    : Bbox {v.x, v.y, w.x, w.y}
  {}
  Bbox (const glm::tvec2<T> & v)
    : Bbox {v.x, v.y}
  {}
  Bbox (T x, T y)
    : Bbox {x, y, x, y}
    {}
  Bbox (T left, T bottom, T right, T top)
    : left {left}
    , bottom {bottom}
    , right {right}
    , top {top}
    , undefined{false}
    {}

  //void operator =(const Bbox<T> & other){
  //  left=other.left;
  //  bottom=other.bottom;
  //  right=other.right;
  //  top=other.top;
  //}
  void include(const glm::tvec2<T> & p){
    include(p.x, p.y); 
  }
  void include(T x, T y) {
    if (undefined || x<left){
      left = x; 
    } 
    if (undefined || x>right){
      right = x; 
    }
    if (undefined || y<bottom){
      bottom = y;
    }
    if (undefined || y>top){
      top=y; 
    }
    undefined = false;
  } 

  glm::tvec2<T> center() const {
    return {0.5 * (left + right), 0.5 * (top + bottom)}; 
  }
  glm::tvec2<T> bottomLeft() const {
    return {left, bottom}; 
  }
  glm::tvec2<T> topRight() const {
    return {right, top}; 
  }
  void fitAspectRatio(double a){
    double a0 = (double)width()/(double)height(); 
    if(a0 < a){
      double x = a * (double)height() - (double)width();
      left -= x/2.0;
      right += x/2.0;
    } else if (a0 > a){
      double y = (double)width() / a - (double) height();
      bottom -= y/2.0;
      top += y/2.0;
    }
  }

  void clip(Bbox<T> c){
    top = std::min(c.top, top);
    right = std::min(c.right, right);
    left = std::max(c.left, left);
    bottom = std::max(c.bottom, bottom);
  }

  void resizeTo(T desiredWidth, T desiredHeight){
    T w = width();
    T h = height();
    if(w != desiredWidth){
      T diff = w-desiredWidth; 
      left += diff/2.0;
      right -= diff/2.0;
    }
    if(h != desiredHeight){
      T diff = h - desiredHeight;
      bottom += diff/2.0;
      top -= diff/2.0;
    }
  }
  void shrinkTo(T maxWidth, T maxHeight){
    T w = width();
    T h = height();
    if(w > maxWidth){
      T diff = w-maxWidth; 
      left += diff/2.0;
      right -= diff/2.0;
    }
    if(h > maxHeight){
      T diff = h - maxHeight;
      bottom += diff/2.0;
      top -= diff/2.0;
    }
  }

  void grow(T d){
    if(undefined){
      return; 
    } 
    left -= d;
    right += d;
    top += d;
    bottom -=d;
  }

  Bbox<T> & operator =(const Bbox<T> & b){
    left=b.left;
    right=b.right;
    bottom=b.bottom;
    top=b.top;
    undefined = b.undefined;
    return *this;
  }


  Bbox<T> translate(glm::tvec2<T> p) const{
    return translate(p.x, p.y);
  }
  Bbox<T> translate(T x, T y) const {
    if(undefined){
      return {}; 
    }
    return {
      left + x,
      bottom + y,
      right + x,
      top + y
    }; 
  }

  bool contains(glm::tvec2<T> p){
    return contains(p.x,p.y);
  }
  bool contains(T x, T y) const {
    if(undefined){
      return false; 
    }
    return left <= x && x <= right && bottom <= y && y <= top; 
  }

  bool maybeIntersecting(T x, T y, T r) const {
    if(undefined){
      return false;
    }
    return x - r <= right && x + r >= left
      &&  y - r <= top && y + r >= bottom;
  }

  int onBorder(T x, T y, T EPSILON) const{
    return _onBorder(x,y, EPSILON); 
  }
  int onBorder(T x, T y) const{
    return _onBorder(x,y); 
  }

  T width() const {
    if(undefined){
      return 0; 
    }
    return right - left; 
  }
  
  T height() const {
    if(undefined){
      return 0; 
    }
    return top - bottom; 
  }

  private:

  template<typename TL>
  int _onBorder(TL x, TL y, TL EPSILON=0.00000001) const {
    if(undefined){
      return 0; 
    }
    if (fabs(x - left) < EPSILON){
      return BBOX_LEFT;
    }
    else if (fabs(x - right) < EPSILON){
      return BBOX_RIGHT;
    }
    else if (fabs(y - top) < EPSILON){
      return BBOX_TOP;
    }
    else if (fabs(y - bottom) < EPSILON){
      return BBOX_BOTTOM;
    }

    return 0;
  }

  int _onBorder(int x, int y, int IGNORED=0) const{
    if(undefined){
      return 0; 
    }
    if (x==left){
      return BBOX_LEFT;
    }
    else if (x == right){
      return BBOX_RIGHT;
    }
    else if (y == top){
      return BBOX_TOP;
    }
    else if (y == bottom){
      return BBOX_BOTTOM;
    }

    return 0;
  }
};

template<typename T>
bool operator ==(const Bbox<T> & a, const Bbox<T> & b){
  return a.top == b.top
    && a.left == b.left
    && a.bottom == b.bottom
    && a.right == b.right;
}

template<typename T>
glm::tvec2<T> operator %(const glm::tvec2<T>& point, const Bbox<T>& grid){
  if(!grid.isDefined()){
    return {point};
  }
  T offsetX = fmod(point.x - grid.left, grid.width()); 
  T offsetY = fmod(point.y - grid.bottom, grid.height()); 
  if (offsetX < 0){
    offsetX += grid.width(); 
  }
  if (offsetY < 0){
    offsetY += grid.height(); 
  }
  T x = grid.left + offsetX;
  T y = grid.bottom + offsetY;
  return {x,y};
}

template<typename T>
Bbox<T> operator %(const Bbox<T> & subject, const Bbox<T> & grid){
  if(!grid.isDefined()){
    return {}; 
  }
  glm::tvec2<T> p {subject.left, subject.bottom};
  glm::tvec2<T> q = p % grid;
    
  return {q.x,q.y,q.x+subject.width(), q.y+subject.height()};
}
/*
template <typename T>
T min(const T & a, const T & b){
  return (a<b) ? a : b;
}
*/

/* this assumes that box0 is *not* larger then grid.
 *
 * This function will start by "normalizing" the given box, i.e. 
 * translate (in grid-steps) so that its lower left corner lies within
 * the given grid cell.
 * It will then "cut" the box at the borders of the grid cell and return
 * the resulting boxes. The first one will always be the lower-left one.
 * It is guaranteed to lie completely within the grid cell.
 * There may be up to three other boxes. They are outside the grid cell.
 */
template <typename T>
std::vector<Bbox<T>> split(const Bbox<T> & box0, const Bbox<T> & grid){
  if(!grid.isDefined()){
    return {} ;
  }
  std::vector<Bbox<T>> facets;
  facets.reserve(4);

  // start by "normalizing" the box.
  // After this, the lower left corner of box lies within grid
  Bbox<T> box = box0 % grid;
  // remember the offset of the two boxes, as we need to substract it from the result
  glm::tvec2<T> offset = box.bottomLeft() - box0.bottomLeft();

  facets.push_back({box.left, box.bottom, std::min(box.right, grid.right), std::min(box.top, grid.top)});

  if(box.right > grid.right){
    facets.push_back({grid.right, box.bottom, box.right, std::min(box.top, grid.top)}); 
  }

  if(box.top > grid.top){
    facets.push_back({box.left, grid.top, std::min(box.right, grid.right), box.top}); 
  }

  if(box.right > grid.right && box.top > grid.top){
    facets.push_back({grid.right, grid.top, box.right, box.top}); 
  }
  for(int i=0;i<facets.size();i++){
    facets[i] = facets[i].translate(-offset); 
  }

  return facets;
}

template <typename T>
std::vector<Bbox<T>> wrap(const Bbox<T> & box, const Bbox<T> & grid){
  if(!grid.isDefined()){
    return {} ;
  }
  std::vector<Bbox<T>> facets0 = split(box, grid);
  std::vector<Bbox<T>> facets;
  facets.reserve(facets0.size());
  for(auto f : facets0){
    facets.push_back(f % grid); 
  }
  return facets;
}


template <typename T>
class zoom {
  public:
  const T scale;
  const glm::tvec2<T> offset;
  
  zoom()
    : scale {1}
    , offset {0,0}
    {}
  zoom(const T & scale, const glm::tvec2<T> & offset)
    : scale {scale}
    , offset {offset} {}

  glm::tvec2<T> inverseTransform(const glm::tvec2<T> & p){
    return (p-offset)/scale;
  }
  glm::tvec2<T> transform(const glm::tvec2<T> & p){
    return offset + scale * p;
  }

  Bbox<T> transform(const Bbox<T> & box){
    return {
      offset.x + scale * box.left, 
      offset.y + scale * box.bottom, 
      offset.x + scale * box.right, 
      offset.y + scale * box.top, 
    };
  }

};

template <typename T>
zoom<T> fit(const Bbox<T> & extent, const Bbox<T> & viewport){
  if(!extent.isDefined() || !viewport.isDefined()){
    return {} ;
  }
  T scale = 1.0 / std::max(extent.width()/viewport.width(), extent.height() / viewport.height());
  glm::tvec2<T> centerOfExtent {(extent.left + extent.right)/2.0, (extent.top + extent.bottom)/2.0};
  glm::tvec2<T> centerOfViewport {(viewport.left + viewport.right)/2.0, (viewport.top + viewport.bottom)/2.0};
  glm::tvec2<T> offset = centerOfViewport - scale * centerOfExtent;
  
  return zoom<T> {scale, offset};
}

#endif
