#include "protocol.h"
#include "flags.h"
#include "misc-utils.h"
#include <iostream>
template <typename T> 
inline std::optional<size_t> dispatch(ProtocolListener & listener, const uint8_t * data, size_t size){
#ifdef DEBUG_PROTOCOL
  debug("decoding %s", typeid(T).name());
#endif
  if(size < sizeof(T)){
    return 0; 
  }
  listener.onMessage((const T *) data);
  return sizeof(T);
}

ProtocolParser::ProtocolParser(ProtocolListener & listener)
  : listener {listener}
{}

std::optional<size_t> ProtocolParser::readMessage(const uint8_t * buffer, size_t size ){
  //std::cout<<"size: "<<size<<std::endl;
  if(remainingContacts > 0){
    //std::cout<<"reading a contact"<<std::endl;
    return readContact(buffer, size);
  }
  if(remainingVertices > 0){
    //std::cout<<"reading a vertex"<<std::endl;
    return readVertex(buffer, size);
  }
  else if(remainingShapes > 0){
    //std::cout<<"reading a shape header"<<std::endl;
    return readShape(buffer, size);
  }
  else if(remainingBlips > 0){
    //std::cout<<"reading a blip"<<std::endl;
    return readBlip(buffer, size);
  }
  else if(remainingTags > 0){
    //std::cout<<"reading a tag"<<std::endl;
    return readTag(buffer, size);
  }
  else{
    //std::cout << "message type: "<<(unsigned int) *buffer <<std::endl;
    switch(*buffer){
      case MSG_INIT:                 return dispatch<msg_init>(listener, buffer, size);
      case CMD_PAUSE:                return dispatch<cmd_pause>(listener, buffer, size);
      case CMD_QUIT:                 return dispatch<cmd_quit>(listener, buffer, size);
      case MSG_CLOSE_REQUESTED:      return dispatch<msg_close_requested>(listener, buffer, size);
      case CMD_RESUME:               return dispatch<cmd_resume>(listener, buffer, size);
      case CMD_TOGGLE_PAUSE:         return dispatch<cmd_toggle_pause>(listener, buffer, size);
      case CMD_PLAY_SOUND:           return dispatch<cmd_play_sound>(listener, buffer, size);
      case MSG_WINDOW_RESIZED:       return dispatch<msg_window_resized>(listener, buffer, size);
      case MSG_BODY_CREATED:         return dispatch<msg_body_created>(listener, buffer, size);
      case MSG_BODY_DELETED:         return dispatch<msg_body_deleted>(listener, buffer, size);
      case MSG_BUTTON_STATE_CHANGED: return dispatch<msg_button_state_changed>(listener, buffer, size);
      case MSG_ERROR:                return dispatch<msg_error>(listener, buffer, size);
      case CMD_CREATE:               return dispatch<cmd_create>(listener, buffer, size);
      case CMD_DELETE:               return dispatch<cmd_delete>(listener, buffer, size);
      case CMD_SELECT_UID:           return dispatch<cmd_select_uid>(listener, buffer, size);
      case CMD_SET_X_Y_THETA:        return dispatch<cmd_set_x_y_theta>(listener, buffer, size);
      case CMD_SET_DX_DY_DTHETA:     return dispatch<cmd_set_dx_dy_dtheta>(listener, buffer, size);
      case CMD_SET_DTHETA:           return dispatch<cmd_set_dtheta>(listener, buffer, size);
      case CMD_SET_FX_FY_TORQUE:     return dispatch<cmd_set_fx_fy_torque>(listener, buffer, size);
      case CMD_APPLY_IMPULSE:        return dispatch<cmd_apply_impulse>(listener, buffer, size);
      case CMD_LOOK_AT:              return dispatch<cmd_look_at>(listener, buffer, size);
      case CMD_TAG:                  return dispatch<cmd_tag>(listener, buffer, size);
      case CMD_HINT:                 return dispatch<cmd_hint>(listener, buffer, size);
      case CMD_CREATE_BODY_TYPE:     return readBodyType(buffer, size);
      case MSG_BLIPS:                return readBlips(buffer, size);
      case MSG_COLLISION:            return readCollision(buffer, size);
    }
  }
  error("bad message type: %u", static_cast<unsigned int>(*buffer));
  return {};
}
void ProtocolParser::flushBlips(){
#ifdef DEBUG_PROTOCOL
  debug("flush blips");
#endif
  listener.onBlips(blipsHeader, blips.data(), tags.data());
  blips.clear();
  tags.clear();
}
void ProtocolParser::flushBodyType(){
#ifdef DEBUG_PROTOCOL
  debug("flush body type");
#endif
  listener.onBodyType({btHeader.tid, std::move(shapes)});
  shapes.clear();
};
void ProtocolParser::flushCollision(){
#ifdef DEBUG_PROTOCOL
  debug("flush collision");
#endif
  listener.onCollision(collisionHeader, contacts.data());
  contacts.clear();
};
void ProtocolParser::flushShape(){
#ifdef DEBUG_PROTOCOL
  debug("flush shape");
#endif
  shapes.push_back({
      std::move(vertices), 
      spHeader.density, 
      spHeader.friction,
      spHeader.elasticity,
      spHeader.hints
      });
  vertices.clear();
  remainingShapes--;
  if(remainingShapes == 0){
    flushBodyType(); 
  }
};
std::optional<size_t> ProtocolParser::readBlips(const uint8_t * buffer0, size_t size ){
#ifdef DEBUG_PROTOCOL
  debug("decoding msg_blips");
#endif
  if(size < sizeof(msg_blips)){
    return 0; 
  }
  blipsHeader = *(msg_blips *)buffer0;
  remainingBlips = blipsHeader.num_blips;
  remainingTags = blipsHeader.num_tags;
#ifdef DEBUG_PROTOCOL
  debug("num_blips: %u, num_tags: %u, t: ", remainingBlips, remainingTags, blipsHeader.t);
#endif
  if(remainingBlips==0){
    flushBlips();
  }
  blips.reserve(remainingBlips);
  return sizeof(msg_blips);
}
std::optional<size_t> ProtocolParser::readBlip(const uint8_t * buffer0, size_t size ){
#ifdef DEBUG_PROTOCOL
  debug("decoding Blip");
#endif
  if(size < sizeof(Blip)){
    return 0;
  }
  Blip * b = (Blip *) buffer0;
  blips.push_back(*b);
  remainingBlips--;
  if(remainingBlips == 0 && remainingTags == 0){
    flushBlips(); 
  }
  return sizeof(Blip);
}
std::optional<size_t> ProtocolParser::readTag(const uint8_t * buffer0, size_t size ){
#ifdef DEBUG_PROTOCOL
  debug("decoding data_tag");
#endif
  if(size < sizeof(data_tag)){
    return 0;
  }
  data_tag * b = (data_tag *) buffer0;
  tags.push_back(*b);
  remainingTags--;
  if(remainingTags == 0){
    flushBlips(); 
  }
  return sizeof(data_tag);
}

std::optional<size_t> ProtocolParser::readVertex(const uint8_t * buffer0, size_t size ){
#ifdef DEBUG_PROTOCOL
  debug("decoding data_vertex");
#endif
  if(size < sizeof(data_vertex)){
    return 0; 
  }
  data_vertex * v =(data_vertex *) buffer0;
  vertices.push_back({v->x,v->y});
  remainingVertices--;
  if(remainingVertices == 0){
    flushShape(); 
  }
  return sizeof(data_vertex);
}
std::optional<size_t> ProtocolParser::readShape(const uint8_t * buffer0, size_t size ){
#ifdef DEBUG_PROTOCOL
  debug("decoding data_shape");
#endif
  if(size < sizeof(data_shape)){
    return 0; 
  }
  spHeader = *(data_shape *)buffer0;
  remainingVertices = spHeader.num_vertices;
  vertices.reserve(remainingVertices);
#ifdef DEBUG_PROTOCOL
  debug("num_vertices: %u",remainingVertices);
#endif
  return sizeof(data_shape);
}
std::optional<size_t> ProtocolParser::readCollision( const uint8_t * buffer0, size_t size ){
#ifdef DEBUG_PROTOCOL
  debug("decoding msg_collision");

#endif
  if(size < sizeof(msg_collision)){
    return 0; 
  }
  collisionHeader = *(msg_collision *)buffer0;
  remainingContacts = collisionHeader.num_contacts;
#ifdef DEBUG_PROTOCOL
  debug("num_contacts: %u",remainingContacts);
#endif
  contacts.reserve(remainingContacts);
  return sizeof(msg_collision);
}
std::optional<size_t> ProtocolParser::readContact( const uint8_t * buffer0, size_t size ){
#ifdef DEBUG_PROTOCOL
  debug("decoding data_contact");
#endif
  if(size < sizeof(data_contact)){
    return 0; 
  }
  data_contact * c =(data_contact *) buffer0;
  contacts.push_back(*c);
  remainingContacts--;
  if(remainingContacts == 0){
    flushCollision(); 
  }
  return sizeof(data_contact);
}
std::optional<size_t> ProtocolParser::readBodyType(const uint8_t * buffer0, size_t size ){
#ifdef DEBUG_PROTOCOL
  debug("decoding cmd_create_body_type");
#endif
  if(size < sizeof(cmd_create_body_type)){
    return 0; 
  }
  btHeader = *(cmd_create_body_type *)buffer0;
  remainingShapes = btHeader.num_shapes;
#ifdef DEBUG_PROTOCOL
  debug("num_shapes: %u",remainingShapes);
#endif
  shapes.reserve(remainingShapes);
  return sizeof(cmd_create_body_type);
}

std::optional<size_t> ProtocolParser::push(const uint8_t * buffer0, size_t size ){
  size_t bytesRead = 0;
  const uint8_t * buffer = buffer0;
  std::optional<size_t> i;
  do{
    i = readMessage(buffer + bytesRead, size - bytesRead);
    if(!i){
      return {}; 
    }
    bytesRead += *i;
  }while(*i > 0 && size > bytesRead);
  return bytesRead;
}


void ProtocolWriter::onBodyType(const BodyType & bt){
  cmd_create_body_type btHeader {.tid=bt.tid(), .num_shapes=(uint8_t)bt.shapes().size()};
  onMessage(&btHeader);

  for(auto shape : bt.shapes()){
    const std::vector<glm::dvec2> & vertices = shape.vertices();
    data_shape spHeader {.density=shape.density(), .elasticity=shape.elasticity(), .friction=shape.friction(), .hints = shape.hints(), .num_vertices=(uint32_t)vertices.size()};
    onMessage(&spHeader);
    for(auto vertex: vertices){
      data_vertex v {vertex.x, vertex.y};
      onMessage(&v);
    }
  }
};

void ProtocolWriter::onBlips(const msg_blips & header, const Blip * blips, const data_tag * tags){
  onMessage(&header);
  for(int i=0;i<header.num_blips;i++){
    onMessage(blips+i); 
  }
  for(int i=0;i<header.num_tags;i++){
    onMessage(tags+i); 
  }
};

void ProtocolWriter::onCollision(const msg_collision & collision, const data_contact * contacts){
  onMessage(&collision);
  for(int i=0;i<collision.num_contacts;i++){
    onMessage(contacts+i);
  }
}

std::unique_ptr<buffer_t> ProtocolWriter::swap(std::unique_ptr<buffer_t> &&newBuffer)
{
  auto tmp = std::move(buffer);
  buffer = std::move(newBuffer);
  buffer->clear();
  return tmp;
}

  
