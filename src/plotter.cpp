#include "plotter.h"
#include <assert.h>

Plotter::Plotter(const string & name)
  : name(name)
{
}

Plotter::~Plotter()
{
  if(!closed){
    close();
  }
}

Plotter::DataPoint Plotter::at(double t)
{
  assert(!closed);
  if(!data.is_open()){
    data.open(name+".txt");
  }
  assert(data.is_open());
  return DataPoint{*this, t};
}

void Plotter::close()
{
  if(closed){
    return;
  }
  if(data.is_open()){
    data.close();
  }
  ofstream plotFile(name+".plt");
  assert(plotFile.is_open());
  plotFile << "#!/usr/bin/gnuplot -persist" <<endl;
  plotFile << "set style fill transparent pattern 4 border" <<endl;
  plotFile << "plot ";
  auto first = true;
  for (auto pair:columnMap){
    if (first){
      plotFile <<"'"<<name<<".txt' ";
    }else {
      plotFile <<", '' ";
    }
    first = false;

    int lc = pair.second.lc;
    int dt = pair.second.dt;

    if(pair.second.type == coltype::interval){
      plotFile <<"using 1:"<<pair.second.column+1<<":"<<pair.second.column+2
              <<" with filledcurves";
    }
    else {
      plotFile <<"using 1:"<<pair.second.column+1
              <<" with lines";
    }
    if(lc > 0){
      plotFile <<" lc "<<lc;
    }
    if(dt > 0){
      plotFile <<" dt "<<dt;
    }
    plotFile << " title '"<<pair.first<<"'";
  }
  plotFile << endl;
  plotFile.close();
  closed = true;
}

Plotter::DataPoint Plotter::DataPoint::line(const string &key, double val, int lc, int dt) &&
{
  assert(!flushed);
  assert(!plotter.closed);
  auto it = plotter.columnMap.find(key);
  unsigned int column;
  if(it == plotter.columnMap.end()){
    column = plotter.columnCount++;
    plotter.columnMap.insert(make_pair(key,colinfo{coltype::line, column,lc,dt}));
  } else {
    assert(it->second.type==coltype::line);
    column = it->second.column;
  }
  if(values.size() < column+1){
    values.resize(column+1);
  }
  values[column] = val;

  return move(*this);
}

Plotter::DataPoint Plotter::DataPoint::interval(const string &key, double a, double b, int lc, int dt) &&
{
  assert(!flushed);
  assert(!plotter.closed);
  auto it = plotter.columnMap.find(key);
  unsigned int column;
  if(it == plotter.columnMap.end()){
    column = plotter.columnCount;
    plotter.columnCount += 2;
    plotter.columnMap.insert(make_pair(key,colinfo{coltype::interval,column,lc,dt}));
  } else {
    assert(it->second.type==coltype::interval);
    column = it->second.column;
  }
  if(values.size() < column+2){
    values.resize(column+2);
  }
  values[column] = a;
  values[column+1] = b;

  return move(*this);
}

void Plotter::DataPoint::flush()
{
  assert(!flushed);
  assert(!plotter.closed);
  auto first = true;
  for(auto val:values){
    if(!first){
      plotter.data<<" ";
    }
    plotter.data<<val;
    first = false;
  }
  plotter.data << endl;
  flushed = true;
}


Plotter::DataPoint::DataPoint(Plotter &plotter, double t)
  : plotter(plotter)
{
  values.push_back(t);
}
