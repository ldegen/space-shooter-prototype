#include "fragment.h"
#include "pnpoly.hpp"

using std::vector;
using glm::dvec2;

Fragment::Fragment(const Shape && shape, const glm::dvec2 && r, const std::vector<data_contact> && contacts, const glm::dvec2 && vt)
  : shape {shape}
  , r {r}
  , contacts {contacts}
  , vt {vt}
{}
Fragment Fragment::from(const Shape & shape0, const State & state, const vector<data_contact> & contacts){

  dvec2 r = shape0.centroid();
  dvec2 rWorld = r + dvec2 {state.x, state.y};

  Shape shape {shape0.normalized()};
  vector<data_contact> containedContacts;
  containedContacts.reserve(contacts.size());
  for(auto contact: contacts){ 
    dvec2 contactWorld {contact.x_a, contact.y_a};
    dvec2 contactLocal {contactWorld-rWorld};
    if (pnpoly(shape.vertices(), contactLocal*0.95)){
      containedContacts.push_back(contact); 
      //shape.fill(true);
    }
  }
  
    // tangential velocity v is perpendicular to the radius.
    glm::dvec2 tangent {-r.y, r.x}; //ccw, i.e. pointing to the left.

    // we also know that |v| should equal |c| * dtheta/dt
    // Note that `tangent` has currently has the same length as `c`.
    // So multiplying it with `dtheta` should do the trick.
    glm::dvec2 vt = state.dtheta * tangent;
    
    return Fragment(std::move(shape), std::move(r), std::move(containedContacts), std::move(vt));
}
