#ifndef RENDER_CONTEXT_H
#define RENDER_CONTEXT_H

#include "shader.h"
#include "drawable.h"
#include <functional>
#include <optional>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using glm::mat4;
using std::function;
using std::optional;



struct init_cx {
  mat4 model;
  mat4 view;
  mat4 projection;
  Shader shader;
  unsigned int hints;
};


class RenderContext {

  struct delta {
    optional<mat4> model=std::nullopt;
    optional<mat4> view=std::nullopt;
    optional<mat4> projection=std::nullopt;
    optional<Shader> shader=std::nullopt;
    optional<unsigned int> hints=std::nullopt;
  };
  struct base_cx {
    const mat4 & model;
    const mat4 & view;
    const mat4 & projection;
    const Shader & shader;
    const unsigned int & hints;
  };

  const base_cx m_base;
  delta m_delta;

  RenderContext( const base_cx & base);
  public:
    RenderContext(init_cx && init);
    RenderContext(const init_cx & init);

    // access current state
    mat4 model() const &;
    mat4 view() const &;
    mat4 projection() const &;
    Shader shader() const &;
    unsigned int hints() const &;

    RenderContext transform(const mat4 & tf) &&;
    RenderContext transform(const mat4 & tf) const &;
    
    //moving a shader is probably not worth the trouble. 
    //Its footprint is basically an unsigned int.
    RenderContext shader(const Shader & s) &&;
    RenderContext shader(const Shader & s) const &;

    //dito
    RenderContext hints(unsigned int h) &&;
    RenderContext hints(unsigned int h) const &;

    RenderContext & render(function<void(RenderContext &&)> f);
    RenderContext & render(const Drawable & d);

};


using renderbar = function<void(RenderContext &&)>;
#endif
