#include "misc-utils.h"
#include "protocol.h"
#include "physics.h"
#include <iostream>
#include <cmath>
#include <bitset>
#if CP_VERSION_MAJOR < 7
  inline cpVect cpBodyGetVelocity(const cpBody * body){
    return cpBodyGetVel(body);
  }
  inline cpVect cpBodyGetPosition(const cpBody * body){
    return cpBodyGetPos(body);
  }
  inline cpFloat cpBodyGetAngularVelocity(const cpBody * body){
    return cpBodyGetAngVel(body); 
  }
  inline void cpBodySetPosition(cpBody * body, cpVect p){
    cpBodySetPos(body, p); 
  }
  inline void cpBodySetVelocity(cpBody * body, cpVect p){
    cpBodySetVel(body, p); 
  }
  inline void cpBodySetAngularVelocity(cpBody * body, cpFloat p){
    cpBodySetAngVel(body, p); 
  }
  inline cpVect cpBodyWorldToLocal(cpBody * body, cpVect v){
    return cpBodyWorld2Local(body,v); 
  }
  inline cpVect cpBodyLocalToWorld(cpBody * body, cpVect v){
    return cpBodyLocal2World(body,v); 
  }
  cpShape * cpPolyShapeNewRaw(cpBody *body, int count, cpVect *verts, cpFloat radius){
    //FIXME: this sucks.
    //cpVect reversed[count];
    //for(int i = 0; i < count; i++){
      //reversed[count - i] = verts[i]; 
    //}
    CP_CONVEX_HULL(count, verts, count2, verts2)
    return cpPolyShapeNew(body, count2, verts2, cpv(0.0,0.0));
  }
#endif
State Space::bodyState(const cpBody * body){
  State s;
  BodyRecord * rec = (BodyRecord *)cpBodyGetUserData(body);
  unsigned int i;
  for(i=0;i<4;i++){
    if(rec->occurences[i] == body){
      break;
    }
  }
  assert(i<4);
  double r = rec->r;
  cpVect pos = cpBodyGetPosition(body);
  cpVect v = cpBodyGetVelocity(body);
  s.x=pos.x;
  s.y=pos.y;
  s.dx=v.x;
  s.dy=v.y;
  s.theta = cpBodyGetAngle(body);
  s.dtheta = cpBodyGetAngularVelocity(body);
  s.flags = 0;
  s.flags |= inLimbo(pos.x, pos.y, r) ;
  s.flags |= touchingLimbo(pos.x, pos.y,r) ;
  s.flags |= gone(pos.x, pos.y, r);
  s.flags |= (i<<LSHIFT_OCCURENCE);
  return s;
};


cpBool _beginCallback(cpArbiter *arb, cpSpace *cpspace, void *data){
  Space * space = (Space*) data;
  return space->begin(arb);
}
void _separateCallback(cpArbiter *arb, cpSpace *cpspace, void *data){
  Space * space = (Space*) data;
  space->separate(arb);
}
cpBool _preSolveCallback(cpArbiter *arb, cpSpace *cpspace, void *data){
  Space * space = (Space*) data;
  return space->preSolve(arb);
}
void _postSolveCallback(cpArbiter *arb, cpSpace *cpspace, void *data){
  Space * space = (Space*) data;
  space->postSolve(arb);
}

Space::Space(Config & config, ProtocolWriter & writer)
: west {config.bboxLeft()}
, south {config.bboxBottom()}
, east {config.bboxRight()}
, north {config.bboxTop()}
, writer {writer}
, deltaT {config.simulationTimeStep()}
{
  space = cpSpaceNew();
  bodyTypes.resize(1024);
  
  //cpSpaceSetGravity(space, gravity);
#if CP_VERSION_MAJOR < 7
  cpSpaceSetDefaultCollisionHandler(space, _beginCallback, _preSolveCallback, _postSolveCallback, _separateCallback, this);
#else
  cpCollisionHandler *collisionHandler = cpSpaceAddDefaultCollisionHandler(space);
  collisionHandler->postSolveFunc = _postSolveCallback;
  collisionHandler->separateFunc = _separateCallback;
  collisionHandler->preSolveFunc = _preSolveCallback;
  collisionHandler->beginFunc = _beginCallback;
  collisionHandler->userData = this;
#endif

  //planetBody = cpSpaceAddBody(space, cpBodyNewStatic());
  //planetShape = cpSpaceAddShape(space, cpCircleShapeNew(planetBody, 70.0f, cpvzero));
}

Space::~Space(){
  for (auto & entry : records){
    destroyBody(entry.second);
  }
  records.clear();
  cpSpaceFree(space);
}
unsigned long min(unsigned long a, unsigned long b){
  return b<a ? b : a;
}
unsigned long max(unsigned long a, unsigned long b){
  return b>=a ? b : a;
}
void Space::submitCollisionMessage(cpArbiter * arb){
  msg_collision * collision = (msg_collision*) cpArbiterGetUserData(arb);
  CP_ARBITER_GET_BODIES(arb, a, b)
  cpVect pos_a = cpBodyGetPosition(a);
  cpVect pos_b = cpBodyGetPosition(b);
  cpVect j_a = cpBodyWorldToLocal(a,cpvadd(
        pos_a, cpArbiterTotalImpulse(arb)));
  cpVect j_b = cpBodyWorldToLocal(b,cpvadd(
        pos_b, cpArbiterTotalImpulse(arb)));
  collision->jx_a=j_a.x;
  collision->jy_a=j_a.y;
  collision->jx_b=-j_b.x;
  collision->jy_b=-j_b.y;
  data_contact contacts[collision->num_contacts];
#if CP_VERSION_MAJOR >= 7
  cpVect normal_a = cpBodyWorldToLocal(a,cpvadd(
        pos_a, cpArbiterGetNormal(arb)));
  cpVect normal_b = cpBodyWorldToLocal(b,cpvadd(
        pos_b, cpArbiterGetNormal(arb)));
#endif
  for(int i=0;i<collision->num_contacts;i++){
    data_contact * contact = contacts + i;
#if CP_VERSION_MAJOR < 7
    cpVect normal_a = cpBodyWorldToLocal(a,cpvadd(
          pos_a, cpArbiterGetNormal(arb,i)));
    cpVect normal_b = cpBodyWorldToLocal(b,cpvadd(
          pos_b, cpArbiterGetNormal(arb,i)));
    cpVect pointA = cpBodyWorldToLocal(a, cpArbiterGetPoint(arb,i));
    cpVect pointB = cpBodyWorldToLocal(b, cpArbiterGetPoint(arb,i));
    contact->x_a=pointA.x;
    contact->y_a=pointA.y;
    contact->x_b=pointB.x;
    contact->y_b=pointB.y;
#else
    cpVect pointA = cpBodyWorldToLocal(a,cpArbiterGetPointA(arb,i));
    cpVect pointB = cpBodyWorldToLocal(b,cpArbiterGetPointB(arb,i));
    contact->x_a=pointA.x;
    contact->y_a=pointA.y;
    contact->x_b=pointB.x;
    contact->y_b=pointB.y;
#endif
    contact->depth = cpArbiterGetDepth(arb,i);
    contact->normal_x_a=normal_a.x;
    contact->normal_y_a=normal_a.y;
    contact->normal_x_b=-normal_b.x;
    contact->normal_y_b=-normal_b.y;
  }
  writer.onCollision(*collision, contacts);
}
void Space::beginCollisionMessage(cpArbiter * arb){

  CP_ARBITER_GET_BODIES(arb, a, b)
  BodyRecord * recA = (BodyRecord *) cpBodyGetUserData(a);
  BodyRecord * recB = (BodyRecord *) cpBodyGetUserData(b);
 
  // create a snapshot of the two body states *before* the collision.

  Blip blipA {recA->uid, recA->type, bodyState(a), recA->r, recA->tags, recA->hints};
  Blip blipB {recB->uid, recB->type, bodyState(b), recA->r, recB->tags, recB->hints}; 
  msg_collision * collision = new msg_collision {.blip_a=blipA, .blip_b=blipB, .num_contacts=(uint8_t)cpArbiterGetCount(arb)};
  cpArbiterSetUserData(arb, collision);
  //FIXME: just for testing, remove it again!!
  //cpArbiterSetFriction(arb, 0.0);
}
cpBool Space::begin(cpArbiter * arb){
  CP_ARBITER_GET_BODIES(arb, a, b)
  BodyRecord * recA = (BodyRecord *) cpBodyGetUserData(a);
  BodyRecord * recB = (BodyRecord *) cpBodyGetUserData(b);
  //std::cout<<"collision begin "<<a<<"->"<<b<<std::endl;
  cpVect posA = cpBodyGetPosition(a);
  cpVect posB = cpBodyGetPosition(b);
  if(inLimbo(posA.x, posA.y, recA->r) && inLimbo(posB.x, posB.y, recB->r)){
    //std::cout<<"both are ghosts"<<std::endl;
    return 0;
  }
  std::pair<unsigned long, unsigned long> key {min(recA->uid,recB->uid), max(recA->uid, recB->uid)};
  if(collisionPairs.find(key) != collisionPairs.end()){
    return 0; 
  }
  collisionPairs.insert(key);

  if(cpArbiterIsFirstContact(arb)){
    beginCollisionMessage(arb);
  } 
  return 1; 
}

void Space::separate(cpArbiter * arb){
  msg_collision * data = (msg_collision*) cpArbiterGetUserData(arb);
  delete data;
  cpArbiterSetUserData(arb,NULL);
}
cpBool Space::preSolve(cpArbiter * arb){
  CP_ARBITER_GET_BODIES(arb, a, b)
  BodyRecord * recA = (BodyRecord *) cpBodyGetUserData(a);
  BodyRecord * recB = (BodyRecord *) cpBodyGetUserData(b);
  //cpVect impulse = cpArbiterTotalImpulse (arb);
  //std::cout<<"collision pre solve: "<<impulse.x<<", "<<impulse.y<<std::endl;
  //std::cout<<"collision pre solve "<<recA->uid<<"->"<<recB->uid<<std::endl;
  return 1; 
}
void Space::postSolve(cpArbiter * arb){
  if(cpArbiterIsFirstContact(arb)){
    submitCollisionMessage(arb);
  } 
  CP_ARBITER_GET_BODIES(arb, a, b)
  BodyRecord * recA = (BodyRecord *) cpBodyGetUserData(a);
  BodyRecord * recB = (BodyRecord *) cpBodyGetUserData(b);
  //cpVect impulse = cpArbiterTotalImpulse (arb);
  //std::cout<<"collision post solve: "<<impulse.x<<", "<<impulse.y<<std::endl;
  
}

unsigned long Space::createStatic(unsigned long uid, unsigned int type){
  auto & bt = bodyTypes[type];

  double M = bt.mass();
  double J = bt.momentOfInertia();
  const std::vector<Shape> & shapes = bt.shapes();
  cpBody * body = createStaticInternal(shapes, M,J,0.0,0.0,0.0);

  BodyRecord r;
  r.uid=uid;
  r.type=type;
  r.timeCreated=t;
  r.x=0.0;
  r.y=0.0;
  r.theta=0.0;
  r.dx=0.0;
  r.dy=0.0;
  r.dtheta=0.0;
  r.r=bt.r();
  r.occurences[0]=body;
  r.occurences[1]=NULL;
  r.occurences[2]=NULL;
  r.occurences[3]=NULL;
  r.tags=0;
  r.hints=0;
  records[uid] = std::move(r);
  cpBodySetUserData(body, &records[uid]);
  //cpBodySetVelocityUpdateFunc(body, planetGravityVelocityFunc);
  writer.write<msg_body_created>({.uid=uid,.tid=type});
  selection=uid;
  return uid;
}
unsigned long Space::createBody(unsigned long uid, unsigned int type, const State & initialState){
  auto & bt = bodyTypes[type];

  const std::vector<Shape> & shapes = bt.shapes();

  //To simulate a body, we need to know its mass and moment of inertia
  double M = bt.mass();
  double J = bt.momentOfInertia();

  //The mass is just the sum of the masses of the individual shapes.
  //But for calculating the total moment of inertia,
  //we first need to find the combined center of gravity of all the given shapes.
  //Then, we use Steiner's Theorem to determin the individual moments with respect to this common axis
  //and sum them up.

  glm::dvec2 C = bt.centerOfGravity();


  //std::cout<<"J="<<J<<", M="<<M<<std::endl;

  cpBody * body = createBodyInternal(shapes, M, J, bt.r(),initialState);

  BodyRecord r;
  r.uid=uid;
  r.type=type;
  r.timeCreated=t;
  r.x=initialState.x;
  r.y=initialState.y;
  r.theta=initialState.theta;
  r.dx=initialState.dx;
  r.dy=initialState.dy;
  r.dtheta=initialState.dtheta;
  r.r=bt.r();
  r.occurences[0]=body;
  r.occurences[1]=NULL;
  r.occurences[2]=NULL;
  r.occurences[3]=NULL;
  r.tags=0;
  r.hints=0;

  records[uid] = std::move(r);
  cpBodySetUserData(body, &records[uid]);
  //cpBodySetVelocityUpdateFunc(body, planetGravityVelocityFunc);
  writer.write<msg_body_created>({.uid=uid,.tid=type});
  selection=uid;
  return uid;
}
cpBody * Space::createStaticInternal(const std::vector<Shape> shapes, double M, double J, double x, double y, double theta){
  // now, that we know the total mass and moment, we can create the body
#if CP_VERSION_MAJOR < 7
  cpBody * body = cpSpaceAddBody(space, cpBodyNewStatic());
#else
  cpBody * body = cpSpaceAddBody(space, cpBodyNewKinematic());
#endif
  //std::cout<<"created body/ghost "<<body<<std::endl;

  // ... and add all shapes
  // TODO: handle non-convex shapes!
  for (auto & shape : shapes) {
    const auto & origVertices = shape.vertices();
    std::vector<cpVect> vertices;
    vertices.reserve(origVertices.size());
    for (auto & vertex: origVertices){
      vertices.push_back({vertex.x,vertex.y});
    }
    cpShape * collisionShape = cpSpaceAddShape(space, cpPolyShapeNewRaw(body, vertices.size(), vertices.data(), 0));
    cpShapeSetFriction(collisionShape, shape.friction());
    cpShapeSetElasticity(collisionShape, shape.elasticity());
  }


  cpBodySetPosition(body, cpv(x, y));
  cpBodySetAngle(body, theta);

  return body;
}

cpBody * Space::createBodyInternal(const std::vector<Shape> shapes, double M, double J, double r, const State & initialState){
  // now, that we know the total mass and moment, we can create the body
  cpBody * body = cpSpaceAddBody(space, cpBodyNew(M,J));
  //std::cout<<"created body/ghost "<<body<<std::endl;

  // ... and add all shapes
  // TODO: handle non-convex shapes!
  for (auto & shape : shapes) {
    const auto & origVertices = shape.vertices();
    std::vector<cpVect> vertices;
    vertices.reserve(origVertices.size());
    for (auto & vertex: origVertices){
      vertices.push_back({vertex.x,vertex.y});
    }
    cpShape * collisionShape;
    if(r < particleThreshold){
      collisionShape = cpSpaceAddShape(space, cpCircleShapeNew(body,r,cpv(0,0)));
    }
    else{
      collisionShape = cpSpaceAddShape(space, cpPolyShapeNewRaw(body, vertices.size(), vertices.data(), 0));
    }
    cpShapeSetFriction(collisionShape, shape.friction());
    cpShapeSetElasticity(collisionShape, shape.elasticity());
  }


  cpBodySetPosition(body, cpv(initialState.x, initialState.y));
  cpBodySetVelocity(body, cpv(initialState.dx, initialState.dy));
  cpBodySetAngle(body, initialState.theta);
  cpBodySetAngularVelocity(body, initialState.dtheta);

  return body;
}

void freeShape(cpBody * body, cpShape * shape, void * data){
  cpSpace * space = cpBodyGetSpace(body);
  //std::cout<<"free shape: "<<shape<<std::endl;
  cpSpaceRemoveShape(space, shape);
  cpShapeFree(shape);
}
void Space::destroyBody(BodyRecord & record){
  for (int i=0;i<4;i++){
    cpBody * body = record.occurences[i];
    if(body != NULL){
      //std::cout<<"removing body/ghost "<<body<<std::endl;
      cpBodyEachShape(body, freeShape, NULL);
      cpSpaceRemoveBody(space, body);
      cpBodyFree(body);
      record.occurences[i] = NULL;
    }
  }
  

}
void Space::destroyBody(unsigned long uid) {
  auto it = records.find(uid);
  if(it==records.end()){
    writer.write<msg_error>({.code=ERR_BAD_UID, .arg=uid}); 
  } 
  else {
    unsigned int tid = it->second.type;
    destroyBody(it->second);
    writer.write<msg_body_deleted>({.uid=uid, .tid=tid});
    records.erase(it);
  }
  selection=0;
}


double Space::step(){
  if(!pause){
    collisionPairs.clear();
    beforeEachStep();
    cpSpaceStep(space, deltaT);
    t += deltaT;
    afterEachStep();
    //if(collisionPairs.size()>0){
    //  pause=true;
    //}
  }
  return t;
}
void Space::onMessage(const cmd_pause *cmd){
  pause=true;
}
void Space::onMessage(const cmd_toggle_pause *cmd){
  //std::cout<<"toggle pause (physics)"<<std::endl;
  pause=!pause;
}
void Space::onMessage(const cmd_resume *cmd){
  pause=false;
}
void Space::onMessage(const cmd_create *cmd){

  //std::cout<<"create: "<<cmd->uid<<", "<<cmd->tid<<std::endl;
  if(cmd->static_body){
    createStatic(cmd->uid, cmd->tid);
  } else {
    createBody(cmd->uid, cmd->tid);
  }
}
void Space::onMessage(const cmd_delete *cmd){
  destroyBody(selection);
}
void Space::onMessage(const cmd_select_uid *cmd){
  selectUid(cmd->uid);
}
void Space::onMessage(const cmd_set_x_y_theta *cmd){
  //std::cout<<"theta A: "<<cmd->theta<<std::endl;
  setXYT(cmd->x, cmd->y, cmd->theta, selection, cmd->rel);
}
void Space::onMessage(const cmd_set_dx_dy_dtheta *cmd){
  setDxDyDt(cmd->dx, cmd->dy, cmd->dtheta, selection);
}
void Space::onMessage(const cmd_set_dtheta *cmd){
  setDt(cmd->dtheta, selection);
}
void Space::onMessage(const cmd_set_fx_fy_torque *cmd){
  setFxFyT(cmd->fx, cmd->fy, cmd->torque, selection);
}
void Space::onMessage(const cmd_hint *cmd){
  hint(selection, cmd->unset, cmd->set);
}
void Space::onMessage(const cmd_tag *cmd){
  tag(selection, cmd->unset, cmd->set);
}
void Space::onMessage(const cmd_apply_impulse *cmd){
  applyImpulse(cmd->jx, cmd->jy, cmd->rx, cmd->ry, selection);
}
void Space::onBodyType(const BodyType & bt){
  if (bt.tid() >= bodyTypes.size()){
    bodyTypes.resize(bt.tid()+1);
  }
  //std::cout<<"body type "<<bt.tid<<std::endl;
  bodyTypes[bt.tid()] =  bt;
}
void Space::hint(unsigned long uid, unsigned int unset, unsigned int set){
  BodyRecord & rec = records[uid];
  rec.hints = rec.hints & ~unset | set;
}
void Space::tag(unsigned long uid, unsigned int unset, unsigned int set){
  BodyRecord & rec = records[uid];
  rec.tags = rec.tags & ~unset | set;
}
void Space::selectUid(unsigned long uid){
  if(records.find(uid) == records.end()){
    writer.write<msg_error>({.code=ERR_BAD_UID, .arg=uid}); 
    selection = 0;
  } else {
    selection = uid;
  }
}

cpVect Space::moduloBbox(const cpVect & p){
  return moduloBbox(p.x, p.y);
}
cpVect Space::moduloBbox(double x0, double y0){
  double offsetX = fmod(x0 - west, east - west); 
  double offsetY = fmod(y0 - south, north - south); 
  if (offsetX < 0){
    offsetX += east-west; 
  }
  if (offsetY < 0){
    offsetY += north-south; 
  }
  double x = west + offsetX;
  double y = south + offsetY;
  return cpv(x,y); 
}

 
cpBody * Space::realOccurence(BodyRecord & rec, State * state){

  State tmpState;

  if(state==NULL){
    state = &tmpState; 
  }
  for(int i=0;i<4;i++){
    cpBody * body = rec.occurences[i];

    if(body!=NULL){
      *state = bodyState(body);
      if(! (state->flags & (MASK_GONE|MASK_IN_LIMBO))){
        return body;
      }
    }
  }
  return NULL;
}

void Space::setXYT(double x0, double y0, double theta, unsigned long uid, unsigned long rel){
  // make sure x,y is within our bbx
  BodyRecord & rec = records[uid];
  // first find the "real" occurence and use it as reference
  cpVect delta;
  State state; 
  cpBody * body = realOccurence(rec, &state);
  if(!body){
    return; 
  }
  cpVect pos;
  if(rel){
    auto it = records.find(rel);
    if(it==records.end()){
      warn("Bad rel: %u", rel);
      return; 
    }
    //std::cout<<"x0: "<<x0<<", y0: "<<y0<<std::endl;
    State relState;
    cpBody * relBody = realOccurence(it->second, &relState);
    pos = moduloBbox(cpBodyLocalToWorld(relBody, cpv(x0, y0)));
    theta +=relState.theta;
  }
  else {
    pos = moduloBbox(x0, y0); 
  }
  delta = cpv(pos.x - state.x, pos.y - state.y);

  // finally, translate all occurences
  for(int i=0;i<4;i++){
    cpBody * body = rec.occurences[i];

    if(body!=NULL){
      cpBodySetPosition(body, cpvadd(cpBodyGetPosition(body),delta));
      cpBodySetAngle(body, theta);
    }
  }

}
void Space::setDt(double dtheta, unsigned long uid){
  BodyRecord & rec = records[uid];
  rec.dtheta=dtheta;
  for(int i=0;i<4;i++){
    cpBody * body = rec.occurences[i];
    if(body!=NULL){
      cpBodySetAngularVelocity(body, dtheta);
      //This is *different* from force/torque/impulse!!
      //synchronization will only occur *after* the next
      //simulation step. Since we change velocities directly,
      //we need to "synchronize" ourselfs
    }
  }
}
void Space::setDxDyDt(double dx, double dy, double dtheta, unsigned long uid){
  BodyRecord & rec = records[uid];
  rec.dx=dx;
  rec.dy=dy;
  rec.dtheta=dtheta;
  for(int i=0;i<4;i++){
    cpBody * body = rec.occurences[i];
    if(body!=NULL){
      
      cpBodySetVelocity(body, cpv(dx,dy));
      cpBodySetAngularVelocity(body, dtheta);
      //This is *different* from force/torque/impulse!!
      //synchronization will only occur *after* the next
      //simulation step. Since we change velocities directly,
      //we need to "synchronize" ourselfs
    }
  }
}
void Space::applyImpulse(double jx, double jy, double rx, double ry, unsigned long uid){
  BodyRecord & rec = records[uid];
  for(int i=0;i<4;i++){
    cpBody * body = rec.occurences[i];
    if(body!=NULL){
#if CP_VERSION_MAJOR >= 7
      cpBodyApplyImpulseAtLocalPoint(body, cpv(jx, jy), cpv(rx,ry));
#else
      cpBodyApplyImpulse(body, cpv(jx, jy), cpv(rx,ry)); 
#endif
      //same as with force/torque: only apply it once 
      break; 
    }
  }
}
void Space::setFxFyT(double fx, double fy, double t, unsigned long uid){
  BodyRecord & rec = records[uid];
  for(int i=0;i<4;i++){
    cpBody * body = rec.occurences[i];
    if(body!=NULL){
      //FIXME: it seems inconsistent to use local coordinates here. Leave transform to
      //       logic layer or have two versions of this function.
      cpVect f = cpvsub(cpBodyLocalToWorld(body,cpv(fx,fy)), cpBodyGetPosition(body));
      cpBodySetForce(body, f);
      cpBodySetTorque(body, t);
      //only apply force to one of the occurences.
      //Velocities will be synchronized after the step.
      //If we would apply forces to each of the occurences 
      //the effect would be the same as adding them k times.
      break; 
    }
  }
}

bool allSet(unsigned int flags, unsigned int query){
  return (flags & query) == query;
}
void Space::synchronize(BodyRecord & rec){
 
  // calculate changes in position
  // Here we simply use the last known velocity and
  // multiply with deltaT
  double deltaX = rec.dx * deltaT;
  double deltaY = rec.dy * deltaT;
  double deltaTheta = rec.dtheta * deltaT;

  // accumulate changes in velocity
  // Here we have to take into account the velocity changes in the
  // individual occurences. Summing them up yields the
  // overall velocity change.
  double deltaVX=0;
  double deltaVY=0;
  double deltaAV=0;
  for (int i=0;i<4;i++){
    cpBody * body = rec.occurences[i];
    if(body != NULL){
      cpVect v = cpBodyGetVelocity(body);
      double av = cpBodyGetAngularVelocity(body);

      deltaVX += v.x - rec.dx;
      deltaVY += v.y - rec.dy;
      deltaAV += av - rec.dtheta;
    }
  }
  // now we can update the body record
  rec.x += deltaX;
  rec.y += deltaY;
  rec.theta += deltaTheta;
  rec.dx += deltaVX;
  rec.dy += deltaVY;
  rec.dtheta += deltaAV;

  // finally, we update the occurences.
  // Note that in theory we should not have to update the position/orientation as long
  // as we make sure that linear/angular velocities stay in sync.
  // In real life positions may drift a bit over time, but I think this does not matter
  // that mucht.
  cpVect v = cpv(rec.dx, rec.dy);
  //cpVect delta = cpv(deltaX,deltaY);
  for (int i=0;i<4;i++){
    cpBody * body = rec.occurences[i];
    if(body != NULL){
      //cpVect occPos = cpBodyGetPosition(body);
      //if(cpvlength(delta)>0.01){
        //std::cout<<"positions drifting! "<<delta.x<<", "<<delta.y<<std::endl; 
      //}
      //cpBodySetPosition(body, cpvadd(occPos, delta));
      //cpBodySetAngle(body, rec.theta);
      cpBodySetVelocity(body, v);
      cpBodySetAngularVelocity(body,rec.dtheta);
    }
  }
}
// create/remove ghosts as needed 
void Space::manageGhosts(BodyRecord & rec){
  unsigned int realFlags = 0;
  unsigned int ghostFlags = 0;
  cpBody * real=NULL;
  for (int i=0;i<4;i++){
    cpBody * body = rec.occurences[i];
    if(body != NULL){
      State state = bodyState(body);
      //std::cout<<entry.first<<"/"<<i<<"/"<<std::bitset<16>{state.flags}<<std::endl;
      if(state.flags & MASK_GONE){ //it's gone
        deleteGhost(body, rec);
      } else{
        if(state.flags & MASK_IN_LIMBO){//it's a ghost
          ghostFlags |= state.flags;
        } else { //it's real
          //if the movment of the occurences is not 100% in sync,
          //it may happen that there are more than one "real" occurences.
          //There is not really much we can do about it.
          //Just pick one (here: the one with the greatest index.
          real = body;
          realFlags = state.flags;
        }
      }
    }
  }

  if(real){
    // check if we have all the ghosts we need
    if(allSet(realFlags,FLAG_TOUCHING_LIMBO_N) && !allSet(ghostFlags,FLAG_IN_LIMBO_S)){
      createGhost(real, 0, south - north, rec);
    }
    if(allSet(realFlags,FLAG_TOUCHING_LIMBO_S) && !allSet(ghostFlags,FLAG_IN_LIMBO_N)){
      createGhost(real, 0, north - south, rec);
    }
    if(allSet(realFlags,FLAG_TOUCHING_LIMBO_E) && !allSet(ghostFlags,FLAG_IN_LIMBO_W)){
      createGhost(real, west - east, 0, rec);
    }
    if(allSet(realFlags,FLAG_TOUCHING_LIMBO_W) && !allSet(ghostFlags,FLAG_IN_LIMBO_E)){
      createGhost(real, east - west, 0, rec);
    }
    if(allSet(realFlags,FLAG_TOUCHING_LIMBO_NW) && !allSet(ghostFlags,FLAG_IN_LIMBO_SE)){
      createGhost(real, east - west, south - north, rec);
    }
    if(allSet(realFlags,FLAG_TOUCHING_LIMBO_NE) && !allSet(ghostFlags,FLAG_IN_LIMBO_SW)){
      createGhost(real, west - east, south - north, rec);
    }
    if(allSet(realFlags,FLAG_TOUCHING_LIMBO_SW) && !allSet(ghostFlags,FLAG_IN_LIMBO_NE)){
      createGhost(real, east - west, north - south, rec);
    }
    if(allSet(realFlags,FLAG_TOUCHING_LIMBO_SE) && !allSet(ghostFlags,FLAG_IN_LIMBO_NW)){
      createGhost(real, west - east, north - south, rec);
    }
  }
}
void Space::beforeEachStep(){
  std::vector<Attractor> attractors;
  for (auto & entry : records){
    if(entry.second.tags & TAG_GRAVITY_SOURCE){
      auto & bt = bodyTypes[entry.second.type];
      cpBody * ab = realOccurence(entry.second);
      if(ab != NULL){
        attractors.push_back({
          .body=ab,
          .mass=bt.mass()
        }); 
      }
    } 
  }
  for (auto & entry : records){
    applyGravity(entry.second, attractors);
  }
}
void Space::afterEachStep(){
  for (auto & entry : records){
    synchronize(entry.second);
    manageGhosts(entry.second);
  }
}
void Space::applyGravity(BodyRecord & rec, std::vector<Attractor> & attractors){
  //std::cout<<"attractors: "<<attractors.size()<<std::endl;
  cpBody * body = realOccurence(rec);
  if(!body){
    return; 
  }
  cpVect fTotal = cpBodyGetForce(body);
  //we assume a body with *huge* mass (i.e. a "planet") at the origin.
  //We expect its mass to be so vastly larger than that of any other
  //object that it does not move.
  double G= 6.6743e-5;
  double m2 = cpBodyGetMass(body);
  for(auto attractor : attractors){
    double m1 = attractor.mass;
    cpVect r = cpvsub(cpBodyGetPosition(body),cpBodyGetPosition(attractor.body));
    double distsq = cpvlengthsq(r);
    double dist = cpvlength(r);
    double s = -G*m1*m2/distsq;
    //std::cout<<"r: "<<r.x<<", "<<r.y<<std::endl;
    //std::cout<<"distsq: "<<distsq<<std::endl;
    //std::cout<<"dist: "<<dist<<std::endl;
    //std::cout<<"s/dist: "<<s/dist<<std::endl;
    fTotal = cpvadd(fTotal, cpvmult(r, s/dist));
  }
  //std::cout<<"fTotal: "<<fTotal.x<<", "<<fTotal.y<<std::endl;
  cpBodySetForce(body, fTotal);
}
unsigned int Space::inLimbo(double x, double y, double r){
  unsigned int flags = 0;
  if (x < west && x + r >= west){
    flags |= FLAG_IN_LIMBO_W;
  }
  if (x >= east && x - r < east){
    flags |= FLAG_IN_LIMBO_E;
  }
  if (y < south && y + r >= south){
    flags |= FLAG_IN_LIMBO_S;
  }
  if (y >= north && y - r < north){
    flags |= FLAG_IN_LIMBO_N;
  }
  return flags;
};

unsigned int Space::touchingLimbo(double x, double y, double r){
  unsigned int mask = 0;

  if (x - r < west && x >= west){
    mask |= FLAG_TOUCHING_LIMBO_W;
  }
  if (x + r >= east && x < east){
    mask |= FLAG_TOUCHING_LIMBO_E;
  }
  if (y - r < south && y >= south){
    mask |= FLAG_TOUCHING_LIMBO_S;
  }
  if (y + r >= north && y < north){
    mask |= FLAG_TOUCHING_LIMBO_N;
  }

  return mask;
};

unsigned int Space::gone(double x, double y, double r){
  unsigned int mask = 0;
  if( x + r < west){
    mask |= FLAG_GONE_W;
  }
  if(x - r >= east){
    mask |= FLAG_GONE_E;
  }
  if(y + r < south){
    mask |= FLAG_GONE_S;
  }
  if(y - r >= north){
    mask |= FLAG_GONE_N;
  }
  return mask;
}
void Space::printDebug(const BodyRecord & rec){

  debug("%u: [%x, %x, %x, %x]", rec.uid, rec.occurences[0], rec.occurences[1], rec.occurences[2], rec.occurences[3]);
  debug("%u-------ESWNeswnESWN/", rec.uid);
  for (int i=0;i<4;i++){
    if(rec.occurences[i] != NULL){
      State state {bodyState( rec.occurences[i] )};
      debug("%u/%d/%s/: %f, %f", rec.uid, i, std::bitset<16>{state.flags}, state.x, state.y );
    } 
  }
}
void Space::createGhost(const cpBody * orig, double tx, double ty, BodyRecord & rec){

  //find a free slot in the occurence array
  for(int i=0;i<4;i++){
    if(rec.occurences[i] == NULL){
      auto & bt = bodyTypes[rec.type];
      State state {bodyState( orig )};
      state.x += tx;
      state.y += ty;
      double M = cpBodyGetMass(orig);
      double J = cpBodyGetMoment(orig);
      cpBody * ghost = createBodyInternal(bt.shapes(), M, J, bt.r(),state);

      cpBodySetUserData(ghost, &rec);
      rec.occurences[i] = ghost;
      // a little sanity check
      if(gone(state.x, state.y, rec.r)){
        debug("at %f: gone ghost %x from orig %x + (%f, %f)", t, ghost, orig, tx, ty);
        printDebug(rec);
      }
      else if(!inLimbo(state.x, state.y, rec.r)){
        debug("at %f: living ghost %x from orig %x + (%f, %f)", t, ghost, orig, tx, ty);
        printDebug(rec);
      }
      return;
    }
  }
  //std::cout<<"ERROR: no free slot!!"<<std::endl;
}
void Space::deleteGhost(const cpBody * ghost, BodyRecord & record){

  //std::cout<<"at "<<t<<": looking for ghost "<<ghost<<" in record "<<record.uid<<std::endl;
  for (int i=0;i<4;i++){
    cpBody * body = record.occurences[i];
    if(body == ghost){
      //std::cout<<"at "<<t<<": removing ghost at ix "<<i<<": "<<body<<std::endl;
      cpBodyEachShape(body, freeShape, NULL);
      cpSpaceRemoveBody(space, body);
      cpBodyFree(body);
      record.occurences[i] = NULL;
      return;
    }
  }
  error("ghost not found!!");
}

int Space::blips(double alpha){
  return blips(writer, alpha);
}
int Space::blips(){
  return blips(writer, 0.0);
}

int Space::blips(ProtocolListener & listener, double alpha){
  std::vector<Blip> output;
  std::vector<data_tag> tags;
  for (auto & entry : records){
    //if(alpha != 0.0 && entry.second.timeCreated == t){
    //  //if the body was created just now, we need to delay it
    //  //for at least one frame before we can interpolate its state.
    //  continue;
    //}
    if(entry.second.tags){
      tags.push_back({entry.second.tags,(uint32_t)output.size()}); 
    }
    for (int i=0;i<4;i++){
      cpBody * body = entry.second.occurences[i];
      if(body != NULL){
        output.push_back({
            entry.second.uid,
            entry.second.type,
            //alpha==0.0 ? bodyState(body) : interpolate(bodyState(body), entry.second, alpha),
            bodyState(body),
            entry.second.r,
            entry.second.tags,
            entry.second.hints
            });
      }
    }
  }
  listener.onBlips({.t=t, .pause=pause, .num_blips=(uint32_t)output.size(), .num_tags=(uint32_t)tags.size()}, output.data(), tags.data());
  return output.size();
}
