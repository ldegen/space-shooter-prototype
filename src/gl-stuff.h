#ifndef GL_STUFF_H
#define GL_STUFF_H
#include "bbox.hpp"
#include "flags.h"
#include "trackball.h"
#include "shader.h"
#include "model.h"
#include "protocol.h"
#include "body-type.h"
#include "render-context.h"
#include "renderer.h"
#include "config.hpp"
#include <vector>
#include <map>
#include <unordered_map>
#include <list>
#include <memory>
#include <lo/lo.h>
#include <lo/lo_cpp.h>




class GlStuff : public FilteringProtocolListener{
  private:
    //Shader frameShader{"gradient.gles2.vert", "gradient.gles2.frag"};
    //Shader blipShader{"blip.gles2.vert", "blip.gles2.frag"};
    Shader batchShader{"batch.gles2.vert", "blip.gles2.frag"};
    Shader uniBatchShader{"unibatch.gles2.vert", "unibatch.gles2.frag"};

    unique_ptr<Renderer> renderer;

    Bbox<double> bbox;

    //Bbox<double> origExtent;
    bool dragging=false;
    glm::dvec2 origMouse;
    glm::dvec2 origPan;

    // all these things are effecting our "camera" transformation:
    // - the viewport dimensions
    int viewportWidth = 800;
    int viewportHeight = 600;

    // and a circular part of the world that needs to be visible
    // in our viewport. The center/radius are given in world coordinates
    //glm::dvec2 m_lookat {0,0};
    //double m_radius = 1;
    Bbox<double> m_motiv;
    double m_motiv_min_width = 2.0;
    double m_motiv_min_height= 2.0;

    // from these values we derive and cache:
    // - the zoom factor that needs to be applied
    double m_zoom = 1;
    // - the extent, which can be imagined as the rectangular part
    //   of the world space that should currently be visible in the viewport
    Bbox<double> m_extent {-1.0,-1.0,1.0,1.0};
    // - and ultimately, the projection matrix
    mat4 m_projection;

    // if any of the viewport, lookat or radius changes, we need to
    // recalculate the derived values.

    bool projectionDirty = true;

    void calculateProjection();

    // simulation time in seconds.
    double t = 0.0;

    lo::Address oscAddress;
    bool _keepRunning = true;
  public:

    GlStuff(Config & config);

    virtual ~GlStuff();

    void render(const Blip * blips, unsigned int numBlips);
    void resize(const int width, const int height);
    void mouseMoved(const int x, const int y);

    void mouseButtonDown(const int x, const int y);

    void mouseButtonUp(const int x, const int y);

    void scrolled(const int xoffset, const int yoffset);
  
    void onBodyType(const BodyType & bt) override;

    bool examineTags(const msg_blips & header, unsigned int tags) override;
    void onBlip(const msg_blips & header, const Blip & bli) override;
    void beforeBlips(const msg_blips & header, const Blip * blips, const data_tag * tags) override;
    void afterBlips(const msg_blips & header, const Blip * blips, const data_tag * tags) override;
    void onMessage(const cmd_look_at *cmd) override;
    void onMessage(const cmd_play_sound *cmd) override;
    void onMessage(const cmd_quit *cmd) override;
    void onMessage(const msg_body_created *cmd) override;
    void onMessage(const msg_body_deleted *cmd) override;
    void onMessage(const msg_window_resized *cmd) override;
    bool keepRunning();
    //void onCollision(const msg_collision&, const data_contact*);
    Bbox<int> viewport(){
      return {0, 0, viewportWidth, viewportHeight}; 
    }
    glm::dvec2 lookAt();
    double radius();
    double zoom();
    Bbox<double> extent();
    mat4 projection();

  private:

    void panTo(const glm::dvec2 & t);
    void panBy(const glm::dvec2 & delta);

    /* create a view matrix
     *
     * extent is defined as above. It is devided into up to four facets.
     * For each facet, we need to define a different view transformation.
     *
     * Think of it this way: The facet may lie outside the world boundaries. But
     * since we live in a torodial world, the content of the world keeps repeating
     * outside of it. So for the extent our facet belongs to, there must be an
     * equivalent extent with a facet corresponding to our facet lying completly
     * inside the world boundaries.
     *
     * The view transformation we need is a translation that maps the center of
     * said equivalent extent (not facet!) into the origin (which is the center
     * of our clipping volume and thus the center of our viewport).
     */
    mat4 view(const Bbox<double> & extentBox, const Bbox<double> & facet);
    vec2 facetTranslation(const Bbox<double> & extentBox, const Bbox<double> & facet);
    pair<double,double> panAndVolume(const glm::dvec3 & pos);
};
#endif
