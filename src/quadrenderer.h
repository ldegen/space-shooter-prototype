#ifndef QUADRENDERER_H
#define QUADRENDERER_H

#include "renderer.h"
#include "gamerenderer.h"

class QuadRenderer : public Renderer {
    vector<unique_ptr<Renderer>> delegatees;
    size_t c = 0;
  public:
    QuadRenderer();

    virtual void reset(unsigned int) override;

    virtual void submit() override;

    virtual void draw(tid_t tid, const vec3 &transform) override;

    virtual void addGeometry(tid_t tid, const vector<glm::vec2> &coordinates) override;
    virtual void removeGeometry(tid_t tid) override;
};

#endif // QUADRENDERER_H
