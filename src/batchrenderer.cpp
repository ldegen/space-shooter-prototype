#include "batchrenderer.h"
#include "misc-utils.h"
#include <iostream>

BatchRenderer::Slot *BatchRenderer::allocateSlot(BatchRenderer::tid_t tid){
  auto it = usedSlots.find(tid);
  // if there is already a slot associated with the
  // tid, return it.
  if(it!=usedSlots.end()){
    return &it->second;
  }
  // otherwise, make a new one.
  Slot s;
  // if the freeList is empty, just place it *after* all the slots that are in use.
  if(freeList.empty()){
    s.start = static_cast<vertex_offset_t>(usedSlots.size()) * slotSize;
    // check if the slot still fits into our pool
    if(s.start + slotSize > maxVertices){
      return nullptr;
    }
  }
  // if the free list contains an offset, reuse it.
  else {
    s.start = freeList.front();
    freeList.pop_front();
  }

  // initialize the size to zero.
  s.size=0;

  // finally, put the slot in our lookup table
  auto [newIt, success] = usedSlots.insert(make_pair(tid,s));
      if(!success){
    return nullptr;
  }
  return &newIt->second;
}


void BatchRenderer::setupBufferObjects(){
  glGenBuffers(1,&VBO_vertices);
  glCheckError();
  glGenBuffers(1, &VBO_transforms);
  glCheckError();
  glGenBuffers(1, &EBO);
  glCheckError();
  glBindBuffer(GL_ARRAY_BUFFER, VBO_vertices);
  glCheckError();
  glBufferData(GL_ARRAY_BUFFER, maxVertices * sizeof(vec2), nullptr, GL_STREAM_DRAW);
  glCheckError();
  glBindBuffer(GL_ARRAY_BUFFER, VBO_transforms);
  glCheckError();
  glBufferData(GL_ARRAY_BUFFER, maxVertices * sizeof(vec3), nullptr, GL_STREAM_DRAW);
  glCheckError();
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glCheckError();
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, maxIndices * sizeof(index_t), nullptr, GL_STREAM_DRAW);
  glCheckError();
}

BatchRenderer::BatchRenderer(BatchRenderer::vertex_count_t slotSize, BatchRenderer::vertex_count_t max_vertices, bool fill)
  : maxVertices(max_vertices)
  , slotSize(slotSize)
  , maxIndices(3 * max_vertices)
  , fill(fill)
{
  positions.resize(max_vertices);
  transforms.resize(max_vertices);
  indices.reserve(maxIndices);
  setupBufferObjects();
}

void BatchRenderer::addGeometry(BatchRenderer::tid_t tid, const vector<glm::vec2> & coordinates){
  Slot * slot = allocateSlot(tid);
  if(!slot){
    error("slot allocation failed");
    return;
  }
  vertex_offset_t offset = 0;
  if (fill){
    positions[slot->start] = {0,0};
    offset = 1;
  }
  slot->size=static_cast<vertex_count_t>(coordinates.size()+offset);

  for(vertex_offset_t i = 0; i< coordinates.size(); i++){
    positions[offset+i+slot->start] = coordinates[i];
  }

}

void BatchRenderer::removeGeometry(BatchRenderer::tid_t tid){
  auto it = usedSlots.find(tid);
  if(it == usedSlots.end()){
    return;
  }
  freeList.push_back(it->second.start);
  usedSlots.erase(it);
}

void BatchRenderer::reset()
{
  _bounds = {std::numeric_limits<index_t>::max(),0};
  _pos = 0;
  indices.clear();
}

void BatchRenderer::submit()
{
  index_offset_t start = 0;
  index_offset_t end = _pos;
  if(start == end){
    return;
  }
  glClientWaitSync(0,0,0);
  // vertex attrib 0: vertex positions
  glBindBuffer(GL_ARRAY_BUFFER, VBO_vertices);
  glCheckError();
  glBufferSubData(GL_ARRAY_BUFFER,
                  _bounds.first * sizeof (vec2),
                  (_bounds.second - _bounds.first) * sizeof (vec2),
                  &positions[_bounds.first]);
  // not sure if we need to enable set up the attribs each frame?!
  glEnableVertexAttribArray(0);
  glCheckError();
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(vec2), nullptr);
  glCheckError();

  // vertex attrib 1: vertex transforms
  glBindBuffer(GL_ARRAY_BUFFER, VBO_transforms);
  glCheckError();
  glBufferSubData(GL_ARRAY_BUFFER,
                  _bounds.first * sizeof (vec3),
                  (_bounds.second - _bounds.first) * sizeof (vec3),
                  &transforms[_bounds.first]);
  glCheckError();
  glEnableVertexAttribArray(1);
  glCheckError();
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), nullptr);
  glCheckError();

  // indices:
  // given we only have one EBO, we probably do not have to bind it each frame.
  // unless we are doing double buffering.
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glCheckError();
  glBufferSubData(GL_ELEMENT_ARRAY_BUFFER,
                  start * sizeof(index_t),
                  (end - start) * sizeof(index_t),
                  &indices[start]);
  glCheckError();
  glDrawElements(
        fill ? GL_TRIANGLES : GL_LINE_LOOP,
        (end - start),
        GL_UNSIGNED_SHORT,
        0);
  glCheckError();

}

void BatchRenderer::draw(Renderer::tid_t tid, const glm::vec3  & transform)
{
  _pos = draw(tid, transform, _pos, _bounds);
}

BatchRenderer::index_offset_t BatchRenderer::draw(const tid_t tid, const vec3 & transform, BatchRenderer::index_offset_t pos, pair<BatchRenderer::index_t, BatchRenderer::index_t> &bounds){
  auto it = usedSlots.find(tid);
  if(it==usedSlots.end()){
    return pos;
  }
  auto const & slot = it->second;
  auto start = slot.start;
  auto end = start + slot.size;
  bounds.first = min(bounds.first, start);
  bounds.second = std::max(bounds.second, static_cast<index_t>(end));
  //draw triangles (a.k.a. connect the dots)
  if (fill){
    //note that we are starting at 1. offset + 0 is the center vertex.
    for(vertex_count_t i = start + 1 ; i <end; i++ ){
      indices.push_back(start);
      indices.push_back(i);
      indices.push_back((i+1 < end) ? i+1 : start+1);
      pos += 3;
    }
  } else {
    for(vertex_count_t i = start ; i < end; i++){
      indices.push_back(i);
      pos++;
    }
  }
  //copy the transform
  for(vertex_count_t i = start; i <end; i++){
    transforms[i] = transform;
  }
  return pos;
}



BatchRenderer::~BatchRenderer()
{

}
