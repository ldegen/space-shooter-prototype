#ifndef MULTIPASSRENDERER_H
#define MULTIPASSRENDERER_H
#include "renderer.h"

#include <unordered_map>
#include <memory>

class MultiPassRenderer : public Renderer
{
    std::unique_ptr<Renderer> delegatee;


    //--------------- draw state

    unordered_multimap<tid_t, vec3 > _queue;
  public:
    MultiPassRenderer(std::unique_ptr<Renderer> delegatee);
    virtual void addGeometry(tid_t tid, const vector<glm::vec2> &coordinates) override;
    virtual void removeGeometry(tid_t tid) override;
    virtual void reset() override;
    virtual void submit() override;
    virtual void draw(tid_t tid, const vec3& transform) override;
};

#endif // MULTIPASSRENDERER_H
