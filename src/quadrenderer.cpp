#include "quadrenderer.h"

QuadRenderer::QuadRenderer()
{
  for(int i=0;i<4;i++){
    delegatees.push_back(make_unique<GameRenderer>());
  }
}

void QuadRenderer::reset(unsigned int upperBound){
  c = (c+1) % 4;
  delegatees[c]->reset(upperBound);
}

void QuadRenderer::submit(){
  delegatees[c]->submit();
}

void QuadRenderer::draw(Renderer::tid_t tid, const glm::vec3 &transform){
  delegatees[c]->draw(tid,transform);
}

void QuadRenderer::addGeometry(Renderer::tid_t tid, const vector<glm::vec2> &coordinates) {
  for(size_t i=0;i<4;i++){
    delegatees[i]->addGeometry(tid,coordinates);
  }
}

void QuadRenderer::removeGeometry(Renderer::tid_t tid) {
  for(size_t i=0;i<4;i++){
    delegatees[i]->removeGeometry(tid);
  }
}
