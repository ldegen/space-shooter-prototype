#ifndef MODEL_H
#define MODEL_H


#include "mesh.h"
#include "drawable.h"

#include <string>
#include <vector>


class TextureRepository;

class Model: public Drawable
{
  public:
    unsigned int refCount;
    /*  Model Data */
    vector<Mesh> meshes;
    double mass=0;
    double area=0;
    Model():refCount(0){};
    Model(const vector<Mesh> && meshes, unsigned int refs=0, double mass=0.0, double area=0.0);
    Model(const Mesh & mesh, unsigned int refs=0, double mass=0.0, double area=0.0);

    // draws the model, and thus all its meshes
    virtual void Draw(const Shader & shader, unsigned int hints=0) const;

  private:
};


#endif
