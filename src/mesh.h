#ifndef MESH_H
#define MESH_H


#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/random.hpp>
#include "texture.h"
#include "drawable.h"
#include "protocol.h"
#include "bbox.hpp"
#include <glad/glad.h> // holds all OpenGL type declarations

#include <vector>
#include <unordered_map>
#include <string>
#include <list>

class Shader;

using namespace std;

using v_ix = unsigned short;
static GLenum gl_index_type = GL_UNSIGNED_SHORT;

struct Vertex {
  // position
  glm::vec3 Position;
 /* // normal
  glm::vec3 Normal;
  // texCoords
  glm::vec2 TexCoords;
  // tangent
  glm::vec3 Tangent;
  // bitangent
  glm::vec3 Bitangent;
  */
};


/*
 * Let's face it, I have no idea how this is done propperly in C++.
 *
 * I want Mesh-Instances to behave a bit like handles. They should not store
 * mesh data themselves, but rather setup the OpenGL buffer objects necessary to
 * render them.
 */
class Mesh : public Drawable{

  unsigned int numIndices;
  unsigned int numVertices;
  unsigned int VAO;
  unsigned int mode;
  unsigned int m_hints;
  unsigned int VBO, EBO;



  // describes the "kind" of topology. Though we have a lot of
  // meshes, we only different types of topologies.
  enum TopologyType {filled, closed_outline, separate_lines};

  // if two meshes are of the same topology type and have the same number
  // of vertices, there are indeed isomorph. They can share the same
  // element array buffer (i.e. the vertex indices).
  struct Topology {
      TopologyType type;
      unsigned int size; //number of vertices
      inline bool operator==(const Topology& rhs) const{
        return rhs.size == size && rhs.type == type;
      }
  };

  // for each topology we encounter, we
  // remember the EBO, as well as a fifo
  // of currently unreferenced VBOs
  struct PoolEntry {
      Topology topology;
      unsigned int EBO;
      std::list<unsigned int> unusedVBOs;
  };
  struct topohash {
  public:
    std::size_t operator()(const Topology &x) const
    {
      return std::hash<TopologyType>()(x.type) ^ std::hash<unsigned int>()(x.size);
    }
  };


  inline static unordered_map<Topology,PoolEntry, topohash> pool;

  // reference counters. uses
  inline static vector<unsigned int> refCounts;
  inline static unsigned int nextFreeCounter=0;
  inline static int totalVertexCount=0;
  inline static int totalIndicesCount=0;
  inline static int totalMeshCount=0;

  unsigned int createCounter();
  void freeCounter(unsigned int i);

  unsigned int counter;
  void incrementRefCount();
  void decrementRefCount();

  Topology topology;

  Mesh(const vector<Vertex> & vertices,
       const vector<v_ix> & indices,
       const unsigned int mode,
       const TopologyType topology,
       const unsigned int hints = 0);

  PoolEntry & poolEntry();


  public:

  static void printStats(){
    std::cout<<"total vertex/indices count: "<<totalVertexCount<<"/"<<totalIndicesCount<<" in "<<totalMeshCount<<" meshes"<<std::endl;
    std::cout<<"\nPool stats:"<<std::endl;
    for(auto pair : pool){
      const PoolEntry & entry = pair.second;
      std::cout<<"T.type: "<<entry.topology.type<<", T.size: "<<entry.topology.size<<", EBO: "<<entry.EBO<<", unused VBOs: "<<entry.unusedVBOs.size()<<std::endl;
    }
    std::cout<<std::endl;
  }

  static Mesh polygon2D(const vector<glm::dvec2> & coords, unsigned int hints=0) ;
  static Mesh polygon2D(const vector<glm::vec2> & coords, unsigned int hints=0) ;
  static Mesh polygon2D(const vector<float> & coords, unsigned int hints=0) ;
  static Mesh polygon2D(const Bbox<double> & box, unsigned int hints=0) ;
  static Mesh line2D(glm::vec2 a, glm::vec2 b, unsigned int hints=0);
  static Mesh cross2D(double r, unsigned int hints=0);
  static Mesh arrowHead2D(glm::vec2 point, unsigned int hints=0);

  /*  Functions  */
  // constructor
  Mesh( Mesh && other);
  Mesh( const Mesh & other);
  ~Mesh();

  Mesh & operator=(const Mesh & other);
  Mesh & operator=(Mesh && other);

  

  // render the mesh
  virtual void Draw(const Shader & shader, unsigned int hints=0) const;

  private:

  void bindBuffers() const;

  void setupVertexAttributes() const;
  /*  Functions    */
  // initializes all the buffer objects/arrays
  void setupMesh(const vector<Vertex> & vertices, const vector<v_ix> & indices);
  void cleanupMesh();

};
#endif
