#ifndef PLOTTER_H
#define PLOTTER_H

#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>

using namespace std;

class Plotter
{
  enum coltype {line, interval};
  struct colinfo{
      coltype type;
      unsigned int column;
      int lc=-1;
      int dt=-1;
  };

    string name;
    ofstream data;
    unordered_map<string, colinfo> columnMap;
    unsigned int columnCount = 1;

    bool closed = false;

  public:
    class DataPoint {
        Plotter & plotter;
        vector<double> values;
        bool flushed = false;
      public:
        DataPoint line(const string & key, double val, int lc=-1, int dt=-1) &&;
        DataPoint interval(const string & key, double a, double b, int lc=-1, int dt=-1) &&;
        void flush();
      private:
        DataPoint(Plotter & plotter, double t);
        friend Plotter;
    };
    Plotter(const string & name);
    ~Plotter();
    DataPoint at(double t);
    void close();
    friend DataPoint;

};

#endif // PLOTTER_H
