#ifndef TEXTURE_REPOSITORY_H
#define TEXTURE_REPOSITORY_H
#include "texture.h"
#include <string>
#include <map>


class TextureRepository {

  private:
    std::map<std::string, Texture> cache;
    std::string directory;

  public:

    TextureRepository(std::string directory="");
    
    Texture getTexture(const std::string & typeName, const std::string & path);

};

#endif
