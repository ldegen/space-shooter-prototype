#include <glad/glad.h>
#include "glfw-shell.h"
#include "protocol.h"
#include "flags.h"
#include "misc-utils.h"


#include <GLFW/glfw3.h>

GlfwShell::GlfwShell(Config & config, Application & app, GLFWwindow * window)
  : config(config)
  , app(app)
  , window(window)
{
}

void error_callback(int code, const char* description)
{
    error("GLFW error %d -- %s", code, description);
}

GLFWwindow* createWindow(Config & config){
  GLFWmonitor* primary = glfwGetPrimaryMonitor();
  const GLFWvidmode* mode = glfwGetVideoMode(primary);
  int width, height;
  if(config.screenWidth()){
    width = static_cast<int>(config.screenWidth());
  } else if (config.fullscreen()){
    width = mode->width;
  } else {
    width = 1280;
  }
  if(config.screenHeight()){
    height = static_cast<int>(config.screenHeight());
  } else if (config.fullscreen()){
    height = mode->height;
  } else {
    height = 720;
  }

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, MY_GL_MAJOR_VERSION);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, MY_GL_MINOR_VERSION);
#ifdef GLES2
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
#elif MY_GL_MAJOR_VERSION > 3
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
#endif

  // anti-aliasing
  //glfwWindowHint(GLFW_SAMPLES, 4);
  glfwSetErrorCallback(error_callback);

#ifdef __APPLE__
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

  // glfw window creation
  // --------------------
  GLFWwindow* window = glfwCreateWindow(width, height, "Bang! Bang!", (config.fullscreen() ? primary : NULL), NULL);
  return window;
}

int GlfwShell::run(Config & config, Application & app){
  // glfw: initialize and configure
  // ------------------------------
  glfwInit();

  GLFWwindow* window = createWindow(config);

  if (window == NULL) {
    error("Failed to create GLFW window");
    glfwTerminate();
    return -1;
  }

  //Don't trust this callback atm. We call it manually in pollEvents instead.
  //glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
  glfwSetScrollCallback(window, scroll_callback);
  glfwSetKeyCallback(window, key_callback);
  //glfwSetInputMode(window, GLFW_STICKY_KEYS, GLFW_TRUE);
  glfwSetCursorPosCallback(window, cursor_position_callback);
  glfwSetMouseButtonCallback(window, mouse_button_callback);


  int present = glfwJoystickPresent(GLFW_JOYSTICK_1);
  info("joystic 1 present: %d", present);
  present = glfwJoystickPresent(GLFW_JOYSTICK_2);
  info("joystic 2 present: %d", present);

  GlfwShell instance(config, app, window);
  glfwSetWindowUserPointer(window, &instance);

  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

  instance.makeContextCurrent();

  glfwSwapInterval(config.swapInterval());
  //glfwSwapInterval(-1);
  // glad: load all OpenGL function pointers
  // ---------------------------------------
  if (!LOAD_GL_LOADER((GLADloadproc)glfwGetProcAddress)) {
    error("Failed to initialize GLAD");
    return -1;
  }

  instance.makeContextNonCurrent();

  int exitCode = app.run(instance);
  glfwTerminate();
  return exitCode;
}

void GlfwShell::checkGlfwError()
{
  const char * err;
  if(glfwGetError(&err)){
    error(err);
  }
}

void GlfwShell::makeContextCurrent()
{
  glfwMakeContextCurrent(window);
  checkGlfwError();
}

void GlfwShell::makeContextNonCurrent()
{
  glfwMakeContextCurrent(nullptr);
  checkGlfwError();
}

unsigned int buttonPressed(GLFWwindow * window, unsigned int keycode, unsigned int mask){
  if(glfwGetKey(window, keycode) == GLFW_PRESS) {
    return mask;
  }
  return 0;
}

void GlfwShell::checkJoystick( unsigned int & bs, bool second)
{
  auto jst = second ? GLFW_JOYSTICK_2:GLFW_JOYSTICK_1;
  unsigned int shift = second ? PER_PLAYER_BUTTONS : 0;
  if(glfwJoystickPresent(jst)){
    int count;
    const float* axes = glfwGetJoystickAxes(jst, &count);
    if(count >= 1){
      if (axes[0] < -0.5){
        bs |= (BTN_TURN_LEFT_A<<shift);
      }
      if (axes[0] > 0.5){
        bs |= (BTN_TURN_RIGHT_A<<shift);
      }
    }
    if(count >= 2){
      if (axes[1] < -0.5){
        bs |= (BTN_FORWARD_THRUST_A<<shift);
      }
      if (axes[1] > 0.5){
        bs |= (BTN_REVERSE_THRUST_A<<shift);
      }
    }
    const unsigned char* bstates = glfwGetJoystickButtons(jst, &count);
    unsigned int mask = 0;
    for(int i=0;i<count;i++){
      if(bstates[i]==GLFW_PRESS) {
        mask |= 1 << i;
        bs |= (BTN_FIRE_A<<shift);
      }
    }
    if(0xf == (mask & 0xf)){ // buttons 0,1,2,3 pressed
      bs |= (BTN_QUIT_GUESTURE<<shift);
    }

  }
  checkGlfwError();
}



void GlfwShell::pollEvents(){
  int newWidth, newHeight;
  glfwGetFramebufferSize(window, &newWidth, &newHeight);
  checkGlfwError();
  if(newWidth != fbWidth || newHeight != fbHeight){
    fbWidth = newWidth;
    fbHeight = newHeight;
    framebuffer_size_callback(window, fbWidth, fbHeight);
  }
  glfwPollEvents();
  checkGlfwError();
  unsigned int bs = 0;
 // if(checkButtons){
 //   checkButtons = false;
    bs |= buttonPressed(window, GLFW_KEY_R, BTN_FORWARD_THRUST_A);
    bs |= buttonPressed(window, GLFW_KEY_F, BTN_REVERSE_THRUST_A);
    bs |= buttonPressed(window, GLFW_KEY_D, BTN_TURN_LEFT_A);
    bs |= buttonPressed(window, GLFW_KEY_G, BTN_TURN_RIGHT_A);
    bs |= buttonPressed(window, GLFW_KEY_Z, BTN_FIRE_A);
    bs |= buttonPressed(window, GLFW_KEY_E, BTN_STOP_A);
    bs |= buttonPressed(window, GLFW_KEY_UP, BTN_FORWARD_THRUST_A<<PER_PLAYER_BUTTONS);
    bs |= buttonPressed(window, GLFW_KEY_DOWN, BTN_REVERSE_THRUST_A<<PER_PLAYER_BUTTONS);
    bs |= buttonPressed(window, GLFW_KEY_LEFT, BTN_TURN_LEFT_A<<PER_PLAYER_BUTTONS);
    bs |= buttonPressed(window, GLFW_KEY_RIGHT, BTN_TURN_RIGHT_A<<PER_PLAYER_BUTTONS);
    bs |= buttonPressed(window, GLFW_KEY_SPACE, BTN_FIRE_A<<PER_PLAYER_BUTTONS);
    bs |= buttonPressed(window, GLFW_KEY_ENTER, BTN_STOP_A<<PER_PLAYER_BUTTONS);
    bs |= buttonPressed(window, GLFW_KEY_P, BTN_TOGGLE_PAUSE);
    bs |= buttonPressed(window, GLFW_KEY_ESCAPE, BTN_QUIT);
    bs |= buttonPressed(window, GLFW_KEY_V, BTN_VIEW);
  //}
  checkJoystick(bs, false);
  checkJoystick(bs, true);
  if(bs!=buttons){
    app.buttonStateChanged(*this, bs);
  }
  buttons = bs;
}
void GlfwShell::swapBuffers(){
  glfwSwapBuffers(window);
  checkGlfwError();
}
WindowSize GlfwShell::windowSize(){
  WindowSize ws;
  glfwGetFramebufferSize(window, &ws.width, &ws.height);
  checkGlfwError();
  return ws;
}
unsigned int GlfwShell::buttonState(){
  return buttons;
}
MouseState GlfwShell::mouseState(){
  MouseState ms;
  glfwGetCursorPos(window, &ms.x, &ms.y);
  ms.buttons = GLFW_PRESS == glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
  return ms;
}
double GlfwShell::time(){
  return glfwGetTime();
}
bool GlfwShell::windowShouldClose(){
  return glfwWindowShouldClose(window);
}
void GlfwShell::requestWindowClose(){
  glfwSetWindowShouldClose(window, true);
}

void GlfwShell::framebuffer_size_callback(GLFWwindow* window, int width, int height){
  GlfwShell * w = (GlfwShell * )glfwGetWindowUserPointer(window);
  debug("resize: %d x %d", width, height);
  w->app.windowResized(*w, width, height);
}
void GlfwShell::scroll_callback(GLFWwindow* window, double xoffset, double yoffset){
  GlfwShell * w = (GlfwShell * )glfwGetWindowUserPointer(window);
  w->app.mouseScrolled(*w, xoffset, yoffset);
}
void GlfwShell::cursor_position_callback(GLFWwindow* window, double xpos, double ypos){
  GlfwShell * w = (GlfwShell * )glfwGetWindowUserPointer(window);
  w->app.mouseMoved(*w, xpos, ypos);
}
void GlfwShell::mouse_button_callback(GLFWwindow* window, int button, int action, int mods){
  GlfwShell * w = (GlfwShell * )glfwGetWindowUserPointer(window);
  double xpos, ypos;
  glfwGetCursorPos(window, &xpos, &ypos);
  if (action == GLFW_PRESS){
    w->app.mouseButtonPressed(*w, xpos,ypos);
  } else if (action == GLFW_RELEASE){
    w->app.mouseButtonReleased(*w, xpos,ypos);
  }
}
void GlfwShell::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){
  GlfwShell * w = (GlfwShell * )glfwGetWindowUserPointer(window);
  w->checkButtons=true;
}
