#include "multipassrenderer.h"

MultiPassRenderer::MultiPassRenderer(std::unique_ptr<Renderer> delegatee)
  :delegatee(move(delegatee))
{

}

void MultiPassRenderer::addGeometry(Renderer::tid_t tid, const vector<glm::vec2> &coordinates)
{
  delegatee->addGeometry(tid,coordinates);
}

void MultiPassRenderer::removeGeometry(Renderer::tid_t tid)
{
  delegatee->removeGeometry(tid);
}

void MultiPassRenderer::reset()
{
  // nothing to do here, see submit()
}

void MultiPassRenderer::submit()
{
  while(!_queue.empty()){
    delegatee->reset();
    auto it = _queue.begin();
    auto next = _queue.end();
    while(it != _queue.end()){
      tid_t tid = it->first;
      delegatee->draw(tid,it->second);
      tie(ignore,next) = _queue.equal_range(tid);
      _queue.erase(it);
      it = next;
    }
    delegatee->submit();
  }
}

void MultiPassRenderer::draw(Renderer::tid_t tid, const glm::vec3 &transform)
{
  _queue.insert(make_pair(tid,transform));
}
