#ifndef SDL_SHELL_H
#define SDL_SHELL_H
#include "application.h"
#include "shell.h"
#include "config.hpp"
#include <SDL.h>


class SdlShell : public Shell {
  Application & app;
  Config & config;
  SDL_Window * window;
  SDL_GLContext context;
  unsigned int buttons;
  bool closeRequested = false;
  
 
  private:
  static int withWindow(Config & config, Application & app, SDL_Window * window);
  SdlShell(Config & config, Application & app, SDL_Window * window, SDL_GLContext cx);
  public: 


  static int run(Config & config, Application & app);

  virtual void pollEvents() override;
  virtual void swapBuffers() override;
  virtual WindowSize windowSize() override;
  virtual unsigned int buttonState() override;
  virtual MouseState mouseState() override;
  virtual double time() override;
  virtual bool windowShouldClose() override;
  virtual void requestWindowClose() override;
  virtual void makeContextCurrent() override;
  virtual void makeContextNonCurrent() override;

};
#endif
