#ifndef TRACKBALL_H
#define TRACKBALL_H

#include <glm/gtc/quaternion.hpp>
#include <glm/vec3.hpp> 

class Trackball {
  private:
    glm::dvec3 project(int x, int y);
    glm::dvec3 lastPos;
    glm::dquat q = glm::angleAxis(0.0, glm::dvec3(0.0,1.0,0.0));
    int tracking = 0;
    int height = 1;
    int width = 1;
  public:
    void motion(const int x, const int y);
    void viewport(const int width, const int height);
    void begin(const int x, const int y);
    void end();
    glm::dquat orientation();

};

#endif
