#ifndef FLAGS_H
#define FLAGS_H
//#define GLES2

//#define DEBUG_PROTOCOL
#define GL_CHECK_ERROR

#ifdef GLES2
#define GLFW_INCLUDE_ES2
#define LOAD_GL_LOADER gladLoadGLES2Loader
#define MY_GL_MAJOR_VERSION 2
#define MY_GL_MINOR_VERSION 0
#else
#define USE_VAO
#define LOAD_GL_LOADER gladLoadGLLoader
#define MY_GL_MAJOR_VERSION 2
#define MY_GL_MINOR_VERSION 1
#endif

#endif
