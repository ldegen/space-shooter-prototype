#include "shape.h"
#include <iostream>
Shape::Shape(const std::vector<glm::dvec2> & vertices, double density, double friction, double elasticity, unsigned int hints)
  : m_vertices(vertices)
  , m_density(density)
  , m_friction(friction)
  , m_elasticity(elasticity)
  , m_hints(hints)
{}

Shape Shape::normalized() const{
  glm::dvec2 C = centroid();
  std::vector<glm::dvec2> newVertices;
  newVertices.reserve(m_vertices.size());
  for (auto v : m_vertices) {
    newVertices.push_back(v - C);
  }
  Shape newShape(newVertices, m_density, m_friction, m_elasticity, m_hints);
  newShape.J = momentCentroid();
  newShape.A = area();
  newShape.momentsKnown = true;
  return newShape;
};
void Shape::calculate() const{
  if(momentsKnown){
    return; 
  }
  int n = m_vertices.size();
  A=0.0;
  Cx=0.0;
  Cy=0.0;
  J=0.0;
  double tmp = 0;
  for (int i=0; i<n; i++){
    const glm::dvec2 & v = m_vertices[i];
    const glm::dvec2 & w = m_vertices[i+1 < n ? i+1 : 0];
    tmp = v.x*w.y - w.x*v.y;
    A += tmp;
    Cx += (v.x + w.x) * tmp;
    Cy += (v.y + w.y) * tmp;
    //J += (v.x*w.y + 2*v.x*v.y + 2*w.x*w.y + w.x*v.y) * tmp;
    J += ( v.y*v.y + v.y*w.y + w.y*w.y 
         + v.x*v.x + v.x*w.x + w.x*w.x ) * tmp;
        
  }
  A /= 2;
  Cx /= (6*A);
  Cy /= (6*A);
  J /= 12;

  momentsKnown = true;
}

double Shape::area() const{
  calculate();
  return A;
}

glm::dvec2 Shape::centroid() const{
  calculate();
  return glm::dvec2 {Cx, Cy};
}

double Shape::mass() const{
  calculate();
  return m_density * A;
}

double Shape::momentOrigin() const{
  calculate();
  return m_density * J;
}

double Shape::momentCentroid() const{
  //As I understand it, this should work due to Steiner's Theorem.
  return momentOrigin() - mass() * (Cx*Cx + Cy*Cy);
}

double Shape::momentAt(glm::dvec2 p) const{
  glm::dvec2 r = p - centroid();
  return momentCentroid() + mass() * (r.x*r.x + r.y*r.y);
}

