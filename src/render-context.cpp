#include "render-context.h"
#include <iostream>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>

mat4 RenderContext::model() const &{
  //return m_delta.model ? *m_delta.model : m_base.model;
  return m_delta.model.value_or(m_base.model);
}
mat4 RenderContext::view() const &{
  //return m_delta.view ? *m_delta.view : m_base.view;
  return m_delta.view.value_or(m_base.view);
}

mat4 RenderContext::projection() const &{
  //return m_delta.projection ? *m_delta.projection : m_base.projection;
  return m_delta.projection.value_or(m_base.projection);
}
Shader RenderContext::shader() const &{
  //return m_delta.shader ? *m_delta.shader : m_base.shader;
  return m_delta.shader.value_or(m_base.shader);
}
unsigned int RenderContext::hints() const &{
  //return m_delta.hints ? *m_delta.hints : m_base.hints;
  return m_delta.hints.value_or(m_base.hints);
}


RenderContext RenderContext::transform(const mat4 & tf) && {
  m_delta.model = model() * tf;
  return std::move(*this);
}
RenderContext RenderContext::transform(const mat4 & tf) const & {
  RenderContext clone {*this};
  return std::move(clone).transform(tf);
}
RenderContext RenderContext::shader(const Shader & s) &&{
  m_delta.shader=s;
  return std::move(*this);
}
RenderContext RenderContext::shader(const Shader & s) const &{
  RenderContext clone {*this};
  return std::move(clone).shader(s);
}

RenderContext RenderContext::hints(unsigned int h) &&{
  m_delta.hints = hints() | h;
  return std::move(*this);
}
RenderContext RenderContext::hints(unsigned int h) const &{
  RenderContext clone {*this};
  return std::move(clone).hints(h);
}


//RenderContext::RenderContext(base_cx && base)
//  : m_base{std::move(base)}
//  , m_delta{}
//{
//}
RenderContext::RenderContext(const base_cx & base)
  : m_base{base}
  , m_delta{}
{
}
RenderContext::RenderContext(init_cx && init)
  : m_delta { .model=std::move(init.model), 
             .view=std::move(init.view), 
             .projection=std::move(init.projection),
             .shader=std::move(init.shader),
             .hints=std::move(init.hints)
           }
  , m_base { .model=*m_delta.model, 
             .view=*m_delta.view, 
             .projection=*m_delta.projection,
             .shader=*m_delta.shader,
             .hints=*m_delta.hints
           }
{

}
RenderContext::RenderContext(const init_cx & init)
  : m_base { .model=std::move(init.model), 
             .view=std::move(init.view), 
             .projection=std::move(init.projection),
             .shader=std::move(init.shader),
             .hints=std::move(init.hints)
           }
  , m_delta {}
{

}


RenderContext & RenderContext::render(function<void(RenderContext &&)> f) {
  base_cx base {
    .model = model(),
    .view = view(),
    .projection = projection(),
    .shader = shader(),
    .hints = hints()
  };
  RenderContext cx {base};

  f(std::move(cx));
  return *this;
}

RenderContext & RenderContext::render(const Drawable & f){
  const Shader & s = shader();
  s.use();
  //std::cout<<"render f, model="<<glm::to_string(model())<<std::endl;
  s.setMat4("model", model());
  s.setMat4("view", view());
  s.setMat4("projection", projection());
  f.Draw(s, hints());
  return *this;
}
