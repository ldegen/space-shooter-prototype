#ifndef SHATTER_H
#define SHATTER_H

#include "shape.h"
#include "bbox.hpp"
#include "objbuilder.h"
#include <clipper.hpp>
#include <vector>

typedef ClipperLib::IntPoint Point;
typedef ClipperLib::cInt cInt;
class Shatter {
  //boost voronoi uses integral coordinates for input.
  //This setting controls the resolution of the integral grid.
  //A "unit" is 1.0 in our normal world/physics coordinates
  double pixelsPerUnit=48;
  ObjBuilder objBuilder;

  public:

  std::vector<Shape> shatter(const Shape & s, const std::vector<Point> & input); 
  std::vector<Shape> shatter(const Shape & s, const std::vector<glm::dvec2> & input);
  std::vector<Shape> shatter(const Shape & s, int numSites=10, const glm::dvec2 & mean={0.0,0.0}, const double deviation=0.3);
  std::vector<std::vector<Point>> shatter(const std::vector<Point> & contour, const std::vector<Point> & input, const Bbox<double> & bbox); 
  void __debug__flush(ostream & out);
};

#endif
