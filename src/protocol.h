#ifndef PROTOCOL_H
#define PROTOCOL_H
#include "shape.h"
#include "body-type.h"
#include <cstddef>
#include <optional>
#include <stdint.h>
#include <vector>
#include <iostream>
#include <memory>

#define MSG_INIT                  1
#define MSG_BODY_CREATED          2
#define MSG_BODY_DELETED          3
#define MSG_BUTTON_STATE_CHANGED  4
#define MSG_ERROR                 5
#define CMD_CREATE                6
#define CMD_DELETE                7
#define CMD_SELECT_UID            8
#define CMD_SET_X_Y_THETA         9
#define CMD_SET_DX_DY_DTHETA      10
#define CMD_SET_DTHETA            11
#define CMD_SET_FX_FY_TORQUE      12
#define CMD_CREATE_BODY_TYPE      13
#define MSG_BLIPS                 14
#define MSG_COLLISION             15
#define CMD_APPLY_IMPULSE         16
#define CMD_LOOK_AT               17
#define CMD_TAG                   18
#define CMD_HINT                  19
#define CMD_PAUSE                 20
#define CMD_RESUME                21
#define CMD_TOGGLE_PAUSE          22
#define CMD_PLAY_SOUND            23
#define CMD_QUIT                  24
#define MSG_CLOSE_REQUESTED       25
#define MSG_WINDOW_RESIZED        26
#define ERR_BAD_ALIAS             1
#define ERR_BAD_UID               2

// used to left-shift button for player A
// to get corresponding button for player B
#define PER_PLAYER_BUTTONS        7u

#define BTN_TOGGLE_PAUSE          1<<0
#define BTN_QUIT                  1<<1
#define BTN_VIEW                  1<<2

#define BTN_FORWARD_THRUST_A      1<<3
#define BTN_REVERSE_THRUST_A      1<<4
#define BTN_TURN_LEFT_A           1<<5
#define BTN_TURN_RIGHT_A          1<<6
#define BTN_FIRE_A                1<<7
#define BTN_STOP_A                1<<8
#define BTN_QUIT_GUESTURE         1<<9



#define TAG_TRACK           1
#define TAG_PLAYER          3
#define TAG_GRAVITY_SOURCE  4
#define TAG_STATIC          8

// layout of the flag bits is as follows
// 0-3: touching limbo (N/W/S/E)
// 4-7: in limbo (N/W/S/E)
// 8-11: gone (a.k.a. nirvana) (N/W/S/E)
// 12-13: occurance id (there are at most four occurances)

#define MASK_TOUCHING_LIMBO     0x000f
#define MASK_IN_LIMBO           0x00f0
#define MASK_GONE               0x0f00
#define MASK_OCCURENCE          0x3000
#define LSHIFT_OCCURENCE        12
#define FLAG_TOUCHING_LIMBO_N   1
#define FLAG_TOUCHING_LIMBO_W   2
#define FLAG_TOUCHING_LIMBO_NW  3
#define FLAG_TOUCHING_LIMBO_S   4
#define FLAG_TOUCHING_LIMBO_SW  6
#define FLAG_TOUCHING_LIMBO_E   8
#define FLAG_TOUCHING_LIMBO_NE  9
#define FLAG_TOUCHING_LIMBO_SE 12
#define FLAG_IN_LIMBO_N        16
#define FLAG_IN_LIMBO_W        32
#define FLAG_IN_LIMBO_NW       48
#define FLAG_IN_LIMBO_S        64
#define FLAG_IN_LIMBO_SW       96
#define FLAG_IN_LIMBO_E       128
#define FLAG_IN_LIMBO_NE      144
#define FLAG_IN_LIMBO_SE      192
#define FLAG_GONE_N           256
#define FLAG_GONE_W           512
#define FLAG_GONE_NW          768
#define FLAG_GONE_S          1024
#define FLAG_GONE_SW         1536
#define FLAG_GONE_E          2048
#define FLAG_GONE_NE         2304
#define FLAG_GONE_SE         3072


#define HINT_PLAYER_A       1
#define HINT_PLAYER_B       2
#define HINT_FILL           4
#define HINT_DEBUG          8

struct State {
  double x=0;
  double y=0;
  double theta=0;
  double dx=0;
  double dy=0;
  double dtheta=0;
  unsigned int flags=0;
};



struct Blip {
  unsigned long uid;
  unsigned int tid;
  State state;
  double r;
  unsigned int tags;
  unsigned int hints;
};
struct msg_init {
  uint8_t type=MSG_INIT;
};
struct cmd_pause {
  uint8_t type=CMD_PAUSE;
};
struct msg_close_requested {
  uint8_t type=MSG_CLOSE_REQUESTED;
};
struct cmd_quit {
  uint8_t type=CMD_QUIT;
};
struct cmd_resume {
  uint8_t type=CMD_RESUME;
};
struct cmd_toggle_pause {
  uint8_t type=CMD_TOGGLE_PAUSE;
};
struct cmd_play_sound {
  uint8_t type=CMD_PLAY_SOUND;
  double x;
  double y;
  double preset=0;
  double velocity=0.0;
  double pitch=0.0;
};

struct msg_window_resized {
  uint8_t type=MSG_WINDOW_RESIZED;
  uint32_t width;
  uint32_t height;
};

struct msg_body_created {
  uint8_t type=MSG_BODY_CREATED;
  uint64_t uid;
  uint32_t tid;
};
struct msg_body_deleted {
  uint8_t type=MSG_BODY_DELETED;
  uint64_t uid;
  uint32_t tid;
};
struct msg_button_state_changed {
  uint8_t type=MSG_BUTTON_STATE_CHANGED;
  uint32_t mask;
};
struct msg_error {
  uint8_t type=MSG_ERROR;
  uint32_t code;
  uint64_t arg;
};
struct msg_blips {
  uint8_t type=MSG_BLIPS;
  double t;
  bool pause;
  uint32_t num_blips;
  uint32_t num_tags;
};

struct cmd_create {
  uint8_t type=CMD_CREATE;
  uint64_t uid;
  uint32_t tid;
  bool static_body = false;
};
struct cmd_delete {
  uint8_t type=CMD_DELETE;
};
struct cmd_select_uid {
  uint8_t type=CMD_SELECT_UID;
  uint64_t uid;
};
struct cmd_set_x_y_theta {
  uint8_t type=CMD_SET_X_Y_THETA;
  double x;
  double y;
  double theta;
  uint64_t rel=0;
};
struct cmd_set_dx_dy_dtheta {
  uint8_t type=CMD_SET_DX_DY_DTHETA;
  double dx;
  double dy;
  double dtheta;
};
struct cmd_set_dtheta {
  uint8_t type=CMD_SET_DTHETA;
  double dtheta;
};
struct cmd_set_fx_fy_torque {
  uint8_t type=CMD_SET_FX_FY_TORQUE;
  double fx;
  double fy;
  double torque;
};
struct cmd_apply_impulse {
  uint8_t type=CMD_APPLY_IMPULSE;
  double jx;
  double jy;
  double rx;
  double ry;
};

struct cmd_create_body_type {
  uint8_t type=CMD_CREATE_BODY_TYPE;
  uint32_t tid;
  uint8_t num_shapes;
};

struct cmd_look_at {
  uint8_t type=CMD_LOOK_AT;
  double left;
  double bottom;
  double right;
  double top;
};
struct cmd_tag {
  uint8_t type=CMD_TAG;
  uint32_t unset=0;
  uint32_t set=0;
};

struct cmd_hint {
  uint8_t type=CMD_HINT;
  uint32_t unset=0;
  uint32_t set=0;
};
struct msg_collision {
  uint8_t type=MSG_COLLISION;
  Blip blip_a;
  Blip blip_b;
  double jx_a;
  double jy_a;
  double jx_b;
  double jy_b;
  uint8_t num_contacts;
};

struct data_contact {
  double x_a;
  double y_a;
  double x_b;
  double y_b;
  double normal_x_a;
  double normal_y_a;
  double normal_x_b;
  double normal_y_b;
  double depth;
};

struct data_shape {
  double density;
  double elasticity;
  double friction;
  uint32_t hints;
  uint32_t num_vertices;
};

struct data_vertex {
  double x;
  double y;
};

struct data_tag {
  unsigned int tags;
  unsigned int index;
};

using buffer_t =std::vector<uint8_t>;

class ProtocolListener {
  public:
  virtual void onMessage(const msg_init * msg) {}
  virtual void onMessage(const cmd_pause * msg) {}
  virtual void onMessage(const cmd_quit * msg) {}
  virtual void onMessage(const msg_close_requested * msg) {}
  virtual void onMessage(const cmd_resume * msg) {}
  virtual void onMessage(const cmd_toggle_pause * msg) {}
  virtual void onMessage(const cmd_play_sound * msg) {}
  virtual void onMessage(const msg_body_created *msg) {}
  virtual void onMessage(const msg_window_resized *msg) {}
  virtual void onMessage(const msg_body_deleted *msg) {}
  virtual void onMessage(const msg_button_state_changed *msg) {}
  virtual void onMessage(const msg_error *msg){}
  virtual void onMessage(const cmd_create *cmd){}
  virtual void onMessage(const cmd_delete *cmd){}
  virtual void onMessage(const cmd_select_uid *cmd){}
  virtual void onMessage(const cmd_set_x_y_theta *cmd){}
  virtual void onMessage(const cmd_set_dx_dy_dtheta *cmd){}
  virtual void onMessage(const cmd_set_dtheta *cmd){}
  virtual void onMessage(const cmd_set_fx_fy_torque *cmd){}
  virtual void onMessage(const cmd_apply_impulse *cmd){}
  virtual void onMessage(const cmd_look_at *cmd){}
  virtual void onMessage(const cmd_tag *cmd){}
  virtual void onMessage(const cmd_hint *cmd){}
  virtual void onBodyType(const BodyType & bt){}
  virtual void onBlips(const msg_blips & msg, const Blip * blips, const data_tag * tags){}
  virtual void onCollision(const msg_collision & msg, const data_contact * contacts){}
};

class ProtocolSink {
  public:
    virtual std::optional<size_t> push(const uint8_t * buffer, size_t size ) = 0;
};


class ProtocolMultiListener : public ProtocolListener {

  private:
    const std::vector<ProtocolListener *> delegatees;
    template<typename M>
    void dispatch(const M* msg){
        for(auto delegatee : delegatees){
          delegatee->onMessage(msg); 
        }
    };
  public:
    ProtocolMultiListener(const std::vector<ProtocolListener *> && delegatees)
      : delegatees {delegatees}
    {}
  virtual void onMessage(const msg_init * msg) {dispatch<msg_init>(msg);};
  virtual void onMessage(const cmd_pause * msg) {dispatch<cmd_pause>(msg);};
  virtual void onMessage(const cmd_quit * msg) {dispatch<cmd_quit>(msg);};
  virtual void onMessage(const msg_close_requested * msg) {dispatch<msg_close_requested>(msg);};
  virtual void onMessage(const cmd_resume * msg) {dispatch<cmd_resume>(msg);};
  virtual void onMessage(const cmd_toggle_pause * msg) {dispatch<cmd_toggle_pause>(msg);};
  virtual void onMessage(const cmd_play_sound * msg) {dispatch<cmd_play_sound>(msg);};
  virtual void onMessage(const msg_window_resized *msg) {dispatch(msg);};
  virtual void onMessage(const msg_body_created *msg) {dispatch(msg);};
  virtual void onMessage(const msg_body_deleted *msg) {dispatch(msg);};
  virtual void onMessage(const msg_button_state_changed *msg) {dispatch(msg);};
  virtual void onMessage(const msg_error *msg){dispatch(msg);};
  virtual void onMessage(const cmd_create *cmd){dispatch(cmd);};
  virtual void onMessage(const cmd_delete *cmd){dispatch(cmd);};
  virtual void onMessage(const cmd_select_uid *cmd){dispatch(cmd);};
  virtual void onMessage(const cmd_set_x_y_theta *cmd){dispatch(cmd);};
  virtual void onMessage(const cmd_set_dx_dy_dtheta *cmd){dispatch(cmd);};
  virtual void onMessage(const cmd_set_dtheta *cmd){dispatch(cmd);};
  virtual void onMessage(const cmd_set_fx_fy_torque *cmd){dispatch(cmd);};
  virtual void onMessage(const cmd_apply_impulse *cmd){dispatch(cmd);};
  virtual void onMessage(const cmd_look_at *cmd){dispatch(cmd);};
  virtual void onMessage(const cmd_tag *cmd){dispatch(cmd);};
  virtual void onMessage(const cmd_hint *cmd){dispatch(cmd);};
  virtual void onBodyType(const BodyType & bt){
    for(auto delegatee : delegatees){
      delegatee->onBodyType(bt); 
    }
  }
  virtual void onBlips(const msg_blips & msg, const Blip * blips, const data_tag * tags){
    for(auto delegatee : delegatees){
      delegatee->onBlips(msg, blips, tags); 
    }
  }
  virtual void onCollision(const msg_collision & collision, const data_contact * contacts){
    for(auto delegatee : delegatees){
      delegatee->onCollision(collision, contacts); 
    }
  }
};

class FilteringProtocolListener: public ProtocolListener {
  virtual bool examineTags(const msg_blips & header, unsigned int tags){return true;};
  virtual void beforeBlips(const msg_blips & header, const Blip * blips, const data_tag * tags){};
  virtual void afterBlips(const msg_blips & header, const Blip * blips, const data_tag * tags) {};
  virtual void onBlip(const msg_blips & header, const Blip & bli) =0;
  virtual void onBlips(const msg_blips & msg, const Blip * blips, const data_tag * tags){
    beforeBlips(msg, blips, tags);
    int j=-1;
    for(int i=0;i<msg.num_tags;i++){
      int index = tags[i].index;
      if (index > j) { //don't visit blips more than once
        for (j=index; j < msg.num_blips && examineTags(msg,blips[j].tags); j++){
          onBlip(msg, blips[j]);
        }
      }
    }
    afterBlips(msg, blips, tags);
  }
};

class ProtocolWriter : public ProtocolListener {

  std::unique_ptr<buffer_t> buffer;

  public:
 
  template<typename M>
  void onMessage(const M * msg){
    size_t msgSize = sizeof(M);
    const uint8_t * src = reinterpret_cast<const uint8_t*>(msg);
#ifdef DEBUG_PROTOCOL
    unsigned int i = *src;
    std::cout << "encoding " << typeid(M).name() <<"("<<i<<")"<<std::endl;
#endif
    buffer->insert(buffer->end(), src, src+msgSize);
  }

  template<typename M>
  void write(const M msg){
    onMessage(&msg);
  }
  virtual void onBodyType(const BodyType & bt);
  virtual void onBlips(const msg_blips & msg, const Blip * blips, const data_tag * tags);
  virtual void onCollision(const msg_collision & collision, const data_contact * contacts);
  std::unique_ptr<buffer_t> swap(std::unique_ptr<buffer_t> && newBuffer);
};
class ProtocolParser : public ProtocolSink {

  ProtocolListener & listener;
  unsigned int remainingShapes = 0;
  unsigned int remainingVertices = 0;
  unsigned int remainingBlips = 0;
  unsigned int remainingTags = 0;
  unsigned int remainingContacts = 0;

  msg_collision collisionHeader;
  cmd_create_body_type btHeader;
  data_shape spHeader;
  msg_blips blipsHeader;
  

  std::vector<data_contact> contacts;
  std::vector<Shape> shapes;
  std::vector<glm::dvec2> vertices;
  std::vector<Blip> blips;
  std::vector<data_tag> tags;
  private:
    void flushBlips();
    void flushShape();
    void flushBodyType();
    void flushCollision();
    std::optional<size_t> readCollision( const uint8_t * buffer, size_t size );
    std::optional<size_t> readContact( const uint8_t * buffer, size_t size );
    std::optional<size_t> readBodyType( const uint8_t * buffer, size_t size );
    std::optional<size_t> readBlips( const uint8_t * buffer, size_t size );
    std::optional<size_t> readBlip( const uint8_t * buffer, size_t size );
    std::optional<size_t> readTag( const uint8_t * buffer, size_t size );
    std::optional<size_t> readShape( const uint8_t * buffer, size_t size );
    std::optional<size_t> readVertex( const uint8_t * buffer, size_t size );
    std::optional<size_t> readMessage( const uint8_t * buffer, size_t size );
  public:
    ProtocolParser(ProtocolListener & listener);
    std::optional<size_t> push(const uint8_t * buffer, size_t size );
};

#endif
