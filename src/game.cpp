#include "game.h"
#include "misc-utils.h"
#include "shell.h"
#include <thread>

Game::Game(Config & config)
  : config(config)
  , writer() 
  , logic {config, writer}
  , physics {config, writer}
{
  // put a number of empty buffers. Neither backend nor glStuff will
  // create new buffers, so this is the total number of buffers that there will
  // ever be. Note, however, that their size may increase dynamically.

  upstream.push(make_unique<buffer_t>(sizeof(Blip)* 1024));
  upstream.push(make_unique<buffer_t>(sizeof(Blip)* 1024));
  upstream.push(make_unique<buffer_t>(sizeof(Blip)* 1024));
  // FIXME: this is ugly
  writer.swap(make_unique<buffer_t>(sizeof(Blip)* 1024));

}


int Game::run(Shell &shell){
  std::srand(std::time(nullptr));
  std::thread glThread (bind(&Game::runGlStuff, this, &shell));
  runBackend(&shell);
  glThread.join();
  return 0;
}
void Game::buttonStateChanged(Shell &shell, unsigned int state){
  if(state & BTN_QUIT){
    shell.requestWindowClose(); 
  }
  writer.write<msg_button_state_changed>({.mask=state});
}
void Game::mouseButtonPressed(Shell &shell, double x, double y){
  //glStuff.mouseButtonDown(x,y);
}
void Game::mouseButtonReleased(Shell &shell, double x, double y){
  //glStuff.mouseButtonUp(x,y);
}
void Game::mouseMoved(Shell &shell, double x, double y){
  //glStuff.mouseMoved(x,y);
}
void Game::mouseScrolled(Shell &shell, double xOffset, double yOffset){
  //glStuff.scrolled(xOffset, yOffset);
}
void Game::windowResized(Shell &shell, int width, int height){
  //glStuff.resize(width, height);
  writer.write<msg_window_resized>({.width = static_cast<uint32_t>(width), .height=static_cast<uint32_t>(height)});
}

void Game::windowCloseRequested(Shell &shell)
{
  writer.write<msg_close_requested>({});
}

void Game::onExit(Shell &shell)
{
  logic.onExit();
}







void Game::runBackend(Shell *shell)
{
  info("running backend");
  ProtocolMultiListener listener {{&logic, &physics}};
  ProtocolParser parser {listener};

  writer.write<msg_init>({MSG_INIT});
  while(logic.keepRunning()){
    shell->pollEvents();
    physics.step();
    physics.blips();
    auto buf = writer.swap(upstream.pop());
    parser.push(buf->data(), buf->size());
    downstream.push(move(buf));
  }
  downstream.push(writer.swap(upstream.pop()));
  info("exit backend");
  info("upstream had a total of %u underruns", upstream.underruns);
}

void Game::runGlStuff(Shell *shell)
{
  shell->makeContextCurrent();
  info("running gl thread");
  GlStuff glStuff(config);
  ProtocolParser parser {glStuff};

  while(glStuff.keepRunning()){
    auto buf = downstream.pop();
    parser.push(buf->data(), buf->size());
    glCheckError();
    upstream.push(move(buf));
    shell->swapBuffers();
  }
  shell->makeContextNonCurrent();
  info("exit gl thread");
  info("downstream had a total of %u underruns", downstream.underruns);
}
