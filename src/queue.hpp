#ifndef QUEUE_HPP
#define QUEUE_HPP
#include <deque>
#include <condition_variable>
#include <mutex>
#include <memory>
using namespace std;
template<typename T>
class Queue {
    deque<unique_ptr<T>> queue;
    mutex mx;
    condition_variable condition;
  public:
    unsigned int underruns = 0;
    bool waiting = false;
    void push(unique_ptr<T> && item){
      {
        // we put this in a block s our lock will
        // run out of scope *before* we notify.
        unique_lock<mutex> lock(mx);
        queue.push_back(move(item));
      }
      // no reason to notify_all. Even if there were more than
      // one consumers, only one of them can have the item.
      condition.notify_one();
    }
    unique_ptr<T> pop(){
      unique_lock<mutex> lock(mx);
      condition.wait(lock,[this]{
        bool dataAvailable =!queue.empty();
        if(!waiting && !dataAvailable){
          waiting = true;
          underruns ++;
        }
        return dataAvailable;
      });
      waiting = false;
      auto item {move(queue.front())};
      queue.pop_front();
      return item;
    }
};
#endif // QUEUE_HPP
