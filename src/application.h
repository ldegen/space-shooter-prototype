#ifndef APPLICATION_H
#define APPLICATION_H
#include<memory>
#include<functional>

class Shell;

class Application {
  public:
  // Subclasses should implement their game loop here.
  // When the program will exit after returning from this method.
  // Implementations are responsible for periodically calling pollEvents() and swapBuffers() on 
  // the given windowing system instance.
  virtual int run(Shell &shell)=0;

  // handlers for events from the windowing system
  // All handlers are guaranteed to be called from the 
  // same thread as the run method.
  virtual void buttonStateChanged(Shell &shell, unsigned int state){}
  virtual void mouseButtonPressed(Shell &shell, double x, double y){}
  virtual void mouseButtonReleased(Shell &shell, double x, double y){}
  virtual void mouseScrolled(Shell &shell, double xOffset, double yOffset){}
  virtual void mouseMoved(Shell &shell, double x, double y){}
  virtual void windowResized(Shell &shell, int width, int height){}
  virtual void windowCloseRequested(Shell &shell){}
  virtual void onExit(Shell &shell){}
};

using ApplicationFactory = std::function<std::unique_ptr<Application>()>;
/* A Proxy for Applications.
 *
 * Why do we need one?
 *
 * Simple. We want to allow applications to assume that a GL Context is 
 * active/current during the complete life span of the application. 
 *
 * On the other hand, we want to allow the Shell implementations
 * to assume that all clean up by the application is done once the run method
 * returns.
 *
 * Now... since the application cannot be deconstructed while the run method
 * still executes, this would force the application to manually manage the life
 * cycle of its GL-requiring subcomponents, which would probably involve
 * a lot of new/delete awkwardness, and clearly contradict our first
 * statement. 
 *
 * So we have this proxy. It is initialized with a factory method, which 
 * will defer the creation of the *actual* application until the run method
 * is called. It will also guarantee, that the *actual* application is 
 * destroyed before the run method on the proxy returns.
 */
class ProxyApplication : public Application {
  ApplicationFactory createApplication;
  std::weak_ptr<Application> app;

  public:
  ProxyApplication(ApplicationFactory factory)
    :createApplication(factory)
  {}
  virtual ~ProxyApplication(){}
  virtual int run(Shell &shell){
    std::shared_ptr<Application> sptr = createApplication();
    // since createApplication returns a unique_ptr, we are now the only one
    // holding a copy of that pointer. The shared reference we just created
    // shall be the only owner and dispose of it once this method returns.
    //
    // But since we need to refer to the pointer in our delegate methods,
    // we store another non-owning copy in the member variable app.
    app = sptr;
    auto rval = sptr->run(shell);
    sptr->onExit(shell);
    return rval;
  }
  virtual void buttonStateChanged(Shell &shell, unsigned int state){
    if (auto sptr = app.lock()) { 
	sptr->buttonStateChanged(shell, state);
    } 
  }
  virtual void mouseButtonPressed(Shell &shell, double x, double y){
    if (auto sptr = app.lock()) { 
	sptr->mouseButtonPressed(shell, x, y);
    } 
  }
  virtual void mouseButtonReleased(Shell &shell, double x, double y){
    if (auto sptr = app.lock()) { 
	sptr->mouseButtonReleased(shell, x, y);
    } 
  }
  virtual void mouseMoved(Shell &shell, double x, double y){
    if (auto sptr = app.lock()) { 
	sptr->mouseMoved(shell, x, y);
    } 
  }
  virtual void mouseScrolled(Shell &shell, double xOffset, double yOffset){
    if (auto sptr = app.lock()) { 
	sptr->mouseScrolled(shell, xOffset, yOffset);
    } 
  }
  virtual void windowResized(Shell &shell, int width, int height){
    if (auto sptr = app.lock()) { 
	sptr->windowResized(shell, width, height);
    } 
  }
  virtual void windowCloseRequested(Shell &shell){
    if (auto sptr = app.lock()) { 
	sptr->windowCloseRequested(shell);
    } 
  }
  virtual void onExit(Shell &shell){
    // ignored, we do it ourself before returning from run()
  }
};

#endif
