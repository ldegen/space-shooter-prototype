#include "body-type.h"

double maxr(const std::vector<Shape> & shapes){
  double max = 0;
  for(auto shape : shapes){
    for(auto v: shape.vertices()){
      double r = v.x*v.x + v.y*v.y; 
      if (r > max){
        max = r; 
      } 
    }
  }
  return 5.0+std::sqrt(max);
}

BodyType::BodyType()
  :BodyType(0,{})
{

}

BodyType::BodyType(uint32_t tid, const std::vector<Shape> & shapes)
  : _shapes {shapes}
  , _tid {tid}
  , _r{maxr(shapes)}
{

}

double BodyType::r() const
{
  return _r;
}

const std::vector<Shape> &BodyType::shapes() const
{
  return _shapes;
}

uint32_t BodyType::tid() const
{
  return _tid;
};


double BodyType::momentOfInertia() const{
  if (J==0.0){
    calc(); 
  }
  return J;
}
double BodyType::area() const{
  if (A==0.0){
    calc(); 
  }
  return A;
}
double BodyType::mass() const{
  if (M==0.0){
    calc(); 
  }
  return M;
}
glm::dvec2 BodyType::centerOfGravity() const{
  if (M==0.0){
    calc(); 
  }
  return C;
}

void BodyType::calc() const{
  //To simulate a body, we need to know its mass and moment of inertia

  //The mass is just the sum of the masses of the individual shapes.
  //But for calculating the total moment of inertia,
  //we first need to find the combined center of gravity of all the given shapes.
  //Then, we use Steiner's Theorem to determin the individual moments with respect to this common axis
  //and sum them up.


  for (auto & shape : _shapes) {
    double m = shape.mass();
    glm::dvec2 c = shape.centroid();
    M += m;
    C += m*c;
    A += shape.area();
  }

  C /= M;

  // next we sum up the individual moments with respect to the combined centroid.
  for (auto & shape : _shapes) {
    double JJ = shape.momentAt(C);
    //std::cout << "JJ: "<<JJ<<std::endl;
    J += JJ;
  }
}

