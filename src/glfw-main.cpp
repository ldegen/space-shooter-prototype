#include "config.hpp"
#include "glfw-shell.h"
#include "game.h"
#include "application.h"

#include <memory>


// settings
int main(int argc, char ** argv) {

  Config config = Config::parseCommandLine(argc,argv);
  if(config.help()){
    return -1; 
  }

  ProxyApplication app([&config](){return std::make_unique<Game>(config);});
  return GlfwShell::run(config, app);
}
