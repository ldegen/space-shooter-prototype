#include "misc-utils.h"
#include "texture-repository.h"
#include <glad/glad.h>
#include <stb_image.h>
#include <iostream>
using namespace std;
unsigned int TextureFromFile(const char *path, const string &directory, bool gamma=false)
{
  string filename = string(path);
  filename = directory + '/' + filename;

  unsigned int textureID;
  glGenTextures(1, &textureID);

  int width, height, nrComponents;
  unsigned char *data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
  if (data)
  {
    GLenum format;
    if (nrComponents == 1)
      //format = GL_RED;
      format = GL_LUMINANCE;
    else if (nrComponents == 3)
      format = GL_RGB;
    else if (nrComponents == 4)
      format = GL_RGBA;

    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    stbi_image_free(data);
  }
  else
  {
    error("Texture failed to load at path: %s", path);
    stbi_image_free(data);
  }

  return textureID;
}

TextureRepository::TextureRepository(string directory)
: directory {directory}
{}

Texture TextureRepository::getTexture(const string & typeName, const string & path){
  auto it = cache.find(path);
  if(it != cache.end()){
    return it->second;
  }
  Texture texture;
  texture.id = TextureFromFile(path.c_str(), this->directory);
  texture.type = typeName;
  texture.path = path.c_str();
  cache[path]=texture;
  return texture;
}
