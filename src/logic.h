#ifndef LOGIC_H
#define LOGIC_H

#include "body-type.h"
#include "shape.h"
#include "bbox.hpp"
#include "shatter.h"
#include "protocol.h"
#include "cameraman.h"
#include "config.hpp"
#include "sequencer.h"
#include <vector> 
#include <unordered_map>
#include <lo/lo.h>
#include <lo/lo_cpp.h>

using namespace std;
class Logic : public FilteringProtocolListener{
  struct object_record {
    unsigned long uid;
    unsigned int tid;
    double timeCreated;
    double maxAge = 0;
    unsigned int hints=0;
    unsigned int tags=0;
    double area=0.0;
    bool fire=false;
    double t_lastShot=0.0;
    double desiredAngularVelocity=0.0;
    double health = 100.0;
    unsigned int buttonShift=0; // left-shift button bits by this amount.
    bool destroyed = false;
  };

  Bbox<double> bbox;
  ProtocolWriter & writer;
  CameraMan cameraMan;
  const unsigned int playerType = 0;
  const unsigned int bulletType = 1;
  const unsigned int planetType = 2;
  const unsigned int reservedTypes = 3;
  vector<unsigned int> buttonStates;
  unsigned int prevButtonState = 0;
  unsigned long nextUid = 1; //0 is reserved for "no uid"
  unordered_map<unsigned long, object_record> objects;
  vector<BodyType> bodyTypes;
  list<unsigned int> freeTids;
  vector<const Blip*> pois;
  Sequencer sequencer;

  double totalArea = 0.0;
  double t = 0.0;
  Shatter shatter;
  Config & config;
  Plotter plotter;

  bool _keepRunning = true;
  bool total = false;
  lo::Address oscAddress;
  unsigned int nextTid();
  public:

    Logic(Config & config, ProtocolWriter & writer);

    void onExit();
    void onMessage(const msg_init * msg);
    void onMessage(const msg_error * msg);
    void onMessage(const msg_body_created * msg);
    void onMessage(const msg_body_deleted * msg);
    void onMessage(const msg_button_state_changed * msg);
    void onMessage(const msg_close_requested * msg);
    void onCollision(const msg_collision & collision, const data_contact * contacts);
    bool examineTags(const msg_blips & header, unsigned int tags);
    void onBlip(const msg_blips & header, const Blip & bli);
    void beforeBlips(const msg_blips & header, const Blip * blips, const data_tag * tags);
    void afterBlips(const msg_blips & header, const Blip * blips, const data_tag * tags);
      

    void cameraTracking();
    bool keepRunning();

  private:
    void blowUp(const Blip & targetBlip, const glm::dvec2 & epicenter={0.0,0.0});
    bool handleBulletCollision(const msg_collision & collision, const data_contact * contacts);
    bool handleGenericCollision(const msg_collision & collision, const data_contact * contacts);
    void handleImpact(const Blip & blip, const glm::dvec2 & epicener, double damage);
    BodyType * lookupBodyType(const Blip & blip);
    BodyType * lookupBodyType(const Blip * blip);
    BodyType * lookupBodyType(const unsigned int tid);
    void createShipShape();
    void createBulletShape();
    void createPlanetShape();
    void createAsteroids();
    void createPlanet();
    void respawnPlayer(unsigned int hints);
    void createPlayer(unsigned int shift, double x, double y, double heading, unsigned int hints=0);
    void processControls(const Blip * playerBlip);
    void centerShip(const Blip * playerBlip);
    void spawnBullet(const Blip * playerBlip);
    void cleanupDebris();
    bool isDebris(const object_record & object);
    // true if at one point since the last frame
    // the button state changed from 0 to 1.
    // Note: if the button was pressed then released again
    // this method will return true, though isButtonDown() will
    // would return false
    bool wasButtonPressed(unsigned int mask);
    // same but for changes from 1 to 0.
    bool wasButtonReleased(unsigned int mask);
    bool isButtonDown(unsigned int mask);

};
#endif
