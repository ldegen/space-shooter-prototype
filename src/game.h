#ifndef GAME_H
#define GAME_H
#include "application.h"
#include "config.hpp"
#include "gl-stuff.h"
#include "logic.h"
#include "physics.h"
#include "protocol.h"
#include "queue.hpp"


class Game : public Application {
  Config & config;

  ProtocolWriter writer;

  Logic logic;
  Space physics;


  // glStuff puts buffers here after it has read their contents
  // backend reuses them, overwriting their content;
  Queue<buffer_t> upstream;

  // backend pushes buffers here after writing
  // glStuff pops buffers from here to read their content
  Queue<buffer_t> downstream;

  public:

  Game(Config & config);
  virtual ~Game(){}
  virtual int run(Shell &shell) override;


  virtual void buttonStateChanged(Shell &shell, unsigned int state) override;
  virtual void mouseButtonPressed(Shell &shell, double x, double y) override;
  virtual void mouseButtonReleased(Shell &shell, double x, double y) override;
  virtual void mouseMoved(Shell &shell, double x, double y) override;
  virtual void mouseScrolled(Shell &shell, double xOffset, double yOffset) override;
  virtual void windowResized(Shell &shell, int width, int height) override;
  virtual void windowCloseRequested(Shell &shell) override;
  virtual void onExit(Shell &shell) override;


  private:


  void runBackend(Shell *shell);
  void runGlStuff(Shell *shell);
};

#endif
