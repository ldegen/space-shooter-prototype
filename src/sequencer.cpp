#include "sequencer.h"
using namespace std;
Sequencer::Sequencer()
{


}

void Sequencer::schedule(double t, function<void ()> f)
{
  queue.insert(make_pair(t,f));
}

void Sequencer::executePending(double t)
{
  auto end = queue.upper_bound(t);
  for(auto it = queue.begin(); it != end; it++){
    it->second();

  }
  queue.erase(queue.begin(), end);
}
