#include "trackball.h"
using glm::dvec3;
using glm::dquat;
using glm::cross;
using glm::length;
using glm::rotate;
using glm::asin;
using glm::sqrt;
using glm::normalize;

void Trackball::motion(const int x, const int y){
  if(this->tracking){
    const dvec3 currentPos = this->project(x,y);

    const dvec3 diff = currentPos - this->lastPos;

    if (diff[0] || diff[1] || diff[2]){
      const dvec3 axis = cross(this->lastPos, currentPos);

      //remember that |a x b| = |a| |b| sin theta
      //Note that neither a nor b can be 0 since they were projected on a unit sphere
      //around the origin.
      const double theta =  asin(length(axis) / (length(this->lastPos) * length(currentPos)));
      const dquat qdiff = angleAxis(theta, normalize(axis));
      this->q = qdiff * q;

      //copy position
      this->lastPos = currentPos;
    }
  }
};

void Trackball::viewport(const int width, const int height){
  this->width=width;
  this->height=height;
};

void Trackball::begin(const int x, const int y){
  this->tracking = 1;
  this->lastPos = this->project(x,y);
};

void Trackball::end(){
  this->tracking = 0;
};

dquat Trackball::orientation(){
  return this->q;
};

/** project mouse coordinates on unit sphere */
dvec3 Trackball::project(const int screenX, const int screenY){
  //first, normalize the coordinates so they are in [-1,+1]
  //keep in mind that we need to invert the y axis
  const double x = (2.0 * screenX - this->width) / this->width;
  const double y = (this->height - 2.0 * screenY) / this->height;

  // caluculate the square length d of (x,y)
  // If it is > 1, the point (i.e. if the mouse is outside the unit circle)
  // set z to zero and normalize
  const double d2 = x*x + y*y;

  // 
  if(d2>1.0){
    const double d = sqrt(d2);
    return dvec3(x/d,y/d,0);
  }

  //z is calculated via pythagoras: 
  //Since we have a unit sphere, the hypothenus is 1. One of the legs is
  //the value z we are looking for. The other leg is the length of (x,y).
  const double z = sqrt(1 - d2);
  return dvec3(x,y,z);
};
