#ifndef PHYSICS_H
#define PHYSICS_H
#include "bbox.hpp"
#include "shape.h"
#include "body-type.h"
#include "protocol.h"
#include "config.hpp"
#include <vector>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include<chipmunk.h>

struct pairhash {
public:
  template <typename T, typename U>
  std::size_t operator()(const std::pair<T, U> &x) const
  {
    return std::hash<T>()(x.first) ^ std::hash<U>()(x.second);
  }
};


class Space : public ProtocolListener{
private:
#if CP_VERSION_MAJOR >= 7
  cpShapeFilter normalFilter  {CP_SHAPE_FILTER_ALL};
  cpShapeFilter ghostFilter {1,CP_ALL_CATEGORIES, CP_ALL_CATEGORIES};
#endif
  cpVect gravity;
  cpSpace * space;
  std::vector<BodyType> bodyTypes;
  std::unordered_set<std::pair<unsigned long, unsigned long>, pairhash> collisionPairs;
  double t = 0.0;
  double south = -21.6;
  double north = 21.6;
  double west = -38.4;
  double east = 38.4;

  struct BodyRecord {
    unsigned long uid;
    unsigned int type;
    double timeCreated;
    double x;
    double y;
    double theta;
    double dx;
    double dy;
    double dtheta;
    double r;
    unsigned int tags;
    unsigned int hints;
    cpBody * occurences[4];
  };

  struct Attractor {
    cpBody * body;
    double mass;
  };

  std::map<unsigned long, BodyRecord> records;
  unsigned long selection;

  ProtocolWriter & writer;

public:
  bool pause = false;
  const double deltaT = 1.0/60.0;
  const double particleThreshold = 0.5;
  //Space(ProtocolWriter & writer, const Bbox<double> & bbox={-38.4,-21.6,38.4,21.6}, double timestep=1.0/60.0);
  Space(Config & config, ProtocolWriter & writer);
  ~Space();

  unsigned long createBody(unsigned long uid, unsigned int type, const State & initialState = {});
  unsigned long createStatic(unsigned long uid, unsigned int type);
  void destroyBody(unsigned long uid);
  void selectUid(unsigned long uid);
  void setFxFyT(double fx, double fy, double t, unsigned long uid);
  void setDxDyDt(double fx, double fy, double t, unsigned long uid);
  void setDt(double t, unsigned long uid);
  void setXYT(double x, double y, double t, unsigned long uid, unsigned long rel=0);
  void applyImpulse(double jx, double jy, double rx, double ry, unsigned long uid);
  void tag(unsigned long uid, unsigned int unset, unsigned int set);
  void hint(unsigned long uid, unsigned int unset, unsigned int set);

  double step();
  cpBool begin(cpArbiter * arb);
  void separate(cpArbiter *arb);
  void postSolve(cpArbiter * arb);
  cpBool preSolve(cpArbiter * arb);
  int blips();
  int blips(double alpha);
  int blips(ProtocolListener & listener, double alpha);
  Bbox<double> bbox(){ return {west, south, east, north};};

  virtual void onMessage(const cmd_create *cmd);
  virtual void onMessage(const cmd_delete *cmd);
  virtual void onMessage(const cmd_select_uid *cmd);
  virtual void onMessage(const cmd_set_x_y_theta *cmd);
  virtual void onMessage(const cmd_set_dx_dy_dtheta *cmd);
  virtual void onMessage(const cmd_set_dtheta *cmd);
  virtual void onMessage(const cmd_set_fx_fy_torque *cmd);
  virtual void onMessage(const cmd_apply_impulse *cmd);
  virtual void onMessage(const cmd_tag *cmd);
  virtual void onMessage(const cmd_hint *cmd);
  virtual void onMessage(const cmd_pause *cmd);
  virtual void onMessage(const cmd_resume *cmd);
  virtual void onMessage(const cmd_toggle_pause *cmd);
  virtual void onBodyType(const BodyType & bt);


private:
  cpBody * planetBody;
  cpShape * planetShape;
  cpVect moduloBbox(const cpVect & p);
  cpVect moduloBbox(double x0, double y0);
  void printDebug(const BodyRecord & r);
  cpBody * createStaticInternal(const std::vector<Shape> shapes, double M, double J, double x, double y, double theta);
  cpBody * createBodyInternal(const std::vector<Shape> shapes, double M, double J, double r, const State & initialState);
  void destroyBody(BodyRecord & r);
  void afterEachStep();
  void beforeEachStep();
  unsigned int inLimbo(double x, double y, double r);
  unsigned int touchingLimbo(double x, double y, double r);
  unsigned int gone(double x, double y, double r);
  void createGhost(const cpBody * orig, double tx, double ty, BodyRecord & rec);
  void deleteGhost(const cpBody * ghost, BodyRecord & rec);
  State bodyState(const cpBody * body);
  void synchronize(BodyRecord & rec);
  void manageGhosts(BodyRecord & rec);
  void applyGravity(BodyRecord & rec, std::vector<Attractor> & attractors);
  void beginCollisionMessage(cpArbiter * arb); 
  void submitCollisionMessage(cpArbiter * arb); 
  cpBody * realOccurence(BodyRecord & rec, State * state=NULL);
};
#endif
