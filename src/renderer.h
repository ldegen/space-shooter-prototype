#ifndef RENDERER_H
#define RENDERER_H


#include <glm/glm.hpp>
#include <vector>
#include <unordered_map>

using glm::vec3;
using glm::vec2;
using namespace std;
class Renderer {
  public:
    using tid_t = uint32_t;
  private:
    unordered_map<tid_t, unsigned int> refCounts;
  public:
    virtual void ref(tid_t tid){
      auto it = refCounts.find(tid);
      if(it == refCounts.end()){
        refCounts.insert(make_pair(tid,1));
      } else {
        it->second++;
      }
    }

    virtual void unref(tid_t tid){
      auto it = refCounts.find(tid);
      if(it == refCounts.end()){
        return;
      }
      it->second--;
      if(it->second == 0){
        removeGeometry(tid);
      }
    }

    virtual void addGeometry(tid_t tid, const vector<glm::dvec2> & coordinates) {
      vector<glm::vec2> r;
      r.reserve(coordinates.size());
      for(auto v: coordinates){
        r.push_back(v);
      }
      addGeometry(tid, move(r));
    }
    virtual void addGeometry(tid_t tid, const vector<glm::vec2> & coordinates) = 0;
    virtual void removeGeometry(tid_t tid) = 0;
    virtual void draw(tid_t tid, const vec3 & transform, uint8_t layer, uint8_t color){
      draw(tid, transform);
    }
    virtual void draw(tid_t tid, const vec3 & transform){
      draw(tid, transform, 0, 0);
    }
    virtual void reset(){
      reset(0);
    }
    virtual void reset(unsigned int upperBound){
      reset();
    }
    virtual void submit() = 0;
    virtual ~Renderer(){}
};
#endif // RENDERER_H
