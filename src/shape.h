#ifndef SHAPE_H
#define SHAPE_H

#include <glm/glm.hpp>
#include <vector>

class Shape {
  //vertices should be in ccw order
  std::vector<glm::dvec2> m_vertices;

  //we assume homogenous density distribution
  double m_density = 1.0;

  double m_friction = 1.0;

  double m_elasticity = 0.5;
  
  unsigned int m_hints = 0;

  mutable bool momentsKnown = false;

  mutable double A = 0.0;

  mutable double Cx = 0.0;

  mutable double Cy = 0.0;

  mutable double J = 0.0;
  
  public:

  Shape(const std::vector<glm::dvec2> & vertices, 
      double density = 1.0, 
      double friction = 1.0, 
      double elasticity=0.5, 
      unsigned int hints=0);
  glm::dvec2 centroid() const;

  // returns the shame shape translated such that its centeroid is at the origin.
  Shape normalized() const;

  double area() const;

  double mass() const;

  double density() const {return m_density;};
  double elasticity() const {return m_elasticity;};

  double friction() const {return m_friction;};

  unsigned int hints() const {return m_hints;};
  void hints(unsigned int f) {m_hints=f;};

  // returns the momentum of inertia with respect to the centeroid of the shape.
  double momentCentroid() const;

  // returns the momentum of inertia with respect to the origin.
  double momentOrigin() const;

  // returns the momentum of inertia with respect to some other point in the local coordinate system
  double momentAt(glm::dvec2 p) const;

  const std::vector<glm::dvec2> & vertices() const {return m_vertices;};
  
  uint32_t numVertices() const {return m_vertices.size();}

  private:

  void calculate() const;
  

};

#endif
