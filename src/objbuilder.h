#ifndef OBJBUILDER_H
#define OBJBUILDER_H

#include<string>
#include<vector>

#include<glm/glm.hpp>
#include<iostream>

using namespace std;
using glm::dvec2;
template<typename S, typename D>
glm::tvec2<D> asVector(const S & x){
  return glm::tvec2<D>{x.x,x.y};
}
class ObjBuilder
{
  public:
    class Node{
      vector<dvec2> & verts;
    public:
      vector<vector<size_t>> faces;
      string name;

      Node(const string & name, vector<dvec2> & verts);
      template<typename T>
      Node & loop(const vector<T> & coords)
      {
        vector<size_t> face;
        face.reserve(coords.size());
        for(auto v : coords){
          verts.push_back(asVector<T,double>(v));
          // NOTE: we add the vertex index + 1.
          // This is done on purpose! indices in wavefront obj files start at 1, not 0
          face.push_back(verts.size());
        }
        faces.push_back(move(face));
        return *this;
      }
      template<typename T>
      Node & vertices(const vector<T> & coords)
      {
        // "vertices" are just degenerated faces with a single vertex index.
        // Not sure if this is conformant with the standard, but it works with blender,
        // which is my only concern right now.
        for (auto v: coords){
          loop(vector<T>{v});
        }
        return *this;
      }
    };
  private:
    vector<Node> nodes;
    vector<dvec2> vertices;
  public:
    ObjBuilder();
    Node & group(const string & name);
    void reset();
    void write(ostream & out);
};

#endif // OBJBUILDER_H
