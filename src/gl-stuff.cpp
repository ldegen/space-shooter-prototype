#include "gl-stuff.h"
#include "gamerenderer.h"
#include "misc-utils.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>
#include <glad/glad.h>
#include <memory>
#include <iostream>
#include <bitset>

bool isRealDeal(const Blip & blip){
  return !(blip.state.flags >> 4);
}

glm::dmat4 makeTfd(double x, double y, double theta){
  glm::dmat4 modelTf = glm::dmat4(1.0);
  modelTf = glm::translate(modelTf, glm::dvec3(x, y,0.0));
  modelTf = glm::rotate(modelTf, theta, glm::dvec3(0.0f,0.0f,1.0f));
  return modelTf;
}
glm::mat4 makeTf(double x, double y, double theta){
  glm::mat4 modelTf = glm::mat4(1.0f);
  modelTf = glm::translate(modelTf, glm::vec3(x, y,0.0f));
  modelTf = glm::rotate(modelTf, (float) theta, glm::vec3(0.0f,0.0f,1.0f));
  return modelTf;
}



void GlStuff::calculateProjection()
{
  if(!projectionDirty){
    return;
  }

  // reset dirty flag to avoid accidental
  // recursion by stupid future me
  projectionDirty = false;
  // calculate zoom
  m_zoom = std::min(
               viewportWidth/max(m_motiv_min_width,m_motiv.width()),
               viewportHeight/max(m_motiv_min_height,m_motiv.height())
               );

  // calculate extent
  double w = static_cast<double>(viewportWidth) / m_zoom;
  double h = static_cast<double>(viewportHeight) / m_zoom;
  glm::dvec2 lookat = m_motiv.center();
  double left =lookat.x - 0.5 * w;
  double bottom = lookat.y - 0.5 * h;
  double right = left + w;
  double top = bottom + h;
  m_extent = {left, bottom, right, top};

  /*
   use this for fixed camera
  double w = bbox.width();
  double h = bbox.height();
  m_extent = bbox;
  */
  // calculate projection
  float near = 0.0f;
  float far = 1000.0f;
  m_projection = glm::ortho(
                   static_cast<float>(-0.5*w),
                   static_cast<float>(0.5*w),
                   static_cast<float>(-0.5*h),
                   static_cast<float>(0.5*h),
                   near,
                   far);
}

GlStuff::GlStuff(Config & config)
  : renderer {make_unique<GameRenderer>()}
  , bbox {config.bboxLeft(), config.bboxBottom(), config.bboxRight(), config.bboxTop()}
  , m_motiv {bbox}
  , oscAddress {config.oscHost(), config.oscPort()}
{
  //glEnable(GL_BLEND);
  glEnable(GL_DEPTH_TEST);
  glCheckError();
#ifdef USE_STENCIL_BUFFER
  glEnable(GL_STENCIL_TEST);
#endif
  //glEnable(GL_MULTISAMPLE);
  //glBlendEquation( GL_FUNC_ADD );
  //glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

}

GlStuff::~GlStuff(){
}
void GlStuff::render(const Blip * blips, unsigned int numBlips){

  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
#ifdef USE_STENCIL_BUFFER
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
#else
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
#endif

  // the game wants us to show a rectangular extent of the game world.
  // The aspect ratio of said extents is the same as that of our window.
  Bbox<double> ex = extent();

  // the problem is: the requested extent may not be completely included within
  // our bounding box. Remember: the bbox is the part of the world for which we
  // are guaranteed to receive all visible geometries (blips) from the 
  // game physics. Geometries overlapping the boundaries of the bbox will be
  // "wrapped arround". But how do we draw areas that are outside the bbox?
  //
  // The effect that we want to achieve is that of a torodial world. Think of the 
  // bbox as of a bathroom tile that keeps repeating over and over in both dimensions
  // into infinity. So no matter where we are pointing our "camera", we will always see
  // a piece of the same tile. We only have to deterime what piece.

  // The split-function is used to cut our extent into one piece that lies within
  // the bbox and up to three pieces that lie outside (in adjacent grid cells).
  // We call those pieces "facets" (for want of a better name).
  std::vector<Bbox<double>> facets = split(ex, bbox);

  /*
  // Each of the facets is now rendered separately. 
  unsigned int i = 1;
  for(auto facet : facets){
    render(blips, numBlips, facet, i++);
  }
  */
  if(numBlips>0){
    renderer->reset(facets.size()*numBlips);
    //vector<mat4> viewsTfs;
    //viewsTfs.reserve(facets.size());
    for (uint8_t layer=0;layer<facets.size();layer++){
      auto & facet = facets[layer];
      auto equivFacet { facet % bbox };
      //viewsTfs.push_back(view(ex,facet));
      vec2 tl = facetTranslation(ex,facet);
      for(unsigned int j=0;j<numBlips;j++){
        const Blip & blip = blips[j];
        if(equivFacet.maybeIntersecting(blip.state.x,blip.state.y,blip.r)){
          uint8_t color=1;
          if(blip.hints & HINT_PLAYER_A){
            color = 0;
          }
          if(blip.hints & HINT_PLAYER_B){
            color = 2;
          }
          renderer->draw(blip.tid, {blip.state.x+tl.x, blip.state.y+tl.y, blip.state.theta}, layer, color);
        }
      }
    }
    uniBatchShader.use();
    //uniBatchShader.setMat4("view", viewsTfs.size(), &viewsTfs[0]);
    uniBatchShader.setMat4("projection", projection());
    renderer->submit();
  }
}
  
mat4 GlStuff::projection()
{
  calculateProjection();
  return m_projection;
}

vec2 GlStuff::facetTranslation(const Bbox<double> & extentBox, const Bbox<double> & facet)
{
  Bbox<double> equivFacet{ facet % bbox};
  // we determine the offset between the requested facet and its counterpart within the world bbox
  // and create a new extent equivalent to the requested one, but with the requested facet
  // being contained in the actual bbox.
  glm::dvec2 shift {equivFacet.bottomLeft() - facet.bottomLeft()};
  Bbox<double> equivExtentBox {extentBox.translate(shift)};

  // the view transformation we are looking for is the one that translates
  // the center of the equivalent extent into the origin.
  return -equivExtentBox.center();
}

glm::mat4 GlStuff::view(const Bbox<double> &extentBox, const Bbox<double> &facet)
{
  // equivFacet is the part of the world that corresponds to the facet we should draw.
  vec2 t {facetTranslation(extentBox, facet)};

  return glm::translate(glm::mat4(1.0f),glm::vec3(t.x, t.y, 0.0f));
}



void GlStuff::resize(const int windowWidth, const int windowHeight){
  viewportWidth = windowWidth;
  viewportHeight = windowHeight;
  glViewport(0,0,viewportWidth, viewportHeight);
  projectionDirty=true;
}
void GlStuff::mouseMoved(const int x, const int y){
  //trackball.motion(x,y);
  if(dragging){
    glm::dvec2 mouse {x, -y};
    glm::dvec2 delta = (mouse-origMouse) / zoom();
    panTo(origPan - delta);
  }
}

void GlStuff::mouseButtonDown(const int x, const int y){
  //trackball.begin(x,y);
  origMouse = glm::dvec2{x,-y};
  origPan = lookAt();
  dragging=true;
}

void GlStuff::mouseButtonUp(const int x, const int y){
  //trackball.end();
  dragging=false;
}

void GlStuff::scrolled(const int xoffset, const int yoffset){
  m_motiv.grow(yoffset);
  projectionDirty = true;
}

void GlStuff::onBodyType(const BodyType & bt){
  renderer->addGeometry(bt.tid(), bt.shapes()[0].vertices());
}
bool GlStuff::examineTags(const msg_blips & header, unsigned int tags){
  return tags & TAG_TRACK;
}
void GlStuff::beforeBlips(const msg_blips & header, const Blip * blips, const data_tag * tags){
}
void GlStuff::onBlip(const msg_blips & header, const Blip & blip){
}
void GlStuff::afterBlips(const msg_blips & msg, const Blip * blips, const data_tag * tags){
  t = msg.t;
  render(blips, msg.num_blips);
}

void GlStuff::onMessage(const cmd_look_at *cmd){
  m_motiv.left=cmd->left;
  m_motiv.right=cmd->right;
  m_motiv.bottom=cmd->bottom;
  m_motiv.top=cmd->top;
  projectionDirty=true;

}

void GlStuff::onMessage(const cmd_play_sound *cmd)
{
  // glm::dvec3 c {cmd->x,cmd->y, 0.0};
  // double pan,dB;
  // tie(pan,dB) = panAndVolume(c);
  // oscAddress.send("/play","fffff",cmd->preset, cmd->velocity, cmd->pitch, pan, dB);
}

void GlStuff::onMessage(const cmd_quit *cmd)
{
  _keepRunning = false;
}
void GlStuff::onMessage(const msg_body_created *cmd){
}
void GlStuff::onMessage(const msg_body_deleted *cmd){
}

void GlStuff::onMessage(const msg_window_resized *cmd)
{
  debug("glstuff-resize %u, %u", cmd->width, cmd->height);
  resize(cmd->width, cmd->height);
}

bool GlStuff::keepRunning()
{
 return _keepRunning;
}

Bbox<double> GlStuff::extent(){
  calculateProjection();
  return m_extent;
}

glm::dvec2 GlStuff::lookAt(){
  return m_motiv.center();
}

void GlStuff::panTo(const glm::dvec2 & t){
  m_motiv = m_motiv.translate( (t%bbox) - lookAt());
  projectionDirty = true;
}

void GlStuff::panBy(const glm::dvec2 & delta){
  m_motiv = m_motiv.translate(delta);
  projectionDirty = true;
}

double GlStuff::zoom(){
  calculateProjection();
  return m_zoom;
}



    
double sqlen(glm::dvec3 v){
  return glm::dot(v,v);
}

pair<double,double> GlStuff::panAndVolume(const glm::dvec3 & c)
{
  Bbox<double> extentBox =extent();
  glm::dvec3 lookAt {extentBox.center(), 0.0};
  glm::dvec3 cRel = c - lookAt;

  // make sure cRel is the shortest possible distance between c and lookAt

  if (cRel.y > bbox.height()/2.0){
    cRel.y -= bbox.height();
  }
  if (cRel.y < -bbox.height()/2.0){
    cRel.y += bbox.height();
  }
  if (cRel.x > bbox.width()/2.0){
    cRel.x -= bbox.width();
  }
  if (cRel.x < -bbox.width()/2.0){
    cRel.x += bbox.width();
  }
  //std::cout<<"c.x: "<<c.x<< ", cRel.x: "<<cRel.x<<std::endl;

  // next determine the position of the virtual camera.
  // The camera is assumed to be at some point on a perpendicular line that
  // intersects the XY-Plane at the lookAt point. Let's call this line the
  // view axis. (Don't know if there is a word for this).
  //
  // We deterimine its distance (i.e. Z-coordinate) by assuming a given Field-of-View (FOV)
  // angle. (Actually, since our screen is not circular, but a rectangle, there are two
  // angles. But since by definition our screen's aspect ratio is the same as that of the
  // extent, it's enough to just pick either horizontal or vertical FOV.)
  //
  // Let's pick 50°. This roughly corresponds to my current screen width and viewing position.
  double fov = 0.8726646;

  // The camera position and axis together with the FOV determine the camera frustrum.
  // The intersection of this frustrum with the XY-Plane is -- again, by definition --
  // exactly our extent box. From this, we can deduce the position of the camera.
  // If we draw a line parallel to the X-Axis through our lookAt-point, it will intersect
  // the left and right side of the extent. Let's pick one of those intersections.
  // It creates a rectangular triangle together with the camera position and the lookAt point.
  // The angle at the camerPosition has to be theta = FOV/2 and the length of the oposite side
  // is half the width of our extent, i.e. w = extent/2. So the distance between camera
  // and lookAt has to be d = w / tan(theta)

  double w = extentBox.width()/2.0;
  double theta = 0.5 * fov;
  double cameraDistance = w / std::tan(theta);

  // as the view axis is perpendicular to the XY-Plane, we can now define the
  // camera position:
  glm::dvec3 cameraPos = {extentBox.center(), cameraDistance};

  // calculating the distance between camera and contact point is trivial now:
  double distance = glm::length(cameraPos - c);

  // the X and Y components give us azimuth and elevation respectively:
  // tan azimuth = cRel.x / cameraDistance

  double azimuth = std::atan(cRel.x / cameraDistance);
  //double elevation = std::atan(cRel.y / cameraDistance);
  double dB = 50 - std::log2(1+distance)*6.0;
  double pan = 0.5 + azimuth/M_PI;
  return make_pair(pan,dB);
}
