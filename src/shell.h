#ifndef SHELL_H
#define SHELL_H
struct WindowSize{
  int width = 0;
  int height = 0;
};

struct MouseState{
  double x = 0;
  double y = 0;
  unsigned int buttons = 0;
};
class Shell {

  public:
    virtual void pollEvents() = 0;
    virtual void swapBuffers() = 0;
    virtual WindowSize windowSize() = 0;
    virtual unsigned int buttonState() = 0;
    virtual MouseState mouseState() = 0;
    virtual double time() = 0;
    virtual bool windowShouldClose() = 0;
    virtual void requestWindowClose() = 0;
    virtual void makeContextCurrent() = 0;
    virtual void makeContextNonCurrent() = 0;
};
#endif
