#include "bbox.hpp"
#include "objbuilder.h"

ObjBuilder::ObjBuilder()
{

}

ObjBuilder::Node &ObjBuilder::group(const string &name)
{
  nodes.push_back({name, vertices});
  return nodes.back();
}

void ObjBuilder::reset()
{
  nodes.clear();
  vertices.clear();
}

void ObjBuilder::write(ostream &out)
{
/*
  for(auto v : vertices){
    out << "v " << v.x <<" " <<v.y<<" 0"<<endl;
  }
  for(auto & n:nodes){
    out << "g "<<n.name<< endl;
    for(auto & f: n.faces){
      out <<"f";
      for(auto ix:f){
        out <<" "<<ix;
      }
      out << endl;
    }
  }
 */
  Bbox<double> box;
  for(auto v : vertices){
    box.include(v.x, -v.y);
  }

  out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"<<endl;
  out << "<svg viewBox=\""<<box.left<<" "<<box.bottom<<" "<<box.width()<<" "<<box.height()<<"\" xmlns=\"http://www.w3.org/2000/svg\">"<<endl;

  for(auto & n:nodes){
    out << "<g id=\""<<n.name<<"\">" << endl;
    for(auto & f: n.faces){
      if(f.size()>1){
        out <<"  <path d=\"";
        bool first = true;
        for(auto ix:f){
          if(first){
            first = false;
            out << "M ";
          }
          else {
            out<<" L ";
          }
          auto v = vertices[ix-1];
          out <<v.x<<","<<-v.y;
        }
        out <<" Z\" />"<< endl;
      }
      else if (f.size() == 1) {
        auto ix = f[0];
        out << "<circle cx=\""<<vertices[ix-1].x<<"\" cy=\""<<-vertices[ix-1].y<<"\" r=\"25\" />"<<endl;
      }
    }
    out << "</g>" <<endl;
  }
  out << "</svg>" <<endl;
}

ObjBuilder::Node::Node(const string &name, vector<dvec2> & verts)
  :verts(verts)
  ,name(name)
{
}




