#include "gamerenderer.h"

#include "asyncbufferrenderer.h"

GameRenderer::GameRenderer()
  : defaultRenderer(make_unique<AsyncBufferRenderer>())
{}

void GameRenderer::addGeometry(Renderer::tid_t tid, const vector<glm::dvec2> &coordinates){
  vector<glm::vec2> r;
  r.reserve(coordinates.size());
  for(auto v: coordinates){
    r.push_back(v);
  }
  addGeometry(tid, move(r));
}

void GameRenderer::addGeometry(Renderer::tid_t tid, const vector<glm::vec2> &coordinates){
  defaultRenderer->addGeometry(tid, coordinates);
}

void GameRenderer::removeGeometry(Renderer::tid_t tid){
  if(bulletType == tid){
    //never remove the bullet type
    //bulletRenderer->removeGeometry(tid);
  } else {
    defaultRenderer->removeGeometry(tid);
  }
}

void GameRenderer::reset(unsigned int upperBound){
  defaultRenderer->reset(upperBound);
}

void GameRenderer::submit(){
  defaultRenderer->submit();
}

void GameRenderer::draw(Renderer::tid_t tid, const glm::vec3 &transform, uint8_t layer, uint8_t color ){
  defaultRenderer->draw(tid, transform, layer, color);
}
