
#include "model.h"

#include <fstream>
#include <sstream>
#include <iostream>

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

Model::Model(const Mesh & mesh, unsigned int refs, double mass, double area)
  :refCount(refs)
  , mass(mass)
  , area(area)
{
  meshes.push_back(mesh);
}

Model::Model(const vector<Mesh> && meshes, unsigned int refs, double mass, double area)
: meshes {meshes}
, refCount{refs}
, mass(mass)
, area(area)
{
  
}

// draws the model, and thus all its meshes
void Model::Draw(const Shader & shader, unsigned int hints) const
{
  for(unsigned int i = 0; i < meshes.size(); i++){
    meshes[i].Draw(shader, hints);
  }
}

