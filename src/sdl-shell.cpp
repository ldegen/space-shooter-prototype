#include <glad/glad.h>
#include "sdl-shell.h"
#include "misc-utils.h"
#include "protocol.h"
#include "flags.h"
#include <SDL.h>

SdlShell::SdlShell(Config & config, Application & app, SDL_Window * window, SDL_GLContext cx)
  : app(app)
  , config(config)
  , window(window)
  , context(cx)
  , buttons(0)
{
}

int SdlShell::run(Config & config, Application & app){
  //The window we'll be rendering to
  SDL_Window* window = NULL;

  //Initialize SDL
  if( SDL_Init( SDL_INIT_EVERYTHING ) < 0 )
  {
    printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
    return -1;
  }
  SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);


  SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, MY_GL_MAJOR_VERSION );
  SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, MY_GL_MINOR_VERSION );
#ifdef GLES2
  SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES );
#else
  SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
#endif

  //Create window
  window = SDL_CreateWindow( "SDL Tutorial",
      SDL_WINDOWPOS_UNDEFINED,
      SDL_WINDOWPOS_UNDEFINED,
      config.screenWidth(),
      config.screenHeight(),
      SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE|SDL_WINDOW_SHOWN );
  if( window == NULL )
  {
    printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
    return -1;
  }
  int exitCode = withWindow(config, app, window);



  //Destroy window
  SDL_DestroyWindow( window );

  //Quit SDL subsystems
  SDL_Quit();

  return exitCode;
}
int SdlShell::withWindow(Config & config, Application & app, SDL_Window * window){

  SDL_GLContext gContext = SDL_GL_CreateContext(window );
  if( gContext == NULL )
  {
    printf( "OpenGL context could not be created! SDL Error: %s\n", SDL_GetError() );
    return -1;
  }
  int exitCode = 0;
  int err = SDL_GL_MakeCurrent(window, gContext);
  if(err){
    printf( "Error during SDL_GL_MakeCurrent! SDL Error: %s\n", SDL_GetError() );
    exitCode = -1;
  } else {
    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!LOAD_GL_LOADER((GLADloadproc)SDL_GL_GetProcAddress)) {
      error("Failed to initialize GLAD");
      return -1;
    }
    SdlShell instance(config, app, window, gContext);
    exitCode = app.run(instance);
  }
  SDL_GL_DeleteContext(gContext);
  return exitCode;
}

void SdlShell::makeContextCurrent()
{
  int err = SDL_GL_MakeCurrent(window, context);
  if(err){
    printf( "Error during SDL_GL_MakeCurrent! SDL Error: %s\n", SDL_GetError() );
  }
}

void SdlShell::makeContextNonCurrent()
{

}

unsigned int buttonPressed(SDL_Event * e, SDL_Keycode sym, unsigned int prevMask, unsigned int buttonMask){
  if(e->key.keysym.sym == sym){
    if (e->key.state == SDL_PRESSED){ // key pressed
      return prevMask | buttonMask;
    }
    else { // key released
      //bit should be set iff it is set on the lhs and not on the right
      //so a AND NOT b, i guess.
      return prevMask & ( ~ buttonMask);
    }
  }
  return prevMask;
}
void SdlShell::pollEvents(){
  SDL_Event e;
  while( SDL_PollEvent( &e ) != 0 ){
    //User requests quit
    if( e.type == SDL_QUIT ){
      app.windowCloseRequested(*this);
    } 
    else if (e.type == SDL_KEYDOWN || e.type ==SDL_KEYUP){
      buttons = buttonPressed(&e , SDLK_KP_8      , buttons, BTN_FORWARD_THRUST_A);
      buttons = buttonPressed(&e , SDLK_KP_5      , buttons, BTN_REVERSE_THRUST_A);
      buttons = buttonPressed(&e , SDLK_KP_4      , buttons, BTN_TURN_LEFT_A);
      buttons = buttonPressed(&e , SDLK_KP_6      , buttons, BTN_TURN_RIGHT_A);
      buttons = buttonPressed(&e , SDLK_KP_0      , buttons, BTN_FIRE_A);
      buttons = buttonPressed(&e , SDLK_e      , buttons, BTN_STOP_A);
      buttons = buttonPressed(&e , SDLK_UP     , buttons, BTN_FORWARD_THRUST_A << PER_PLAYER_BUTTONS);
      buttons = buttonPressed(&e , SDLK_DOWN   , buttons, BTN_REVERSE_THRUST_A << PER_PLAYER_BUTTONS);
      buttons = buttonPressed(&e , SDLK_LEFT   , buttons, BTN_TURN_LEFT_A << PER_PLAYER_BUTTONS);
      buttons = buttonPressed(&e , SDLK_RIGHT  , buttons, BTN_TURN_RIGHT_A << PER_PLAYER_BUTTONS);
      buttons = buttonPressed(&e , SDLK_SPACE  , buttons, BTN_FIRE_A << PER_PLAYER_BUTTONS);
      buttons = buttonPressed(&e , SDLK_RETURN , buttons, BTN_STOP_A << PER_PLAYER_BUTTONS);
      buttons = buttonPressed(&e , SDLK_p      , buttons, BTN_TOGGLE_PAUSE);
      buttons = buttonPressed(&e , SDLK_ESCAPE , buttons, BTN_QUIT);
      app.buttonStateChanged(*this, buttons);
    }
    else if (e.type == SDL_MOUSEBUTTONUP || e.type == SDL_MOUSEBUTTONDOWN){
      int xpos = e.button.x;
      int ypos = e.button.y;
      if (e.button.state == SDL_PRESSED){
        app.mouseButtonPressed(*this,xpos,ypos);
      } else if (e.button.state == SDL_RELEASED){
        app.mouseButtonPressed(*this,xpos,ypos);
      }
    }
    else if (e.type == SDL_MOUSEMOTION){
      int xpos = e.motion.x;
      int ypos = e.motion.y;
      app.mouseMoved(*this, xpos, ypos);
    }
    else if (e.type == SDL_MOUSEWHEEL){
      int xoffset = e.wheel.x;
      int yoffset = e.wheel.y;
      app.mouseScrolled(*this, xoffset, yoffset);
    }
    else if (e.type == SDL_WINDOWEVENT){
      switch (e.window.event){
        case SDL_WINDOWEVENT_RESIZED:
          int w;
          int h;
          SDL_GL_GetDrawableSize(window, &w, &h);
          app.windowResized(*this, w, h);
          break;
      } 
    }
  }
}

void SdlShell::swapBuffers(){
  SDL_GL_SwapWindow(window);
}

WindowSize SdlShell::windowSize(){
  WindowSize ws;
  SDL_GL_GetDrawableSize(window, &ws.width, &ws.height);
  return ws;
}

unsigned int SdlShell::buttonState(){
  return buttons;
}

MouseState SdlShell::mouseState(){
  int x,y;
  unsigned int btnState = SDL_GetMouseState(&x, &y);
  return {
    .x=static_cast<double>(x), 
    .y=static_cast<double>(y),
    .buttons=btnState & SDL_BUTTON(SDL_BUTTON_LEFT)
  };
  
}

double SdlShell::time(){
  return (double)SDL_GetTicks() / 1000.0;
}

bool SdlShell::windowShouldClose(){
  return closeRequested;
}

void SdlShell::requestWindowClose(){
  closeRequested=true;
}

