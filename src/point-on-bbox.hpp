#ifndef POINT_ON_BBOX_HPP
#define POINT_ON_BBOX_HPP

#include "bbox.hpp"
#include <glm/glm.hpp>
#include <assert.h>
#include <cmath>
#include <iostream>
using std::vector; 
using std::pair;
using glm::dvec2;
using std::atan2;
class PointOnBbox {

  private:
    double EPSILON = 0.00000000001; //FIXME
    static int nextSide(int prevSide){
      switch(prevSide){
        case BBOX_BOTTOM: return BBOX_RIGHT;
        case BBOX_RIGHT: return BBOX_TOP;
        case BBOX_TOP: return BBOX_LEFT;
        case BBOX_LEFT: return BBOX_BOTTOM;
      }
      return 0;
    }

  int side;
  double ratio;
  public:


    bool operator !=(const PointOnBbox & b) const{
      // I know, I know... but for our purpose, this will work
      return b.side != side || std::fabs(b.ratio - ratio) > EPSILON;  
    }
    bool operator ==(const PointOnBbox & b) const{
      // I know, I know... but for our purpose, this will work
      return b.side == side && std::fabs(b.ratio - ratio) <= EPSILON;  
    }
    PointOnBbox(int side, double ratio) : side{side}, ratio{ratio} {};

    PointOnBbox next(const PointOnBbox & target) const {
      if(side == target.side && ratio <= target.ratio){
        return target; 
      }  
      return PointOnBbox {nextSide(side), 0.0};
    } 

    template<typename T>
    glm::tvec2<T> position(const Bbox<T> & bbox) const {
      glm::tvec2<T> r;
      switch(side){
        case BBOX_BOTTOM: r.x = bbox.left + ratio * (bbox.right-bbox.left); 
                          r.y = bbox.bottom; 
                          break;
        case BBOX_TOP:    r.x = bbox.right - ratio * (bbox.right-bbox.left); 
                          r.y = bbox.top; 
                          break;
        case BBOX_LEFT:   r.x = bbox.left;
                          r.y = bbox.top - ratio * (bbox.top-bbox.bottom); 
                          break;
        case BBOX_RIGHT:  r.x = bbox.right;
                          r.y = bbox.bottom + ratio * (bbox.top-bbox.bottom); 
                          break;
        default:          throw "Was der fick?";
      } 
      //std::cout<<"side: "<<side<<", ratio: "<<ratio<<", x: "<<r.x<<", y: "<<r.y<<std::endl;
      return r; 
    };
    
    /*
     * c is a point within the bbox. v is the direction of a ray starting 
     * at c. We are looking for a point s = (sx,sy) which is at the intersection of said
     * ray with the given bounding box.
     */
    template<typename T>
    static PointOnBbox project(const Bbox<T> & bbox, const glm::tvec2<T> & c, const glm::tvec2<T> & v, T EPSILON=0.0000001){
      //assert(bbox.contains(c.x, c.y));

      dvec2 s;
      double ratio;
      int side = bbox.onBorder(c.x, c.y);

      if (side){
        s = c;
      }
      else {
        dvec2 v0 = dvec2{bbox.left,bbox.bottom} - c;
        dvec2 v1 = dvec2{bbox.right,bbox.bottom} - c;
        dvec2 v2 = dvec2{bbox.right,bbox.top} - c;
        dvec2 v3 = dvec2{bbox.left,bbox.top} - c;

        double theta0 = atan2(v0.y, v0.x);
        double theta1 = atan2(v1.y, v1.x);
        double theta2 = atan2(v2.y, v2.x);
        double theta3 = atan2(v3.y, v3.x);
        double theta = atan2(v.y, v.x);

        side = 
            (theta0 <= theta && theta < theta1) ? BBOX_BOTTOM
          : (theta1 <= theta && theta < theta2) ? BBOX_RIGHT
          : (theta2 <= theta && theta < theta3) ? BBOX_TOP
          :                                       BBOX_LEFT;

        switch(side){
          case BBOX_TOP:
            s.y = bbox.top; 
            s.x = c.x + v.x * (s.y - c.y) / v.y;
            break;
          case BBOX_BOTTOM:
            s.y = bbox.bottom; 
            s.x = c.x + v.x * (s.y - c.y) / v.y;
            break;
          case BBOX_RIGHT:
            s.x = bbox.right;
            s.y = c.y + v.y * (s.x - c.x) / v.x;
            break;
          case BBOX_LEFT:
            s.x = bbox.left;
            s.y = c.y + v.y * (s.x - c.x) / v.x;
            break;
        } 
      }
      switch(side){
        case BBOX_TOP:    ratio = (bbox.right - s.x)  / (bbox.right - bbox.left); break;
        case BBOX_BOTTOM: ratio = (s.x - bbox.left)   / (bbox.right - bbox.left); break;
        case BBOX_LEFT:   ratio = (bbox.top - s.y)    / (bbox.top - bbox.bottom); break;
        case BBOX_RIGHT:  ratio = (s.y - bbox.bottom) / (bbox.top - bbox.bottom); break;
      }
      //std::cout<<"side: "<<side<<", c: ("<<c.x<<", "<<c.y<<"), v: ("<<v.x<<", "<<v.y<<"), s: ("<<s.x<<", "<<s.y<<"), ratio: "<<ratio<<std::endl;

      return {side, ratio};
    }

};
#endif
