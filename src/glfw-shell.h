#ifndef GLFW_SHELL_H
#define GLFW_SHELL_H
#include "application.h"
#include "shell.h"
#include "config.hpp"

class GLFWwindow;

class GlfwShell : public Shell {
  Application & app;
  Config & config;
  GLFWwindow * window;
  bool checkButtons=false;
  unsigned int buttons;
  unsigned int joyButtons;
  int fbWidth=0;
  int fbHeight=0;
  
  static void framebuffer_size_callback(GLFWwindow* window, int width, int height);
  static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
  static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos);
  static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
  static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
 
  private:
    GlfwShell(Config & config, Application & app, GLFWwindow * window);
  public: 


  static int run(Config & config, Application & app);

  virtual void pollEvents();
  virtual void swapBuffers();
  virtual WindowSize windowSize();
  virtual unsigned int buttonState();
  virtual MouseState mouseState();
  virtual double time();
  virtual bool windowShouldClose();
  virtual void requestWindowClose();
  void checkJoystick( unsigned int & bs, bool second);
  virtual void makeContextCurrent() override;
  virtual void makeContextNonCurrent() override;
  void checkGlfwError();
};
#endif
