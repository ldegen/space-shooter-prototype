#include "misc-utils.h"
#include "logic.h"
#include "protocol.h"
#include "bbox.hpp"
#include "fragment.h"
#include <bitset>
#include <iostream>
#include <glm/gtc/random.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL //TODO: we do not really need this...
#include <glm/gtx/rotate_vector.hpp>
#include <assert.h>
const int numVerts = 15;
const int numShapes = 10;
const int numBodies = 15;
const int numPlanetVertices = 15;
const double desiredTotalArea = 1090.0;
const double totalAreaThreshold = 10.0;
const double planetDensity=100000.0;
const double planetFriction=0.7;
const double planetElasticity=0.4;
const double planetHealth=1000000000.0;
const double planetRadius = 36.0;
const double planetRotationSpeed = 0.11;
const double bulletSpeed = 70.0;
const double bulletDistance=2.0;
const double bulletDensity=2.5;
const double bulletFriction=0.7;
const double bulletElasticity=0.6;
const double bulletHealth=1000.0;
const double bulletTTL = 3.0;
const double playerHealth=10000.0;
const double fragmentHealth=8000.0;
const double asteroidHealth=30000.0;
const double asteroidDensity=0.5;
const double asteroidFriction=0.7;
const double asteroidElasticity=0.4;
const double debrisTTL = 4.0;
const double shotDelay = 0.04;
const double angularVelocityIncrement = 0.25;
const double maxAngularVelocity = 7.5;
const double maxTorque = 150.0;
const double thrust = 80.0;
const double minAsteroidRadius = 5.0;
const double maxAsteroidRadius = 12.0;
const double radiusDerivation = 0.7;
const double angleDerivation = 0.3;
const double minFragmentArea=0.15;
const double minFragmentRadius=0.125;
const double minDebrisArea=0.002;
const double maxDebrisArea=1.5;
const double maxAsteroidDx=6.0;
const double maxAsteroidDy=6.0;
const double maxAsteroidDTheta=0.0;
const double voronoiFeatureDerivation=0.1;
const int voronoiFeatures=20;

std::vector<glm::dvec2> asteroid2D(unsigned int N, float dR, float dtheta, float mR=1.0){
  std::vector<glm::dvec2> vertices;
  vertices.reserve(N);
  for (unsigned int i = 0; i < N; i++){
    const float mtheta = 2.0f * glm::pi<float>() * (float) i / (float) N;
    const float theta = glm::gaussRand<float>(mtheta, dtheta);
    const float r = glm::gaussRand<float>(mR, dR);
    const float x = r * glm::cos(theta);
    const float y = r * glm::sin(theta);

    vertices.push_back(glm::dvec2 {x,y});
  }
  return vertices;
};

std::vector<glm::dvec2> spaceship2D(){
  return std::vector<glm::dvec2> {
    {0.0,3.0},
    {-1.0,0.0},
    {-0.745,-0.51},
    {0.745,-0.51},
    {1.0,0.0}
  };
}


std::vector<glm::dvec2> bullet2D(){
  //distance the bullet travels during one frame
  //(assuming it was shot from a non-moving ship)
  double length = bulletSpeed / 55.0;



  return std::vector<glm::dvec2> {
    {0.1,length},
    {-0.1, length},
    {-0.2,0.0},
    {0.2,0.0}
  };
}

std::vector<glm::dvec2> planet2D(){
  std::vector<glm::dvec2> verts;
  int N = numPlanetVertices;
  verts.reserve(N);

  double r = planetRadius;
  for (int i=0;i<N;i++){
    double theta = ((double)i)*glm::pi<double>()*2.0/((double)N);
    double x = r * glm::cos(theta);
    double y = r * glm::sin(theta);
    verts.push_back({x,y});
  }
  return verts;
}

unsigned int Logic::nextTid()
{
  unsigned int tid;
  if(freeTids.empty()) {
    tid = static_cast<unsigned int>(bodyTypes.size());
    bodyTypes.resize(tid+1);
  }
  else {
    tid = freeTids.front();
    freeTids.pop_front();
  }
  return tid;
}

Logic::Logic(Config & config, ProtocolWriter & writer)
  : bbox {config.bboxLeft(), config.bboxBottom(), config.bboxRight(), config.bboxTop()}
  , writer {writer}
  , cameraMan {bbox}
  , config(config)
  , plotter("stats")
  , oscAddress {config.oscHost(), config.oscPort()}
{
  bodyTypes.reserve(1024);
  bodyTypes.resize(reservedTypes);
}

void Logic::onExit()
{
  cameraMan.onExit();
  plotter.close();
}

State randomState(const Bbox<double> & bbox){
  return {
    glm::linearRand(bbox.left, bbox.right), //x
      glm::linearRand(bbox.bottom, bbox.top), //y
      2.0, //glm::linearRand(-3.14,3.14), //theta
      glm::linearRand(-maxAsteroidDy,maxAsteroidDy), //dy
      glm::linearRand(-maxAsteroidDx,maxAsteroidDx), //dx
      glm::linearRand(-maxAsteroidDTheta, maxAsteroidDTheta)  //dtheta
  };
}
void Logic::createShipShape(){
  Shape spaceshipShape {spaceship2D(), 0.3, 0.5, 0.5, HINT_FILL};
  spaceshipShape = spaceshipShape.normalized();
  BodyType bt{playerType,std::vector<Shape> {spaceshipShape}};
  bodyTypes[playerType] = bt;
  writer.onBodyType(std::move(bt));
}
void Logic::createBulletShape(){
  Shape bulletShape {bullet2D(), bulletDensity, bulletFriction, bulletElasticity, HINT_FILL};
  bulletShape = bulletShape.normalized();
  BodyType bt {bulletType,std::vector<Shape> {bulletShape}};
  bodyTypes[bulletType] = bt;
  writer.onBodyType(std::move(bt));
}

void Logic::createPlanetShape(){
  Shape planetShape {planet2D(), planetDensity, planetFriction, planetElasticity, HINT_FILL};
  planetShape = planetShape.normalized();
  BodyType bt {planetType,std::vector<Shape> {planetShape}};
  bodyTypes[planetType] = bt;
  writer.onBodyType(std::move(bt));
}
void Logic::createPlanet(){
  unsigned long uid = nextUid++;
  writer.write<cmd_create>({.uid=uid, .tid=planetType, .static_body=true});
  //writer.write<cmd_set_x_y_theta>({.x=-150, .y=0, .theta=0.0});
  writer.write<cmd_tag>({.set=TAG_GRAVITY_SOURCE | TAG_STATIC});
  writer.write<cmd_set_dtheta>({.dtheta=planetRotationSpeed});
  //writer.write<cmd_hint>({.set=hints});
  double area = lookupBodyType(planetType)->area();
  objects[uid] = object_record{.uid=uid, .tid=planetType, .timeCreated=t,.area=area, .health=planetHealth};
}

void Logic::createAsteroids(){
  double newArea = 0.0;
  while(totalArea + newArea < desiredTotalArea-totalAreaThreshold){
    unsigned long uid = nextUid++;
    unsigned int tid = nextTid();
    double meanRadius = glm::linearRand(minAsteroidRadius, maxAsteroidRadius);
    Shape shape {
      asteroid2D(numVerts,radiusDerivation, angleDerivation, meanRadius),
        asteroidDensity,asteroidFriction,asteroidElasticity
    };
    shape = shape.normalized();
    BodyType bt {tid, std::vector<Shape> {shape}} ;
    newArea += bt.area();
    bodyTypes[tid] = bt;
    State s {randomState(bbox)};
    writer.onBodyType(std::move(bt));
    writer.write<cmd_create>({.uid=uid, .tid=tid});
    writer.write<cmd_set_x_y_theta>({.x=s.x, .y=s.y, .theta=s.theta});
    writer.write<cmd_set_dx_dy_dtheta>({.dx=s.dx, .dy=s.dy, .dtheta=s.dtheta});
    objects[uid] = {.uid=uid, .tid=tid, .timeCreated=t, .area=shape.area(), .health=asteroidHealth};
  }
}

void Logic::createPlayer(unsigned int shift, double x, double y, double heading, unsigned int hints){
  unsigned long uid = nextUid++;
  writer.write<cmd_create>({.uid=uid, .tid=playerType});
  writer.write<cmd_set_x_y_theta>({.x=x, .y=y, .theta=heading});
  writer.write<cmd_tag>({.set=TAG_TRACK | TAG_PLAYER});
  writer.write<cmd_hint>({.set=hints});
  double area = lookupBodyType(playerType)->area();
  objects[uid] = object_record{.uid=uid, .tid=playerType, .timeCreated=t, .hints=hints, .area=area, .health=playerHealth, .buttonShift=shift};
}
double rand(double min, double max){
  double r = static_cast<double>(std::rand()) / static_cast<double>(RAND_MAX);
  return min + r * (max - min);
}
void Logic::respawnPlayer(unsigned int hints)
{
  double theta = rand(-M_PI, M_PI);
  //double theta = M_PI/2.0;
  //double theta = 0;
  double r = planetRadius + 0.745; 
  double x = std::cos(theta)*r;
  double y = std::sin(theta)*r;
  unsigned int shift = (hints & HINT_PLAYER_B) ? PER_PLAYER_BUTTONS : 0;
  createPlayer(shift,x,y,theta-(M_PI/2.0),hints);
  cameraMan.reset(t);
}

int sgn(double val) {
  return (double(0) < val) - (val < double(0));
}

double absMin(double limit, double v){
  return std::abs(v) <= limit ? v : limit * sgn(v);
}

void Logic::processControls(const Blip * playerBlip){
  auto it = objects.find(playerBlip->uid);
  if(it == objects.end()){
    return; 
  }
  
  object_record & player = it->second;
  unsigned int lshift =player.buttonShift;

  if(isButtonDown(BTN_STOP_A<<lshift)){
    writer.write<cmd_select_uid>({.uid=player.uid});
    writer.write<cmd_set_dx_dy_dtheta>({.dx=0.0, .dy=0.0, .dtheta=0.0});
  }
  else{
    double forceX = 0.0;
    double forceY = 0.0;
    double s = 1.0;

    if(isButtonDown(BTN_FORWARD_THRUST_A<<lshift)){
      forceY += thrust;
    }
    if(isButtonDown(BTN_REVERSE_THRUST_A<<lshift)){
      forceY -= thrust;
    }
    if(isButtonDown(BTN_TURN_LEFT_A<<lshift)){
      player.desiredAngularVelocity += angularVelocityIncrement;
      player.desiredAngularVelocity = std::min(player.desiredAngularVelocity,maxAngularVelocity);
      //player.desiredAngularVelocity = maxAngularVelocity;
    } 
    else if(isButtonDown(BTN_TURN_RIGHT_A<<lshift)){
      player.desiredAngularVelocity -= angularVelocityIncrement;
      player.desiredAngularVelocity = std::max(player.desiredAngularVelocity,-maxAngularVelocity);
      //player.desiredAngularVelocity = -maxAngularVelocity;
    }
    else{
      player.desiredAngularVelocity = 0.0; 
    }
    BodyType & bt = bodyTypes[playerType];

    double deltaOmega = player.desiredAngularVelocity - playerBlip->state.dtheta;
    double deltaT = 1.0/50.0; 
    double I = bt.momentOfInertia();

    //double torque = std::min(1.0, s*std::abs(d)) * maxTorque * (double)sgn(d);
    double torque = absMin(maxTorque, I * deltaOmega / deltaT);

    writer.write<cmd_select_uid>({.uid=player.uid});
    writer.write<cmd_set_fx_fy_torque>({.fx=forceX, .fy=forceY, .torque=torque});
    //writer.write<cmd_set_dtheta>({.dtheta=player.desiredAngularVelocity});
  }

  if(isButtonDown(BTN_FIRE_A<<lshift) && t - player.t_lastShot > shotDelay){
    player.t_lastShot = t;
    spawnBullet(playerBlip);
  }
}
void Logic::spawnBullet(const Blip * playerBlip){
    const State & s = playerBlip->state;
    double dx = -std::sin(s.theta);
    double dy = std::cos(s.theta);
    double dist = bulletDistance;
    double speed = bulletSpeed;
    unsigned long uid = nextUid++;
    double area = lookupBodyType(bulletType)->area();
    objects[uid]={.uid=uid, .tid=bulletType, .timeCreated=t, .maxAge=bulletTTL,.area=area, .health=bulletHealth};
    writer.write<cmd_create>({.uid=uid, .tid=bulletType});
    writer.write<cmd_set_x_y_theta>({.x=0.0, .y=dist, .rel=playerBlip->uid});
    //writer.write<cmd_set_x_y_theta>({.x=s.x+dist*dx, .y=s.y+dist*dy, .theta=s.theta});
    writer.write<cmd_set_dx_dy_dtheta>({.dx=s.dx + speed*dx, .dy=s.dy+speed*dy, .dtheta=0.0});
    writer.write<cmd_hint>({.set=(playerBlip->hints & (HINT_PLAYER_A|HINT_PLAYER_B))});
   //----------------
  cmd_play_sound cmd;
  cmd.velocity = 80;
  cmd.pitch = 45;
  cmd.preset = 2.0;
  cmd.x=s.x;
  cmd.y=s.y;
  writer.write<cmd_play_sound>(cmd);
   //----------------
}
void Logic::onMessage(const msg_init * msg){
  createShipShape();
  createBulletShape();
  createPlanetShape();
  createPlanet();
  respawnPlayer(HINT_PLAYER_A);
  respawnPlayer(HINT_PLAYER_B);
  //createPlayer(
      //0,
      ////-60, 0.0, -glm::pi<double>()/2.0,
      //0.5*bbox.left, 2.0, 0.0,
      //HINT_PLAYER_A 
      //);
  //createPlayer(
      //PER_PLAYER_BUTTONS,
      //0.5*bbox.right, -2.0, glm::pi<double>()/2.0,
      //HINT_PLAYER_B 
      //);
  createAsteroids();
  //createShatterTest();
}
void Logic::onMessage(const msg_error * msg){
  warn("protocol error code %u, argument is %u", msg->code, msg->arg);
}
void Logic::onMessage(const msg_body_created * msg){
  //std::cout << "body created: " <<msg->uid<<std::endl;
  BodyType * bt = lookupBodyType(msg->tid);
  if(bt && msg->tid != planetType){
    totalArea += bt->area();
  }

}
void Logic::onMessage(const msg_body_deleted * msg){
  auto objIt = objects.find(msg->uid);
  if(objIt!=objects.end()){
      totalArea -= objIt->second.area;

      if(objIt->second.tid == playerType){
        cameraMan.reset(t);
        sequencer.schedule(t+2.5, std::bind(&Logic::respawnPlayer, this, objIt->second.hints));
      }


      objects.erase(objIt);
  }
  //std::cout << "body deleted: " <<msg->uid<<std::endl;
  auto & bt = bodyTypes[msg->tid];
  if(bt.tid() > reservedTypes){
    freeTids.push_back(bt.tid());
  }
}
void Logic::onMessage(const msg_button_state_changed * msg){
  buttonStates.push_back(msg->mask);
}

void Logic::onMessage(const msg_close_requested *msg)
{
  _keepRunning = false;
  writer.write<cmd_quit>({});
}
void Logic::beforeBlips(const msg_blips & msg, const Blip * blips, const data_tag * tags){
  this->t=msg.t;
  pois.clear();
}



void Logic::afterBlips(const msg_blips & msg, const Blip * blips, const data_tag * tags){
  cleanupDebris();
  createAsteroids();
  if(wasButtonPressed(BTN_TOGGLE_PAUSE)){
    //std::cout<<"toggle pause (logic)"<<std::endl;
    writer.write<cmd_toggle_pause>({}); 
  }
  if(wasButtonPressed(BTN_VIEW)){
    total = !total;
  }
  if(wasButtonPressed(BTN_QUIT)){
    _keepRunning = false;
    writer.write<cmd_quit>({});
  }
  if(wasButtonPressed(BTN_QUIT_GUESTURE) && wasButtonPressed(BTN_QUIT_GUESTURE<<PER_PLAYER_BUTTONS)){
    _keepRunning = false;
    writer.write<cmd_quit>({});
  }
  if(!buttonStates.empty()){
    prevButtonState = buttonStates.back();
    buttonStates.clear();
  }
  cameraTracking();
  sequencer.executePending(t);
  //plotter.at(t).line("totalArea",totalArea).line("numberOfObjects",objects.size()).flush();
}

void Logic::cameraTracking()
{
  cmd_look_at cmd;
  if(total){
    cmd = cameraMan.total(t);
  }else{
    switch(pois.size()){
      case 1: cmd = cameraMan.track(t,pois[0]); break;
      case 2: cmd = cameraMan.track(t,pois[0],pois[1]); break;
      default: cmd = cameraMan.total(t); break;
    }
  }
  writer.write(cmd);
}

bool Logic::keepRunning()
{
  return _keepRunning;
}
void Logic::onBlip(const msg_blips & msg, const Blip & blip){
  if(blip.tid == playerType && !(blip.state.flags & (MASK_GONE|MASK_IN_LIMBO))){
    processControls(&blip);
    /*
    glm::dvec2 pos{blip.state.x,blip.state.y};
    glm::dvec2 vel{blip.state.dx, blip.state.dy};
    double deltaT = 0;//config.simulationTimeStep();
    */
    pois.push_back(&blip);
  }

}
bool Logic::examineTags(const msg_blips & msg, unsigned int tags){
  return tags & TAG_PLAYER;
}
bool Logic::wasButtonPressed(unsigned int mask){
  unsigned int prevState = prevButtonState;
  for (auto buttonState : buttonStates){
    unsigned int nextState = (buttonState & mask);
    if((nextState != prevState) && nextState){
      return true; 
    }
    prevState = nextState;
  }
  return false;
}
bool Logic::wasButtonReleased(unsigned int mask){
  unsigned int prevState = 0;
  for (auto buttonState : buttonStates){
    unsigned int nextState = (buttonState & mask);
    if(nextState != prevState && !nextState){
      return true; 
    }
    prevState = nextState;
  }
  return false;
}
bool Logic::isButtonDown(unsigned int mask){
  unsigned int state = buttonStates.empty() ? prevButtonState : buttonStates.back();
  return  state & mask;
}

BodyType * Logic::lookupBodyType(const Blip & blip){
  return lookupBodyType(blip.tid);
}
BodyType * Logic::lookupBodyType(const Blip * blip){
  return lookupBodyType(blip->tid);
}
BodyType * Logic::lookupBodyType(const unsigned int tid){
  auto & bt = bodyTypes[tid];
  return &bt;
}

void Logic::blowUp(const Blip & targetBlip, const glm::dvec2 & epicenter0){

  const State & state = targetBlip.state;
  const BodyType * targetType = lookupBodyType(targetBlip);
  const Shape & origShape = targetType->shapes()[0];

  // if orig shape is very small, just let it go.
  //if(origShape.area() < 1.0){
    //return; 
  //}
  std::vector<glm::dvec2> contourVertices;
  contourVertices.reserve(origShape.numVertices());
  for(auto v0: origShape.vertices()){
    contourVertices.push_back(glm::rotate(v0, state.theta)); 
  }
  glm::dvec2 epicenter = glm::rotate(epicenter0, state.theta);
  Shape contourShape(contourVertices,origShape.density(), origShape.friction(), origShape.elasticity(), origShape.hints());
  unsigned int N = voronoiFeatures;
  std::vector<Shape> regionShapes = shatter.shatter(contourShape, N, epicenter, 1.5*targetType->r()*voronoiFeatureDerivation);
  if(targetBlip.tid != bulletType){
    shatter.__debug__flush(cout);
  }
  std::vector<Fragment> fragments;
  std::vector<data_contact> contacts;
  //contacts.reserve(collision.num_contacts);
  //contacts.insert(contacts.begin(), contacts0, contacts0+collision.num_contacts);
  fragments.reserve(regionShapes.size());
  for(auto shape: regionShapes){
    if (shape.area() < minFragmentArea){
      continue; 
    }
    Fragment fragment {Fragment::from(shape, state, contacts)};
    //containedContacts += fragment.contacts.size();
    fragments.push_back(std::move(fragment));
  } 
  unsigned int tid = nextTid();
  unsigned long uid = nextUid++;
  for(auto fragment: fragments){
    BodyType bt {(unsigned int)tid, 
      std::vector<Shape> {fragment.shape}};
    if(bt.r()>minFragmentRadius){
      bodyTypes[tid]=bt;
      writer.onBodyType(std::move(bt));
      writer.write<cmd_create>({.uid=uid, .tid=tid});
      writer.write<cmd_set_x_y_theta>({.x=fragment.r.x+state.x, .y=fragment.r.y+state.y, .theta=0});
      writer.write<cmd_set_dx_dy_dtheta>({.dx=state.dx+fragment.vt.x, .dy=state.dy+fragment.vt.y, .dtheta=0});
      writer.write<cmd_hint>({.set=targetBlip.hints});
      objects[uid] = {.uid=uid, .tid=tid, .timeCreated=t, .area=fragment.shape.area(), .health=fragmentHealth};
      tid = nextTid();
      uid = nextUid++;
    }
  }
}

void Logic::handleImpact(const Blip & blip, const glm::dvec2 & epicenter, double damage){
  auto it = objects.find(blip.uid);
  if(it==objects.end()){
    warn("Bad UID: %u", blip.uid);
  } else {
    it->second.health -= damage;

   //----------------
  //How "hard" did the two bodies hit?
  cmd_play_sound cmd;
  cmd.velocity = (1.0-(1.0/(1.0+damage/100.0)))*60+60;
  cmd.pitch = 25.0 + 70.0/(1.0+it->second.area);
  cmd.preset = blip.tid == playerType
               ? 0.0
               : blip.tid == bulletType
               ? 3.0
               : 1.0;
  cmd.x=epicenter.x;
  cmd.y=epicenter.y;
  writer.write<cmd_play_sound>(cmd);
   //----------------



    if(it->second.health <= 0.0 && !it->second.destroyed){
      writer.write<cmd_select_uid>({.uid=blip.uid});
      writer.write<cmd_delete>({});
      it->second.destroyed=true;
      blowUp(blip, epicenter );
    }
  }
}
bool Logic::handleGenericCollision(const msg_collision & collision, const data_contact * contacts){
  double damage = collision.jx_a*collision.jx_a + collision.jy_a*collision.jy_a;
  //oscAddress.send("/test","f",damage); 
  handleImpact(collision.blip_a, {contacts[0].x_a, contacts[0].y_a},damage);
  handleImpact(collision.blip_b, {contacts[0].x_b, contacts[0].y_b},damage);
  return true;
}

void Logic::onCollision(const msg_collision & collision, const data_contact * contact){
  //std::cout<<"collision, num_contacts = "<<(int)collision.num_contacts<<std::endl;
  //if(handleDebrisCollision(collision)){
    //return; 
  //}
  //writer.write<cmd_pause>({});
  if(handleGenericCollision(collision, contact)){
    return; 
  }
}

void Logic::cleanupDebris(){
  //std::cout<<"cleanupBullets"<<std::endl;
  for (auto it = objects.begin();it != objects.end();){
    object_record & object = it->second;
    if(!object.destroyed && isDebris(object)){
      //std::cout<<"cleanup debris "<<object.uid<<std::endl;
      writer.write<cmd_select_uid>({.uid=object.uid}); 
      writer.write<cmd_delete>({}); 
      object.destroyed=true;
    }
    it++;
  }
}

bool Logic::isDebris(const object_record & object){

  if(object.tid==playerType){
    return false;
  }
  double age = t - object.timeCreated;
  if (object.area > 0.0 && object.area < minDebrisArea){
    return true;
  }
  if (object.area > 0.0 && object.area < maxDebrisArea && age > debrisTTL){
    return true; 
  }
  if (object.area > 0.0 && object.area < maxDebrisArea * 3.0 && age > debrisTTL*3.0){
    return true; 
  }
  if (object.area > 0.0 && object.area < maxDebrisArea * 10.0 && age > debrisTTL*10.0){
    return true; 
  }
  if(object.maxAge > 0 && age > object.maxAge){
    return true;
  }
  return false;
}
