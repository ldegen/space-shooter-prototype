#ifndef ASYNCBUFFERRENDERER_H
#define ASYNCBUFFERRENDERER_H
#include "renderer.h"

#include <glad/glad.h>
#include <list>


class AsyncBufferRenderer : public Renderer
{

    using index_t = unsigned short;
    using object_name_t = unsigned int;

    struct vertex_t {
        vec2 position;
        vec3 transform;
        // uint8_t layer;
        uint8_t color;

    };

    struct buffer_t {
      object_name_t VBO;
      object_name_t EBO;
      GLsync fence;
    };

    struct Slot {
      index_t start;
      index_t size;
    };


    vector<vec2> positions;

    // all the slots that are currently in use
    // are kept in a table, keyed by their "user" uid
    vector< Slot> usedSlots;

    list<index_t> freeList;

    vertex_t * mappedVertices;
    index_t * mappedIndices;

    vector<buffer_t> buffers;

    size_t bufNum = 0;

    const index_t slotSize=16;
    const size_t numberOfBuffers=3;
    const index_t vboSize=16*1024;
    const index_t eboSize=3*16*1024;

    Slot * allocateSlot(tid_t tid);

    index_t indexPos = 0;
    index_t vertexPos = 0;
  public:
    AsyncBufferRenderer();
    virtual void addGeometry(tid_t tid, const vector<glm::vec2> & coordinates) override;
    virtual void removeGeometry(tid_t tid) override;
    virtual void reset(unsigned int) override;
    virtual void submit() override;
    virtual void draw(tid_t tid, const vec3 & transform, uint8_t layer, uint8_t color) override;
    ~AsyncBufferRenderer() override;
};

#endif // ASYNCBUFFERRENDERER_H
