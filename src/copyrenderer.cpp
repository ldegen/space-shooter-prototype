#include "copyrenderer.h"


CopyRenderer::CopyRenderer(unique_ptr<Renderer> delegatee, unsigned int numCopies)
  : delegatee(move(delegatee))
  , numcopies(numCopies)
{

}

void CopyRenderer::addGeometry(Renderer::tid_t tid, const vector<glm::vec2> & coordinates)
{
  auto it = ranks.find(tid);
  tid_t rank;
  if(it == ranks.end()){
    rank = static_cast<tid_t>(ranks.size());
    ranks.insert(make_pair(tid,rank));
  } else {
    rank = it->second;
  }

  for(tid_t i = 0; i<numcopies; i++){
    delegatee->addGeometry(rank * numcopies + i, coordinates);
  }
}
void CopyRenderer::removeGeometry(Renderer::tid_t tid)
{
  auto it = ranks.find(tid);
  if(it == ranks.end()){
    return;
  }
  auto rank = it->second;
  for(tid_t i = 0; i<numcopies;i++){
    delegatee->removeGeometry(rank * numcopies + i);
  }
}

void CopyRenderer::reset(unsigned int upperBound)
{
  delegatee->reset(upperBound);
  _counters.clear();
}

void CopyRenderer::submit()
{
  delegatee->submit();
}

void CopyRenderer::draw(tid_t tid, const vec3 & transform)
{
  auto rankIt = ranks.find(tid);
  if(rankIt == ranks.end()){
    return;
  }
  tid_t rank = rankIt->second;
  auto counterIt = _counters.find(tid);
  tid_t counter;
  if(counterIt == _counters.end()){
    counter = 0;
    _counters.insert(make_pair(tid,counter));
  } else {
    counter = ++counterIt->second;
  }
  auto copytid = rank * numcopies + (counter % numcopies);
  delegatee->draw(copytid, transform);
}


