#ifndef FRAGMENT_H
#define FRAGMENT_H
#include "shape.h"
#include "protocol.h"
#include <vector>
#include <glm/glm.hpp>

class Fragment{
  public:
  // the normalized fragment shape
  const Shape shape;
  // position of the fragment center of mass relative to the center of mass
  // of the original shape.
  const glm::dvec2 r;

  // contact points contained in this fragment in world coordinates
  const std::vector<data_contact> contacts;

  // velocity of this fragment relative to that of the original body
  // This is the tangential component of the fragments center of mass *before*
  // the fracture. We assume that the radial component is eliminated at the
  // moment of the fracture, so if there are no collisions, the fragment would
  // travel on a linear path along the tangent with respect to the center of mass of 
  // the original body. When the angular velocity of the original body was zero,
  // then this value will be zero as well.
  const glm::dvec2 vt;



  Fragment(const Shape && shape, const glm::dvec2 && r, const std::vector<data_contact> && contacts, const glm::dvec2 && vt);
  /**
   * create a Fragment.
   *
   * shape0 is the shape of the fragment in the local coordinate space of the original shape.
   * state is the position/velocity of the original shape.
   * contacts is a list of contact points in world coordinates. The fragment will keep those
   * that are contained in its shape and ignore the rest.
   */
  static Fragment from(const Shape & shape0, const State & state, const std::vector<data_contact> & contacts);


};
#endif
