#ifndef CONFIG_H
#define CONFIG_H
#include <boost/program_options.hpp>
#include <iostream>
#include <string>

namespace po = boost::program_options;

class Config {

  // here, a value of 0 means: the user did not specify a value
  // The shell should pick some resolution that "makes sense".
  unsigned int _screenWidth = 0;
  unsigned int _screenHeight = 0;

  bool _interpolateTimesteps = 0;
  double _simulationTimeStep = 1.0/60.0;
  int _swapInterval = 1;
  bool _help = false;
  bool _fullscreen = false;
  double _randomSleep = 0.0;
  double _fixedSleep = 0.0;
  std::string _oscHost="localhost";
  int _oscPort=9000;
  //Bbox<double> bbox {-192,-108,192,108};
  double _bboxLeft = -192.0*2.5;
  double _bboxRight = 192.0*2.5;
  double _bboxBottom = -108.0*2.5;
  double _bboxTop = 108.0*2.5;

  public:
  static Config parseCommandLine(int argc, char** argv){
    Config config;
    po::options_description desc("Allowed options");
    desc.add_options()
      ("help",po::bool_switch(&config._help), "display this help message")
      ("screen-width,w", po::value<unsigned int>(&config._screenWidth), "Initial width of the window in pixels")
      ("screen-height,h", po::value<unsigned int>(&config._screenHeight), "initial height of the window in pixels")
      ("interpolate,i", po::bool_switch(&config._interpolateTimesteps), "Interpolate timesteps")
      ("time-step,t", po::value<double>(&config._simulationTimeStep), "Simulation timestep in seconds")
      ("swap-interval,s", po::value<int>(&config._swapInterval), "Swap interval (passed on to glfwSwapInterval)")
      ("random-sleep,r", po::value<double>(&config._randomSleep), "add a random delay to each frame, max duration in seconds")
      ("fixed-sleep,f", po::value<double>(&config._fixedSleep), "sleep for the given number of seconds between rendering frames")
      ("osc-host", po::value<std::string>(&config._oscHost), "Host address to send OSC messages to")
      ("osc-port", po::value<int>(&config._oscPort), "Port to send OSC messages to")
      ("bbox-left", po::value<double>(&config._bboxLeft), "Minimum X coordinate in world space")
      ("bbox-right", po::value<double>(&config._bboxRight), "Maximum X coordinate in world space")
      ("bbox-bottom", po::value<double>(&config._bboxBottom), "Minimum Y coordinate in world space")
      ("bbox-top", po::value<double>(&config._bboxTop), "Maximum Y coordinate in world space")
      ("fullscreen,F", po::bool_switch(&config._fullscreen), "Run in fullscreen mode")
      ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if(config.help()){
      std::cerr <<desc<<std::endl;
    }
    return config;
  }

  double bboxLeft(){
    return _bboxLeft; 
  }
  double bboxRight(){
    return _bboxRight; 
  }
  double bboxBottom(){
    return _bboxBottom; 
  }
  double bboxTop(){
    return _bboxTop; 
  }

  int oscPort(){
    return _oscPort; 
  }

  std::string oscHost(){
    return _oscHost; 
  }
  unsigned int screenWidth(){
    return _screenWidth;
  }

  unsigned int screenHeight(){
    return _screenHeight;
  }

  bool interpolateTimesteps(){
    return _interpolateTimesteps;
  }

  double simulationTimeStep(){
    return _simulationTimeStep;
  }

  //This is passed to glfwSwapInterval()
  // typically, one would use 1 to
  // effectively enable vsync. Zero disables it.
  // with the glfw app, higher integers allow
  // for waiting more than one vblank before swapping.
  int swapInterval(){
    return _swapInterval;
  }
  double fixedSleep(){
    return _fixedSleep; 
  }
  double randomSleep(){
    return _randomSleep; 
  }
  bool fullscreen(){
    return _fullscreen;
  }

  bool help(){
    return _help; 
  }
};
#endif
