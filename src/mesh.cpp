#include "flags.h"
#include "mesh.h"
#include "shader.h"
#include <fstream>
#include <sstream>
#include <iostream>


const glm::vec3 UNIT_X(1.0f,0.0f,0.0f);
const glm::vec3 UNIT_Y(0.0f,1.0f,0.0f);
const glm::vec3 UNIT_Z(0.0f,0.0f,1.0f);


Vertex vertex2D(const float & x, const float & y, const glm::vec3 & n = UNIT_Z){
  return Vertex{
    .Position = glm::vec3(x , y, 0.0f)
      /*,
      n,
      glm::vec2(0.0f, 0.0f),
      glm::vec3(1.0f, 0.0f, 0.0f),
      glm::vec3(0.0f, 1.0f, 0.0f)
          */
  };
}


Mesh Mesh::polygon2D(const Bbox<double> & bbox, unsigned int hints) {
  return polygon2D(vector<glm::dvec2>{
      {bbox.left, bbox.bottom},
      {bbox.right, bbox.bottom},
      {bbox.right, bbox.top},
      {bbox.left, bbox.top}
      }, hints);
}

Mesh Mesh::polygon2D(const vector<glm::dvec2> & coords0, unsigned int hints) {
  vector<glm::vec2> coords;
  coords.reserve(coords0.size());
  for(auto v : coords0){
    coords.push_back({v.x,v.y});
  }
  return polygon2D(coords, hints);
}
Mesh Mesh::polygon2D(const vector<glm::vec2> & coords, unsigned int hints) {
  bool fill = (hints & HINT_FILL);
  vector<Vertex> vertices;
  vector<v_ix> indices;
  v_ix offset = 0;
  const unsigned int mode = fill ? GL_TRIANGLE_FAN : GL_LINE_LOOP;
  TopologyType topoType = fill ? filled : closed_outline;
  if(fill) {
    offset++;
    vertices.push_back(vertex2D(0,0, UNIT_Z));
    indices.push_back(0);
  }
  for (v_ix i = 0; i < coords.size(); i++) {
    const glm::vec3 & n = (i%2 ? UNIT_X : UNIT_Y);
    vertices.push_back(vertex2D(coords[i].x, coords[i].y, n));
    indices.push_back(i+offset);
  }
  if(fill){
    if (coords.size() % 2){
      //if we have an odd number of contour vertices, the first and the last
      //cannot be identical: they need to have different barycentric coordinates
      indices.push_back(static_cast<v_ix>(vertices.size()));
      vertices.push_back(vertex2D(coords[0].x, coords[0].y, UNIT_X));
    } else {
      indices.push_back(1);
    }
  }
  return Mesh(vertices, indices, mode, topoType, hints);
}
Mesh Mesh::line2D(glm::vec2 a, glm::vec2 b, unsigned int hints){
  vector<Vertex> vertices {
    vertex2D(a.x, a.y),
    vertex2D(b.x,b.y)
  };
  vector<v_ix> indices {
    0, 1 
  };
  return Mesh(vertices, indices, GL_LINES, separate_lines, hints);
}
Mesh Mesh::cross2D(double r, unsigned int hints){
  // this is sin 45° = cos 45° = 1/sqrt(2)
  float s = 0.70710678f;
  float a = s*r;
  vector<Vertex> vertices {
    vertex2D(a, a), vertex2D(-a, -a), 
    vertex2D(-a, a), vertex2D(a, -a)
  };
  vector<v_ix> indices {
    0, 1, 2, 3 
  };
  return Mesh(vertices, indices, GL_LINES, separate_lines, hints);
}
Mesh Mesh::arrowHead2D(glm::vec2 point, unsigned int hints){
  //we want an equilateral triangle with one of the three points being the
  //given point. We want the oposite site to be perpendicular to  point and contain
  //the origin.
  
  // we know that the height of an equilateral triangle is h = a * sqrt(3)/2
  // where a is the length of each of the three sides.
  //
  // btw: did you know that sqrt(3) == tan 60°? I first went this way
  //      and satisfyingly it yields exactly the same calculation.
  float sqrt3 = 1.7320508f;
  //
  // Now, we don't know a, but we happen to know h which is |point|.
  // Thus a = 2 * h / sqrt(3) is the base length of our arrow head.
  // We know that the the origin is in the exact middle of our base line.
  // So the base must extend a/2 to the left and to the right. We can easily
  // obtain a vector q by rotating p 90° ccw. 

  glm::vec2 q {-point.y, point.x};

  // After normalizing and scaling by a/2 and -a/2 respetively, we should
  // get our two missing points. E.g. q1 = a/2 * 1/h * q = 1 / sqrt(3) * q
  //
  
  float s = 1.0f/sqrt3;
  glm::vec2 q1 = s * q; 
  glm::vec2 q2 = -s * q; 

  return polygon2D(vector<glm::vec2>{ point, q1, q2 }, hints);

}

/*  Functions  */
// constructor
Mesh::Mesh(const vector<Vertex> & vertices, const vector<v_ix> & indices,  const unsigned int mode, const TopologyType topologyType, unsigned int hints)
  : numIndices(indices.size())
  , numVertices(vertices.size())
  , mode(mode)
  , m_hints(hints)
  , topology({topologyType, numIndices})
  //, counter(createCounter())
{
  // now that we have all the required data, set the vertex buffers and its attribute pointers.
  counter = createCounter();
  setupMesh(vertices, indices);
  incrementRefCount();
}

// we need explicit move and copy constructors to get the ref counts right

// move constructor
Mesh::Mesh(Mesh && other)
  : numIndices(other.numIndices)
  , numVertices(other.numVertices)
  , VAO(other.VAO)
  , mode(other.mode)
  , m_hints(other.m_hints)
  , VBO(other.VBO)
  , EBO(other.EBO)
  , counter(other.counter)
  , topology(other.topology)
{
  // when moving, we do not need to increment refCount, instead we invalidate the
  // other handle.
  other.VAO=0;
  other.VBO=0;
  other.EBO=0;
}

// copy constructor
Mesh::Mesh(const Mesh & other)
  : numIndices(other.numIndices)
  , numVertices(other.numVertices)
  , VAO(other.VAO)
  , mode(other.mode)
  , m_hints(other.m_hints)
  , VBO(other.VBO)
  , EBO(other.EBO)
  , counter(other.counter)
  , topology(other.topology)
{
  // when copying, the other handle remains intact
  // and we increment the ref count
  incrementRefCount();

}

Mesh::~Mesh(){
  decrementRefCount();
}

Mesh & Mesh::operator=(const Mesh & other){
  numIndices = other.numIndices;
  numVertices = other.numVertices;
  VAO = other.VAO;
  EBO = other.EBO;
  VBO = other.VBO;
  mode = other.mode;
  m_hints = other.m_hints;
  counter = other.counter;
  topology = other.topology;
  incrementRefCount();
  return *this;
}
Mesh & Mesh::operator=(Mesh && other){
  numIndices = other.numIndices;
  numVertices = other.numVertices;
  VAO = other.VAO;
  EBO = other.EBO;
  VBO = other.VBO;
  mode = other.mode;
  m_hints = other.m_hints;
  counter =other.counter;
  topology = other.topology;
  other.VAO = 0;
  other.EBO = 0;
  other.VBO = 0;
  return *this;
}


glm::vec3 colorFor(const unsigned int hints){

  if(hints & HINT_PLAYER_A){
    return glm::vec3(1.0,0.0, 0.5);
  }
  if(hints & HINT_PLAYER_B){
    return glm::vec3(0.0,1.0, 0.5);
  }
  return glm::vec3(1.0f); //--> white
}
// render the mesh
void Mesh::Draw(const Shader & shader, unsigned int hints) const
{
  shader.setVec3("objectColor", colorFor(hints|m_hints));
  bindBuffers();
  setupVertexAttributes();
  glDrawElements(mode, numIndices, gl_index_type, 0);
}

Mesh::PoolEntry & Mesh::poolEntry(){
  auto it = pool.find(topology);

  if(it == pool.end()){
    PoolEntry entry;
    entry.EBO = 0;
    entry.topology = topology;
    return pool[topology] = std::move(entry);
  }
  else {
    return it->second;
  }
}
void Mesh::bindBuffers() const {
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
}

void Mesh::setupVertexAttributes() const {
  // set the vertex attribute pointers
  // vertex Positions
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
}


void Mesh::setupMesh(const vector<Vertex> & vertices, const vector<v_ix> & indices)
{

  PoolEntry & entry = poolEntry();

  // setup a new EBO if the entry does not have one
  if(entry.EBO == 0){
    glGenBuffers(1, &entry.EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, entry.EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(v_ix), &indices[0], GL_STATIC_DRAW);
  }
  EBO = entry.EBO;

  // check if the entry has a VBO that we can reuse
  if(entry.unusedVBOs.empty()){
    //if not, create a new one.
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_DYNAMIC_DRAW);
  }
  else {
    // otherwise, reuse an old one and copy over the new vertex data
    VBO = entry.unusedVBOs.front();
    entry.unusedVBOs.pop_front();
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size() * sizeof (Vertex), &vertices[0]);
  }


  setupVertexAttributes();

}

/* aquire refCounter and initialize it to zero
 */
unsigned int Mesh::createCounter(){
  //use a slot that is known to be free.
  unsigned int c = nextFreeCounter;
  //We need to determine new value for nextFreeCounter.
  //if the slot was previously used, it should contain the address of another
  //known-to-be-free slot. Otherwise, just increment.
  if (c < refCounts.size()){
    nextFreeCounter = refCounts[c];
    refCounts[c] = 0;
  }
  else {
    refCounts.push_back(0); 
    nextFreeCounter = refCounts.size();
  }

  return c;
}

/* mark a refCounter as unused */
void Mesh::freeCounter(unsigned int c){
  //the current value of nextFreeCounter is a known-to-be-free slot.
  //Before overwriting it, we store it in the slot we are just now
  //marking free.
  //This way, we effectively create a forward-linked list but without
  //the need for allocating separate memory.
  refCounts[c] = nextFreeCounter;
  nextFreeCounter = c;
}

void Mesh::incrementRefCount(){
  if (!VBO){
    return; 
  }
  totalMeshCount++;
  totalVertexCount+=numVertices;
  totalIndicesCount+=numIndices;
  refCounts[counter] ++;
}

void Mesh::decrementRefCount(){
  if (!VBO){
    return; 
  }
  totalMeshCount--;
  totalVertexCount-=numVertices;
  totalIndicesCount-=numIndices;
  assert(refCounts[counter]>0);
  refCounts[counter]--;
  if (refCounts[counter] == 0){
    freeCounter(counter); 
    cleanupMesh();
  }
}


void Mesh::cleanupMesh(){
  PoolEntry & entry = poolEntry();
  entry.unusedVBOs.push_back(VBO);
  VBO=0;
  EBO=0;
}

