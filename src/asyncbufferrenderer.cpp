#include "asyncbufferrenderer.h"
#include "misc-utils.h"
#include "flags.h"

#include <iostream>



AsyncBufferRenderer::AsyncBufferRenderer()
	:numberOfBuffers(4)
{
  buffers.reserve(numberOfBuffers);
  for(size_t i=0;i<numberOfBuffers;i++){
    buffer_t & buffer = buffers[i];
    glGenBuffers(1, &buffer.VBO);
    glCheckError();
    glGenBuffers(1, &buffer.EBO);
    glCheckError();
    glBindBuffer(GL_ARRAY_BUFFER, buffer.VBO);
    glCheckError();
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer.EBO);
    glCheckError();
    glBufferData(GL_ARRAY_BUFFER, vboSize * sizeof(vertex_t), nullptr, GL_STREAM_DRAW);
    glCheckError();
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, eboSize * sizeof(index_t), nullptr, GL_STREAM_DRAW);
    glCheckError();
    buffer.fence=nullptr;
  }
  // GLuint vaoId = 0;
  // glGenVertexArrays(1, &vaoId);
  //glBindVertexArray(vaoId);
}
AsyncBufferRenderer::Slot *AsyncBufferRenderer::allocateSlot(AsyncBufferRenderer::tid_t tid){
  // "allocating" a slot has become really easy as of late...

  index_t offset = static_cast<index_t>(tid) * slotSize;
  if(tid >= usedSlots.size()){
    usedSlots.resize(tid+1);
    positions.resize(offset+slotSize);
  }

  Slot & s = usedSlots[tid];
  s.start = offset;
  s.size=0;

  return &s;
}

void AsyncBufferRenderer::addGeometry(AsyncBufferRenderer::tid_t tid, const vector<glm::vec2> & coordinates){
  Slot * slot = allocateSlot(tid);

  if(!slot){
    error("slot allocation failed\n");
    return;
  }

  index_t offset = 1;
  positions[slot->start] = {0,0};
  slot->size=static_cast<index_t>(coordinates.size()+offset);

  for(index_t i = 0; i< coordinates.size(); i++){
    positions[offset+i+slot->start] = coordinates[i];
  }

}

void AsyncBufferRenderer::removeGeometry(AsyncBufferRenderer::tid_t tid){
}

void AsyncBufferRenderer::reset(unsigned int upperBound)
{
  buffer_t & buffer = buffers[bufNum % numberOfBuffers];

  if(buffer.fence){
    GLenum result = glClientWaitSync(buffer.fence, 0, 4*1000*1000);
    glCheckError();
    if(result == GL_TIMEOUT_EXPIRED){
      warn("timeout expired");
    }
    if(result == GL_WAIT_FAILED){
      error("wait failed");
    }
    glDeleteSync(buffer.fence);
    glCheckError();
    buffer.fence=nullptr;
  }

  glBindBuffer(GL_ARRAY_BUFFER, buffer.VBO);
  glCheckError();
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer.EBO);
  glCheckError();

  unsigned int vertexUpperBound = upperBound
                                  ? std::min(static_cast<unsigned int>(vboSize),upperBound * slotSize)
                                  : vboSize;
  unsigned int indexUpperBound = upperBound
                                 ? std::min(static_cast<unsigned int>(eboSize),upperBound * 3 * (slotSize - 1))
                                 : eboSize;

  GLint bufferMapped;
  glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_MAPPED, &bufferMapped);
  glCheckError();

  mappedVertices = static_cast<vertex_t*>(glMapBufferRange(GL_ARRAY_BUFFER, 0, vertexUpperBound*sizeof(vertex_t), GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT));
  glCheckError();
  mappedIndices = static_cast<index_t*>(glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, indexUpperBound*sizeof(index_t), GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT));
  glCheckError();

  indexPos = 0;
  vertexPos = 0;
}

void AsyncBufferRenderer::submit()
{
  buffer_t & buffer = buffers[bufNum % numberOfBuffers];
  bufNum = bufNum  % numberOfBuffers;

  glUnmapBuffer(GL_ARRAY_BUFFER);
  glCheckError();
  glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
  glCheckError();

  mappedIndices = nullptr;
  mappedIndices = nullptr;

  if(!glIsBuffer(buffer.VBO)){
    glCheckError();
    error("buffer.VBO is not a buffer object?");

  }
  int test;
  glGetIntegerv(GL_ARRAY_BUFFER_BINDING,  &test);
  glCheckError();
  if(test != buffer.VBO || test == 0){
    error("buffer.VBO is not the currently bound array buffer?!");
  }
  // x,y
  glEnableVertexAttribArray(0);
    glCheckError();
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(vertex_t), nullptr);
    glCheckError();
  // transform (x,y,theta)
  glEnableVertexAttribArray(1);
    glCheckError();
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vertex_t), (GLvoid*)(2*sizeof(GLfloat)));
    glCheckError();
  // layer
  // glEnableVertexAttribArray(2);
  //   glCheckError();
  // glVertexAttribPointer(2, 1, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(vertex_t), (GLvoid*)(5*sizeof(GLfloat)));
  //   glCheckError();
  // color
  glEnableVertexAttribArray(2);
    glCheckError();
  glVertexAttribPointer(2, 1, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(vertex_t), (GLvoid*)(5*sizeof(GLfloat) /*+ sizeof(GLubyte)*/));
    glCheckError();
  glDrawElements(
        GL_TRIANGLES,
        indexPos,
        GL_UNSIGNED_SHORT,
        0);

    glCheckError();
  buffer.fence = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
    glCheckError();
}

void AsyncBufferRenderer::draw(Renderer::tid_t tid, const glm::vec3 &transform, uint8_t layer, uint8_t color)
{
  auto const & slot = usedSlots[tid];
  auto start = slot.start;
  auto v0 = vertexPos;
  auto vEnd = v0 + slot.size;

  // make sure we do not exceed the mapped vbo range
  if(v0+slot.size > vboSize) {
    error("vbo size exceeded!!");
    return;
  }
  //copy the vertex data
  for(index_t i = 0; i <slot.size; i++){
    vertex_t & vertex = mappedVertices[v0+i];
    vertex.position = positions[start + i];
    vertex.transform = transform;
    //vertex.layer = layer;
    vertex.color = color;
  }

  //avance vertexPos
  vertexPos += slot.size;

  //Each edge of the contour will contribute a triangle with the middle vertex.
  //The number of contour edges is equal to the number of contour vertices.
  //So the number of indices we need is exactly 3 * (slot.size - 1)

  index_t numIndices = 3 * (slot.size - 1);
  index_t i0 = indexPos;
  // check those indices will fit into the mapped EBO range
  if(i0 + numIndices > eboSize) {
    error("ebo size exceedd!!");
    return;
  }

  //draw triangles (a.k.a. connect the dots)
  //note that we are starting v0 + 1, as v0 is the middle vertex
  for(index_t i = v0 + 1 ; i < vEnd; i++ ){
    mappedIndices[indexPos++] = v0;
    mappedIndices[indexPos++] = i;
    mappedIndices[indexPos++] = (i+1 < vEnd) ? i + 1 : v0 + 1;
  }
}

AsyncBufferRenderer::~AsyncBufferRenderer()
{
  for(size_t i=0;i<numberOfBuffers;i++){
    buffer_t & buffer = buffers[i];
    if(buffer.VBO){
      glDeleteBuffers(1, &buffer.VBO);
    glCheckError();
    }
    if(buffer.EBO){
      glDeleteBuffers(1, &buffer.EBO);
    glCheckError();
    }
    if(buffer.fence){
      glDeleteSync(buffer.fence);
    glCheckError();
    }
  }
}

