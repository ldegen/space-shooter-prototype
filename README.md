# Prototype for a Game

I am trying to build a simple space shooter. Something like Asteroids or maybe Space Wars.
I am doing this mainly to wrap my head around some of the general problems involved here.
I have zero experience with OpenGL, C++ or game programming in general.
Don't expect a playable game ;-)

# Build Requirements

You will need `glfw3`, `libstb`, `libassimp4`, `chipmunk`.
I am using the dev-packages from Debian sid. 

Details --> please have a look into the (really tiny) Makefile.

## Quick list for Debian buster

build-essential cmake freeglut3-dev libassimp-dev libboost1.67-dev
libglfw3-dev libglm-dev liblo-dev libpolyclipping-dev libsdl2-dev
libstb-dev libxi-dev libxmu-dev libxrandr-dev

# About Chipmunk...

While everything should work with the version 6.x.x shipped with Debian, I am actually using
7.0.x for development. Sadly, Chipmunk 7 does not compile out of the box because of its dependency
on a rather antique version of GLFW. 

See https://github.com/slembcke/Chipmunk2D/issues/181

I am working on a pull request for this problem, hopefully it gets accepted.

Have a look at the first few lines of the Makefile, there you can select which
version of chipmunk should be used.

# Sources/References

As I said, I doing this to teach me a little C++ and OpenGL. I started out by following this fantastic tutorial: https://learnopengl.com/Getting-started/Hello-Triangle
There are still some code snippets from that tutorial in my code base. 