#include<array>
#include<vector>
#include<bitset>
#include<iostream>
#include<functional>
#include<string>

#include<chipmunk.h>
#include <glm/glm.hpp>

using namespace std;






class Foo;

template <typename D>
int exec(const function<int(string, D&)> & f, D & data){
  return f("world", data);
}

int f(const string & s, Foo & foo){
  cout << "indeed, hello " << s <<endl;
  return 0;
};

class Foo {

  public:

  static int g(const string & s, Foo & self){
    cout << "indeed, hello " << s <<endl;
    return 0;
  };

  int bar(){
    return exec<Foo>(g, *this); 
  }
};

int notmain() {
  int v = 0;
  Foo foo;
  v+= exec<Foo>([](const string & s, Foo & foo){
    cout << "hello " <<s <<endl;
    return 0;
  }, foo);
  v+= exec<Foo>(f,foo);

  v+= foo.bar();

  double w = 42;
  double * W = &w;
  int * V = &v;
  cout << "sizeof(uint8_t): "<<sizeof(uint8_t)<<endl;
  cout << "sizeof(uint8_t): "<<sizeof(uint8_t)<<endl;
  cout << "sizeof(long): "<<sizeof(long)<<endl;
  cout << "  V: " << V<<endl;
  cout << "V+1: " << V+1<<endl;
  cout << "  W: " << W<<endl;
  cout << "W+1: " << W+1<<endl;

  cout << "sizeof(pair<double,double>): "<<sizeof(pair<double, double>)<<endl;
  cout << "sizeof(cpVect): "<<sizeof(cpVect)<<endl;
  cout << "sizeof(glm::dvec2): "<<sizeof(glm::dvec2)<<endl;
  return v;
}
