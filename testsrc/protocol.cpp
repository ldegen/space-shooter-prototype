#include <catch.hpp>
#include "../src/body-type.h"
#include "../src/protocol.h"
#include <vector>
#include <iostream>

using namespace std;

template<typename T>
uint8_t * write(const T & data, uint8_t * buf){
  T * tbuf = (T *) buf;
  *tbuf = data;
  return (uint8_t *) (tbuf + 1);
}
template<typename M>
uint8_t detect2(const M msg){
  return detect(&msg);
};
template<typename M>
uint8_t detect(const M * msg){
  size_t msgSize = sizeof(M);
  uint8_t * src = (uint8_t *) msg;
  unsigned int i = *src;
  return *src;
};
TEST_CASE("detecting the type (first byte) of a message"){
  cmd_create_body_type msg{.tid=12,2};
  REQUIRE(detect(&msg) == CMD_CREATE_BODY_TYPE);
  REQUIRE(detect2<cmd_create_body_type>({.tid=12,2}) == CMD_CREATE_BODY_TYPE);
}
TEST_CASE("reading a body type"){
  class Listener : public ProtocolListener {
    public: 
    vector<BodyType> bts;
    virtual void onBodyType(const BodyType & bt){
      bts.push_back(std::move(bt));
    };
  } listener;
  ProtocolParser parser(listener);

  uint8_t buffer[1024];

  uint8_t * d = buffer;

  //create two identical body types. Each with two shapes,
  //a triangle and a square
  for(int i=0;i<2;i++){ 
    d = write(cmd_create_body_type {.tid=12, 2}, d);
    d = write(data_shape {.density=0.4, .elasticity=0.6,.friction=0.2, .num_vertices=3},d);
    d = write(data_vertex {-1.0 , 0.0},d);
    d = write(data_vertex {1.0 , 0.0},d);
    d = write(data_vertex {0.0 , 1.0},d);
    d = write(data_shape {.density=0.2, .elasticity=0.2, .friction=0.1, .num_vertices=4},d);
    d = write(data_vertex {-1.0 , -1.0},d);
    d = write(data_vertex {1.0 , -1.0},d);
    d = write(data_vertex {1.0 , 1.0},d);
    d = write(data_vertex {-1.0 , 1.0},d);
  }

  size_t size = sizeof(cmd_create_body_type)
      +2*sizeof(data_shape)
      +7*sizeof(data_vertex);
  REQUIRE(d == buffer + 2 * size);

  SECTION("reading the full body type in one gulp"){
    // we allow the parser to read one of the BTs plus
    // the first byte of the second shape, just for fun. 
    optional<size_t> read = parser.push(buffer, size+1);

    // This extra byte should not be consumed.
    REQUIRE(read.value() == size);
    REQUIRE(listener.bts.size() == 1);

    // do a few quick checks to see if the correct data
    // was copied

    REQUIRE(listener.bts[0].shapes.size() == 2);
    REQUIRE(listener.bts[0].shapes[1].vertices().size() == 4);
    REQUIRE(listener.bts[0].shapes[1].vertices()[2].x == 1.0);
  }
  SECTION("chunking it up a bit"){
    // in the middle of the first shape header
    size_t d1 = sizeof(cmd_create_body_type) + 2;

    // after reading the first shape including vertices
    size_t d2 = sizeof(cmd_create_body_type)
      + sizeof(data_shape) + 3 * sizeof(data_vertex);

    //after reading the first vertex of the second shape
    size_t d3 = d2 + sizeof(data_shape) + sizeof(data_vertex);

    size_t read = 0;
    read = parser.push(buffer, d1).value();
    read += parser.push(buffer + read, d2 - read).value();
    read += parser.push(buffer + read, d3 - read).value();

    //again, give one byte extra, like above. 
    read += parser.push(buffer + read, 1 + size - read).value();

    REQUIRE(read == size);
    REQUIRE(listener.bts.size() == 1);
  }
  SECTION("two body types"){
    size_t read = parser.push(buffer, 2*size).value();

    REQUIRE(read == 2*size);
    REQUIRE(listener.bts.size() == 2);
  }
}
