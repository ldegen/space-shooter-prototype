
#include <catch.hpp>
#include <vector>

using namespace std;


class Foo {
  int v;
  int d;

  public:
  Foo():v(42), d(0){}
  Foo(const Foo & other)
    : d(1+other.d)
    , v(other.v)
  {}
  Foo(Foo && other)
    : v(other.v)
    , d(other.d)
  {
    other.v=0;
    other.d=0;
  }

  Foo & operator=(Foo && other){
    d=other.d;
    v=other.v;
    other.v=0;
    other.d=0;
    return *this;
  }
  Foo & operator=(const Foo & other){
    d=other.d+1;
    v=other.v;
    return *this;
  }
  Foo copy(){
    return *this;
  }

  Foo inc() const &{
    Foo clone {*this};
    clone.v++;
    return clone;
  }
  Foo inc() &&{
    v++;
    return std::move(*this);
  }

  Foo & ref(){
    return *this; 
  }
  const Foo & constRef(){
    return *this; 
  }
  //string bar() {return "";}
  //string bar() & {return "&";}
  string bar() && {return "&&";}
  //string bar() const {return "const";}
  string bar() const &{return "const &";}
  //string bar() const && {return "const &&";}

  int depth() const {
    return d; 
  }
  int value() const {
    return v; 
  }
};
TEST_CASE("qualifier and overrides"){
  Foo f;
  REQUIRE(f.bar() == "const &");
  REQUIRE(f.copy().bar() == "&&");
  REQUIRE(f.ref().bar() == "const &");
  REQUIRE(f.copy().copy().bar() == "&&");
  // and even:
  REQUIRE(f.ref().copy().bar() == "&&"); 
  //interstingly, though:
  REQUIRE(f.copy().ref().bar() == "const &"); 
  // it seems that refs are "opaque" to the compiler?
}


TEST_CASE("copy or move"){
  Foo f;
  Foo g;
  REQUIRE(f.depth() == 0);
  REQUIRE(f.copy().depth() == 1);
  REQUIRE(f.inc().depth() == 1);
  REQUIRE(g.inc().depth() == 1);
  REQUIRE(f.inc().inc().depth() == 1);
  REQUIRE(f.inc().inc().depth() == 1);
  REQUIRE(f.inc().inc().value() == 44);
  REQUIRE(f.value() == 42);
}
