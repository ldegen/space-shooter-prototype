
#include <catch.hpp>
#include <vector>

using namespace std;

TEST_CASE("using std::vector to store binary data"){
  vector<uint8_t> buf;
  buf.reserve(256);

  uint8_t data[5];
  data[0] = 42;
  data[1] = 43;
  data[2] = 44;
  data[3] = 45;
  data[4] = 46;

  buf.assign(data, data+5);

  REQUIRE(buf.size() == 5);
  REQUIRE(buf[0] == 42);
  REQUIRE(buf[1] == 43);
  REQUIRE(buf[2] == 44);
  REQUIRE(buf[3] == 45);
  REQUIRE(buf[4] == 46);
}
