
#include <catch.hpp>


#include <math.h>

using namespace std;

TEST_CASE("modulo"){
  REQUIRE(fabs(fmod(144.4, 20) - 4.4) < 0.000000000001);
  REQUIRE(fabs(fmod(-0.5, 20) - -0.5) < 0.000000000001);
}
