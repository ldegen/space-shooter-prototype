#include <catch.hpp>
#include <vector>

using namespace std;

TEST_CASE("moving a vector"){
    vector<int> a {1,2,3};
    vector<int> b {std::move(a)};
    a.clear();
    REQUIRE(b.size() == 3);
    REQUIRE(b[0] == 1);
    REQUIRE(b[1] == 2);
    REQUIRE(b[2] == 3);
    REQUIRE(a.size() == 0);
}

TEST_CASE("move something into a unordered_map does NOT keep the pointer"){
  unordered_map<int, int> vec;

  int foo = 42;
  int * p1 = &foo;
  vec.insert(make_pair(23,move(foo)));
  int * p2 = &vec[23];
  REQUIRE(p1 != p2);

}
