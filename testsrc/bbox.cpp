
#include <catch.hpp>

#include <iostream>
#include <bitset>
#include <math.h>
#include "../src/bbox.hpp"

using namespace std;
using Catch::VectorContains;
using glm::tvec2;
using glm::dvec2;

template <typename T>
T sqDist(const tvec2<T> & A, const tvec2<T> & B){
  tvec2<T> tmp = A-B;
  return glm::dot(tmp, tmp);
}


TEST_CASE("point modulo box"){
  const Bbox<double> box {-1.5, -1.5, 1.5, 1.5};



  REQUIRE(sqDist(dvec2(-2.0, 2.0) % box, dvec2(1.0, -1.0)) < 0.0001);
  REQUIRE(sqDist(box.topRight() % box, box.bottomLeft()) < 0.0001);
  REQUIRE(sqDist(box.bottomLeft() % box, box.bottomLeft()) < 0.0001);

}

TEST_CASE("box modulo box"){
  const Bbox<double> b {13.3,-2.4, 13.9, -2.1};
  const Bbox<double> grid {-1.5, -1.5, 1.5, 1.5};

  const Bbox<double> q = b % grid;
  
  REQUIRE(fabs(q.left - 1.3) <= 0.00001);
  REQUIRE(fabs(q.bottom - 0.6) <= 0.00001);
  REQUIRE(fabs(q.right - 1.9) <= 0.00001);
  REQUIRE(fabs(q.top - 0.9) <= 0.00001);
}

TEST_CASE("bbox split (regression)"){
  const Bbox<double> grid {-192,-108,192,108};
  const Bbox<double> extent {-384, -216, 0, 0};

  vector<Bbox<double>> facets = split(extent, grid);

  REQUIRE_THAT(facets, VectorContains(Bbox<double>{-384, -216, -192, -108}));
  REQUIRE_THAT(facets, VectorContains(Bbox<double>{-192, -216, 0, -108}));
  REQUIRE_THAT(facets, VectorContains(Bbox<double>{-384, -108, -192, 0}));
  REQUIRE_THAT(facets, VectorContains(Bbox<double>{-192, -108, 0, 0}));
  REQUIRE(facets.size() == 4);
}


TEST_CASE("box wrapped around box"){
  const Bbox<double> grid {-1.5, -1.5, 1.5, 1.5};
  const Bbox<double> b {1.0, -2.0, 2.0, -1.0};

  vector<Bbox<double>> facets = wrap(b, grid);

  REQUIRE_THAT(facets, VectorContains(Bbox<double>{1.0,1.0, 1.5, 1.5}));
  REQUIRE_THAT(facets, VectorContains(Bbox<double>{-1.5,1.0, -1.0, 1.5}));
  REQUIRE_THAT(facets, VectorContains(Bbox<double>{1.0,-1.5, 1.5, -1.0}));
  REQUIRE_THAT(facets, VectorContains(Bbox<double>{-1.5,-1.5, -1.0, -1.0}));

  REQUIRE(facets.size() == 4);
}

TEST_CASE("rounding"){
  REQUIRE(0==(int)0.6);
}

TEST_CASE("bitwise operators"){

//std::cout<<rec.uid<<"/"<<i<<"/"<<std::bitset<16>{state.flags}<<": "<<state.x<<", "<<state.y<<std::endl;

  cout<<"~0: "<<bitset<32>{(unsigned int)~0}<<endl;
  cout<<"5 & ~4 | 2: "<<bitset<32>{(unsigned int)(5 & ~4 | 2)}<<endl;
  cout<<"5 & ~0 | 2: "<<bitset<32>{(unsigned int)(5 & ~0 | 2)}<<endl;

  REQUIRE((5 & ~4 | 2) == 3);
  REQUIRE((5 & ~0 | 2) == 7);
}
