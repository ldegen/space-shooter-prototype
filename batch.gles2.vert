#version 100
//uniform mat3 normalMatrix;
uniform mat4 view;
uniform mat4 projection;

attribute vec2 pos;
attribute vec3 xyT;
attribute float colorIx;
//attribute vec3 aNormal;
//attribute vec2 aTexCoords;

//varying vec2 TexCoords;
//varying vec3 FragPos;
//varying vec3 Normal;
//varying vec3 Center;

varying float vertColorIx;



void main()
{
    //TexCoords = aTexCoords;    
    //Normal = aNormal;
    float theta = xyT.z;
    float s = sin(theta);
    float c = cos(theta);

    // note that glsl matrices are initialized column by column
    mat3 model;
    model[0] = vec3(c,s,0.0); //first column
    model[1] = vec3(-s, c,0.0);    //second column
    model[2] = vec3(xyT.xy, 0.0); //third column

    vec3 FragPos = model * vec3(pos, 1.0);
   // vec3 FragPos = vec3(pos, 0.0);
    vertColorIx = colorIx;
    gl_Position = projection * view * vec4(FragPos, 1.0);
}
