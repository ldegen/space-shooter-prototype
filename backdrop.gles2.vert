#version 100
uniform mat4 model;
uniform mat3 normalMatrix;
uniform mat4 view;
uniform mat4 projection;

attribute vec3 aPos;
attribute vec3 aNormal;
attribute vec2 aTexCoords;

varying vec2 TexCoords;
varying vec3 FragPos;
varying vec3 Normal;


void main()
{
    TexCoords = aTexCoords;    
    Normal = normalMatrix * aNormal;
    FragPos = vec3(model * vec4(aPos, 1.0));

    gl_Position = projection * view * vec4(FragPos, 1.0);
}
