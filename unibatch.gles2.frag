#version 100
precision mediump float;

//varying vec2 TexCoords;

//FIXME: this does actually contain
//       barycentric coordinates.
//       I am too lazy to rename it.
//varying vec3 Normal;
//varying vec3 FragPos;
//varying vec3 Center;

varying float vertColorIx;
//uniform vec3 lightPos;
//uniform vec3 lightColor;
//uniform vec3 objectColor;
//uniform sampler2D texture_diffuse1;
//varying vec3 vertColor;
void main() {
  vec3 white = vec3(1.0,1.0,1.0);
  vec3 red = vec3(1.0,0.0,0.0);
  vec3 green = vec3(0.0,1.0,0.0);

  float step0 = 0.0;
  float step1 = 1.0;
  float step2 = 2.0;

  vec3 objectColor = mix(red, white,smoothstep(step0, step1,vertColorIx));
  objectColor = mix(objectColor, green, smoothstep(step1, step2, vertColorIx));
  // ambient
  //FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);
  //FragColor = tmp*texture(texture_diffuse1, TexCoords);
  //gl_FragColor = tmp * vec4(objectColor, 1.0);

  //gl_FragColor = Normal.y * vec4(objectColor, 1.0);
  //float d = smoothstep(0.75,0.9, 1.0-Normal.z);
  //gl_FragColor = d * vec4(objectColor, 0.0);

   gl_FragColor = vec4(objectColor,1.0);
}
