#version 100
precision mediump float;

varying vec3 FragPos;
varying vec3 Center;

uniform vec3 gridColorA;
uniform vec3 gridColorB;
uniform vec2 viewportSize;
void main() { 
  vec2 frag = vec2(FragPos.x, FragPos.y);
  vec2 center =vec2(Center.x, Center.y);
  float dist = length(frag-center);
  float ref = length(viewportSize);
  float mixF = 4.0 * dist/ref;
  vec3 color = gridColorA * mixF + gridColorB * (1.0-mixF);
  //vec3 color = vec3(.0,FragPos.xy/viewportSize);
  gl_FragColor = vec4( color, 0.4);

}
