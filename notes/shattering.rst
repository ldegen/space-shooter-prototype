When an asteroid colides with a bullet or another asteroid I want it to be
shattered into little pieces.  I got the idea from one of the chipmunk demos.
The way it is done there (as far as I understand the code):

- devide the shapes bbox into equal-sized, axis-aligned grid cells.  for each
  of the grid cells, pick one random point within the cell.  The author refers
  to this as "Worley" context. I am not 100% sure if this is actually related
  to Worley Noise, but the idea of using one feature point per grid cell seems
  similar.

- After this, create Voronoi regions around the feature point of each cell.
  The Voronoi regions are the new shapes. Which looks rather nice, although the 
  implementation seems somewhat wasteful. Or maybe I didn't understand it.

- To preserve the original contours, the region at the boundary of the original
  shape need to be clipped. At this part, I somehow got lost in the code. I
  understand that this is necessary, but I have the feeling it could be done
  more efficiently. Which is OK, after all, it is a toy example.


Then I spent some time looking for alternative approaches. It should be possible
to choose the feature points in a way that makes clipping entirely unnecessary.
After all, I "just" have to find a Delaunay-Triangulation that ensures that the
original contour can be reconstructed by piecing togeher perpendicular bisectors
of Delaunay-Edges. The vertices in the original shape have to be points where 
the bisectors of some Delaunay-triangle intersect. Surely we can turn this around 
and fabricate feature points that support this.

I found some publications on a Software/Algorithm called "VoroCrust", which
seems to take a similar approach, but in 3D.  We only need 2d, lucky us. :-)


So, here's the plan. 

- We create the Voronoi regions indirectly via a Delaunay-Triangulation. We use
  a slightly modified Bowyer–Watson Variant to "grow" our triangle mesh. Vertex
  coordinates are choosen randomly, but in a way that satisfies the
  abovementioned criteria.

  - Start with on biiiiiig triangle. 

  - For each vertex of the original contour, 

    - insert three vertices Bowyer-Watson-Style, forming a triangle such that
      the two contour segments meeting at the original vertex intersect at the
      triangles circumcenter.

    When adding further points, it is important to make sure that these initial
    triangles are *not touched*. So when adding a vertex, we always check if it
    lies within the circumcircle of one of those "holy" triangles. If it does,
    we reject that point and choose another one.

  - Next, we add further vertices as we see fit, obeying the usual rules.
    We need to watch out for edges crossing the original contour line.

    TODO: yet have to formulate an exact rule. It seems that we need pairs of
    triangle that form isosceles trapezoids. The bases are perpendicular to the
    contour line. Note that the circumcircle of both participating triangles
    *is* the circumcircle of the trapezoid. The solution is porbably to add
    "enough" support triangles not only at the contour vertices but also along
    the contour segments.
