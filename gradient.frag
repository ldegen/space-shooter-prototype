#version 330 core
out vec4 FragColor;
in vec3 FragPos;
in vec3 Center;
uniform vec3 gridColorA;
uniform vec3 gridColorB;
uniform vec2 viewportSize;
void main() { 
  vec2 frag = vec2(FragPos.x, FragPos.y);
  vec2 center =vec2(Center.x, Center.y);
  float dist = length(frag-center);
  float ref = length(viewportSize);
  float mix = 4 * dist/ref;
  vec3 color = gridColorA * mix + gridColorB * (1-mix);
  //FragColor = vec4(200000.0*len*len * gridColor, 1.0);
  FragColor = vec4( color, 1.0);
}
