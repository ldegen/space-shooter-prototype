#version 330 core
out vec4 FragColor;

in vec3 FragPos;

uniform vec3 gridColorA;
uniform vec3 gridColorB;
uniform vec2 gridSize;
uniform vec2 viewportSize;


void main() { 
  // ambient
  //FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);
  //FragColor = tmp*texture(texture_diffuse1, TexCoords);
  //FragColor = tmp * vec4(objectColor, 1.0);

  vec2 frag = vec2(FragPos.x/gridSize.x, FragPos.y/gridSize.y);
  vec2 fragFloor = vec2(FragPos.x/viewportSize.x, FragPos.y/viewportSize.y);
  vec2 gridPoint = round(frag);
  //float ux = mod(FragPos.x, gridSize.x);
  //float uy = mod(FragPos.y, gridSize.y);
  
  //vec2 gridPoint = vec2(floor(FragPos.x/gridSize.x), floor(FragPos.y/gridSize.y));

  vec2 delta = frag-gridPoint;
  float dist = min(abs(delta.x), abs(delta.y));
  float distFloor = fragFloor.x+ fragFloor.y;
  //float intensity = 0.75 - sqrt(dist); 
  float intensity = step(0.95, 1-dist)*(1-40*dist);
  float mix = distFloor / 2.0;
  //float mix = 1.0;
  vec3 color = gridColorA * mix + gridColorB * (1-mix);
  //FragColor = vec4(200000.0*len*len * gridColor, 1.0);
  FragColor = vec4(intensity * color, 1.0);
  //FragColor = vec4(1.0,0.0,1.0, 1.0);
}
