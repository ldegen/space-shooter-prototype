CP_CFLAGS = -I/usr/local/include/chipmunk
CP_LDFLAGS = -Wl,-rpath,/usr/local/lib
#CP_CFLAGS = -I/home/lukas/src/Chipmunk2D/include/chipmunk
#CP_LDFLAGS = -L/home/lukas/src/Chipmunk2D/src

CFLAGS = -std=c++17 -I./include -I/usr/include/stb -I/usr/include/polyclipping -D_REENTRANT -I/usr/include/SDL2 $(CP_CFLAGS) -fmax-errors=1
LDFLAGS = -std=c++17 `pkg-config --static --libs liblo glfw3 assimp polyclipping sdl2` -lstb -lchipmunk $(CP_LDFLAGS) -lboost_program_options
SRC=$(wildcard src/*.cpp)
CSRC=$(wildcard src/*.c)
mains=src/sdl-main.o src/glfw-main.o src/experiment.o #src/voronoi_basic_tutorial.o src/shatter-test.o
testmains=testsrc/main.o
OBJ=$(filter-out $(mains) $(testmains), $(SRC:.cpp=.o) $(CSRC:.c=.o))
DEP=$(SRC:.cpp=.d) $(CSRC:.c=.d)

TESTSRC=$(wildcard testsrc/*.cpp)
TESTOBJ=$(filter-out $(mains) $(testmains), $(TESTSRC:.cpp=.o) )
DEP=$(SRC:.cpp=.d) $(CSRC:.c=.d) $(TESTSRC:.cpp=.d)

#CFLAGS += -DDEBUG_PROTOCOL

# enable profiling
# note to self: to profile, compile with the -pg flag, then run, which will create
# a file gmon.out containing the raw data.
# use gprof to turn it into a human readable text file which you can e.g. open in vim.
#CFLAGS += -pg
#LDFLAGS += -pg

#CFLAGS += -DGLES2
#CFLAGS += -g
CFLAGS += -O8
#CFLAGS += -DENABLE_OBJECT_BUILDER

all: sdl-main glfw-main experiment# voronoi_basic_tutorial shatter-test

.PHONY: test clean

-include $(DEP)

src/glad.o: src/glad.c
	$(CXX) $(CFLAGS) -o $@ -MMD -c $<
src/%.o: src/%.cpp
	$(CXX) $(CFLAGS) -o $@ -MMD -c $<

testsrc/%.o: testsrc/%.cpp
	$(CXX) $(CFLAGS) -o $@ -MMD -c $<

voronoi_basic_tutorial: $(OBJ) $(mains)
	$(CXX) $(OBJ) src/voronoi_basic_tutorial.o $(LDFLAGS) -o voronoi_basic_tutorial

sdl-main: $(OBJ) $(mains)
	$(CXX)  $(OBJ) src/sdl-main.o  $(LDFLAGS) -o sdl-main

glfw-main: $(OBJ) $(mains)
	$(CXX)  $(OBJ) src/glfw-main.o $(LDFLAGS) -o glfw-main

experiment: $(OBJ) $(mains)
	$(CXX)  $(OBJ) src/experiment.o $(LDFLAGS) -o experiment

hello-triangle.o: hello-triangle.cpp
	$(CXX) $(CFLAGS) -o $@ -MMD -c $<

hello: $(OBJ) hello-triangle.o
	$(CXX) $(OBJ) hello-triangle.o $(LDFLAGS) -o hello

shatter-test: $(OBJ) $(mains)
	$(CXX) $(OBJ) src/shatter-test.o $(LDFLAGS) -o shatter-test
clean:
	rm -f sdl-main glfw-main test voronoi_basic_tutorial shatter-test $(OBJ) $(TESTOBJ) $(DEP) $(mains) $(testmains)

test: $(OBJ) $(TESTOBJ) $(testmains)
	$(CXX) $(OBJ) $(TESTOBJ) $(testmains) $(LDFLAGS) -o test 
